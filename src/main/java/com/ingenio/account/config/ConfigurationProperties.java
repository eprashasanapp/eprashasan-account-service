package com.ingenio.account.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
//@PropertySource("file:C:/updatedwars/configuration/fee.properties")
public class ConfigurationProperties {
	
	public static String tallyXmlBaseUrl;

	@Value("${tallyXmlBaseUrl}")
	public  void setTallyXmlBaseUrl(String tallyXmlBaseUrl) {
		ConfigurationProperties.tallyXmlBaseUrl = tallyXmlBaseUrl;
	}
}
