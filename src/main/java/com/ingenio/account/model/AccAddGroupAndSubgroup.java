package com.ingenio.account.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 * AddGroupAndSubgroup entity. @author MyEclipse Persistence Tools
 */
@Entity
@DynamicUpdate
@Table(name = "acc_add_group_and_subgroup")
public class AccAddGroupAndSubgroup implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer groupSubgroupId;
	private AccAddGroupAndSubgroup2 addGroupAndSubgroup2;
	private AccAddGroupAndSubgroup4 addGroupAndSubgroup4;
	private AccCreateSchoolCollege accCreateSchoolCollege;
	private AccAddGroupAndSubgroup6 addGroupAndSubgroup6;
	private AccAddHead addHead;
	private AccAddGroupAndSubgroup1 addGroupAndSubgroup1;
	private AccAddGroupAndSubgroup5 addGroupAndSubgroup5;
	private AccAddGroupAndSubgroup3 addGroupAndSubgroup3;
	private AccAddSubHead addSubHead;
	private String isDel;
	private SchoolMasterModel schoolMaster;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit="0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	private List<AccCreateLedger> accCreateLedgers = new ArrayList<AccCreateLedger>(0);
	// Property accessors
	@Id
	//@GeneratedValue(strategy = IDENTITY)
	@Column(name = "groupSubgroupID", unique = true, nullable = false)
	public Integer getGroupSubgroupId() {
		return this.groupSubgroupId;
	}

	public void setGroupSubgroupId(Integer groupSubgroupId) {
		this.groupSubgroupId = groupSubgroupId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupSubgroup2ID")
	public AccAddGroupAndSubgroup2 getAddGroupAndSubgroup2() {
		return this.addGroupAndSubgroup2;
	}

	public void setAddGroupAndSubgroup2(
			AccAddGroupAndSubgroup2 addGroupAndSubgroup2) {
		this.addGroupAndSubgroup2 = addGroupAndSubgroup2;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupSubgroup4ID")
	public AccAddGroupAndSubgroup4 getAddGroupAndSubgroup4() {
		return this.addGroupAndSubgroup4;
	}

	public void setAddGroupAndSubgroup4(
			AccAddGroupAndSubgroup4 addGroupAndSubgroup4) {
		this.addGroupAndSubgroup4 = addGroupAndSubgroup4;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cschoolID")
	public AccCreateSchoolCollege getCreateSchoolCollege() {
		return this.accCreateSchoolCollege;
	}

	public void setCreateSchoolCollege(AccCreateSchoolCollege accCreateSchoolCollege) {
		this.accCreateSchoolCollege = accCreateSchoolCollege;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupSubgroup6ID")
	public AccAddGroupAndSubgroup6 getAddGroupAndSubgroup6() {
		return this.addGroupAndSubgroup6;
	}

	public void setAddGroupAndSubgroup6(
			AccAddGroupAndSubgroup6 addGroupAndSubgroup6) {
		this.addGroupAndSubgroup6 = addGroupAndSubgroup6;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "heaID")
	public AccAddHead getAddHead() {
		return this.addHead;
	}

	public void setAddHead(AccAddHead addHead) {
		this.addHead = addHead;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupSubgroup1ID")
	public AccAddGroupAndSubgroup1 getAddGroupAndSubgroup1() {
		return this.addGroupAndSubgroup1;
	}

	public void setAddGroupAndSubgroup1(
			AccAddGroupAndSubgroup1 addGroupAndSubgroup1) {
		this.addGroupAndSubgroup1 = addGroupAndSubgroup1;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupSubgroup5ID")
	public AccAddGroupAndSubgroup5 getAddGroupAndSubgroup5() {
		return this.addGroupAndSubgroup5;
	}

	public void setAddGroupAndSubgroup5(
			AccAddGroupAndSubgroup5 addGroupAndSubgroup5) {
		this.addGroupAndSubgroup5 = addGroupAndSubgroup5;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupSubgroup3ID")
	public AccAddGroupAndSubgroup3 getAddGroupAndSubgroup3() {
		return this.addGroupAndSubgroup3;
	}

	public void setAddGroupAndSubgroup3(
			AccAddGroupAndSubgroup3 addGroupAndSubgroup3) {
		this.addGroupAndSubgroup3 = addGroupAndSubgroup3;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subHeadID")
	public AccAddSubHead getAddSubHead() {
		return this.addSubHead;
	}

	public void setAddSubHead(AccAddSubHead addSubHead) {
		this.addSubHead = addSubHead;
	}

	@Column(name = "isDel", length = 1)
	public String getIsDel() {
		return this.isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@OrderColumn
@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "addGroupAndSubgroup")
	public List<AccCreateLedger> getCreateLedgers() {
		return this.accCreateLedgers;
	}

	public void setCreateLedgers(List<AccCreateLedger> accCreateLedgers) {
		this.accCreateLedgers = accCreateLedgers;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMaster;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

}