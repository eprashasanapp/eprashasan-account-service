 
package com.ingenio.account.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@DynamicUpdate
@Table(name="acc_create_school_college")

public class AccCreateSchoolCollege  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
     private Integer cschoolId;
     private AccAddOrgType addOrgType;
     private String schoolRegiNo;
     private String schoolName;
     private String address;
     private String regiDate;
     private String panNo;
     private String vatTinNo;
     private String tanNo;
     private String serviceTaxNo;
     private String catNo;
     private String isDel;
     private SchoolMasterModel schoolMaster;
	 private AppUserRoleModel userId;
	 @Generated(GenerationTime.ALWAYS)
	 @Temporal(javax.persistence.TemporalType.DATE)
	 private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	 private Date delDate;
	 private AppUserRoleModel deleteBy;
	 private String isEdit="0";
	 private Date editDate;
	 private AppUserRoleModel editBy;
	 private String isApproval="0";
     private AppUserRoleModel approvalBy;
     private Date approvalDate;
	 private String deviceType="0";
	 private String ipAddress;
	 private String macAddress;
	 private String sinkingFlag="0";
	 private String schoolNameGL;
	 private String addressGL;

    // Constructors

    /** default constructor */
    public AccCreateSchoolCollege() {
    }
    

    // Property accessors
    @Id
    @Column(name="cschoolID", unique=true, nullable=false)
    public Integer getCschoolId() {
        return this.cschoolId;
    }
    
    public void setCschoolId(Integer cschoolId) {
        this.cschoolId = cschoolId;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="orgTypeID")
    public AccAddOrgType getAddOrgType() {
        return this.addOrgType;
    }
    
    public void setAddOrgType(AccAddOrgType addOrgType) {
        this.addOrgType = addOrgType;
    }
    
    @Column(name="schoolRegiNo", unique=true, length=20)
    public String getSchoolRegiNo() {
        return this.schoolRegiNo;
    }
    
    public void setSchoolRegiNo(String schoolRegiNo) {
        this.schoolRegiNo = schoolRegiNo;
    }
    
    @Column(name="schoolName", unique=true, length=100)
    public String getSchoolName() {
        return this.schoolName;
    }
    
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    
    @Column(name="address", length=100)
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    @Column(name="regiDate", length=10)
    public String getRegiDate() {
        return this.regiDate;
    }
    
    public void setRegiDate(String regiDate) {
        this.regiDate = regiDate;
    }
    
    @Column(name="panNo", length=20)
    public String getPanNo() {
        return this.panNo;
    }
    
    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }
    
    @Column(name="vatTinNo", length=20)
    public String getVatTinNo() {
        return this.vatTinNo;
    }
    
    public void setVatTinNo(String vatTinNo) {
        this.vatTinNo = vatTinNo;
    }
    
    @Column(name="tanNo", length=20)
    public String getTanNo() {
        return this.tanNo;
    }
    
    public void setTanNo(String tanNo) {
        this.tanNo = tanNo;
    }
    
    @Column(name="serviceTaxNo", length=20)
    public String getServiceTaxNo() {
        return this.serviceTaxNo;
    }
    
    public void setServiceTaxNo(String serviceTaxNo) {
        this.serviceTaxNo = serviceTaxNo;
    }
    
    @Column(name="catNo", length=20)
    public String getCatNo() {
        return this.catNo;
    }
    
    public void setCatNo(String catNo) {
        this.catNo = catNo;
    }
    
    @Column(name="isDel", length=1)
    public String getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }
    
    
    
   
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMaster;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@Column(name="schoolNameGL", length=100)
	public String getSchoolNameGL() {
		return schoolNameGL;
	}

	public void setSchoolNameGL(String schoolNameGL) {
		this.schoolNameGL = schoolNameGL;
	}

	@Column(name="addressGL", length=100)
	public String getAddressGL() {
		return addressGL;
	}

	public void setAddressGL(String addressGL) {
		this.addressGL = addressGL;
	}

}