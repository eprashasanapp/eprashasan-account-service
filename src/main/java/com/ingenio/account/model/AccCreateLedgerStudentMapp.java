package com.ingenio.account.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@DynamicUpdate
@Table(name="acc_create_ledger_to_student_mapp")
public class AccCreateLedgerStudentMapp {
	
	private Integer ledgerStudentMappId;
	private AccCreateLedger accCreateLedger;
	private StudentMasterModel studentMaster;
	private AppUserRoleModel appUserRole; 
	private String isDel="0";
	private Date delDate;
	private String isEdit="0";
	private Date editDate;
	private String sinkingFlag="0";
	private SchoolMasterModel schoolMaster;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	
	@Id
	//@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "ledgerStudentMappId", nullable = false)
	public Integer getLedgerStudentMappId() {
		return ledgerStudentMappId;
	}
	public void setLedgerStudentMappId(Integer ledgerStudentMappId) {
		this.ledgerStudentMappId = ledgerStudentMappId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ledgerId", nullable = false)
	public AccCreateLedger getAccCreateLedger() {
		return accCreateLedger;
	}

	public void setAccCreateLedger(AccCreateLedger accCreateLedger) {
		this.accCreateLedger = accCreateLedger;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "studentId", nullable = false)
	public StudentMasterModel getStudentMasterModel() {
		return studentMaster;
	}
	public void setStudentMasterModel(StudentMasterModel studentMaster) {
		this.studentMaster = studentMaster;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return appUserRole;
	}
	
	public void setAppUserRoleModel(AppUserRoleModel appUserRole) {
		this.appUserRole = appUserRole;
	}
	
	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMaster;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
}
