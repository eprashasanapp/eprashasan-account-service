package com.ingenio.account.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 * AccCreateLedger entity. @author MyEclipse Persistence Tools
 */
@Entity
@DynamicUpdate
@Table(name = "acc_create_ledger")
public class AccCreateLedger implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer ledgerId;
	private AccCreateLedgerHead accCreateLedgerHead;
	private AccAddGroupAndSubgroup addGroupAndSubgroup;
	private AccCreateSchoolCollege accCreateSchoolCollege;
	private String ledgerName;
	private Double openingBalance;
	private String amountType;
	private String seventyThirty;
	private String isDel;
	private String updatedBy;
	private SchoolMasterModel schoolMaster;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit="0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
//	private List<AccJournalDayBookEntry> journalDayBookEntriesForDebitLedger = new ArrayList<AccJournalDayBookEntry>(0);
//	private List<AccJournalDayBookEntry> journalDayBookEntriesForCreditLedger = new ArrayList<AccJournalDayBookEntry>(0);
//	private List<AccDepreciation> accDepreciations = new ArrayList<AccDepreciation>(0);
//	private List<AccCreateLedgerSubHeadMapp> accLedgerMapp = new ArrayList<AccCreateLedgerSubHeadMapp>(0);
//	private List<AccCreateLedgerFeesTaxMapp> accLedgerTaxMapp = new ArrayList<AccCreateLedgerFeesTaxMapp>(0);
//	private List<AccCreateLedgerBankMapp> accLedgerBankMapp = new ArrayList<AccCreateLedgerBankMapp>(0);
//	private List<AccCreateLedgerStandardMapp> accLedgerStandardMapp = new ArrayList<AccCreateLedgerStandardMapp>(0);
//	private List<AccCreateLedgerStudentMapp> accLedgerStudentMapp = new ArrayList<AccCreateLedgerStudentMapp>(0);
//	private List<AccCreateLedgerAllowanceMapp> accCreateLedgerAllowanceMapp = new ArrayList<AccCreateLedgerAllowanceMapp>(0);
//	private List<AccCreateLedgerDeductionMapp> accLedgerDeductionMapp = new ArrayList<AccCreateLedgerDeductionMapp>(0);
//	private List<AccCreateLedgerStaffMapp> accLedgerStaffMapp = new ArrayList<AccCreateLedgerStaffMapp>(0);
//	private List<AccCreateLedgerBudgetHeadMap> accLedgerBudgetHeadMapp = new ArrayList<AccCreateLedgerBudgetHeadMap>(0);
//	private List<AccCreateLedgerSupplierMap> accLedgerSupplierMapp = new ArrayList<AccCreateLedgerSupplierMap>(0);
//	private List<AccCreateLedgerTaxMapp> accLedgerStckTaxMapp = new ArrayList<AccCreateLedgerTaxMapp>(0);
	// Constructors

	/** default constructor */
	public AccCreateLedger() {
	}

	/** full constructor */
	public AccCreateLedger(AccCreateLedgerHead accCreateLedgerHead,
			AccAddGroupAndSubgroup addGroupAndSubgroup,
			AccCreateSchoolCollege accCreateSchoolCollege, String ledgerName,
			Double openingBalance, String amountType, String seventyThirty,
			String isDel,String updatedby,
			List<AccJournalDayBookEntry> journalDayBookEntriesForDebitLedger,
			List<AccJournalDayBookEntry> journalDayBookEntriesForCreditLedger,
			List<AccDepreciation> accDepreciations) {
		this.accCreateLedgerHead = accCreateLedgerHead;
		this.addGroupAndSubgroup = addGroupAndSubgroup;
		this.accCreateSchoolCollege = accCreateSchoolCollege;
		this.ledgerName = ledgerName;
		this.openingBalance = openingBalance;
		this.amountType = amountType;
		this.seventyThirty = seventyThirty;
		this.isDel = isDel;
		this.updatedBy=updatedby;
//		this.journalDayBookEntriesForDebitLedger = journalDayBookEntriesForDebitLedger;
//		this.journalDayBookEntriesForCreditLedger = journalDayBookEntriesForCreditLedger;
//		this.accDepreciations = accDepreciations;
	}

	// Property accessors
	@Id
	@Column(name = "ledgerID", unique = true, nullable = false)
	public Integer getLedgerId() {
		return this.ledgerId;
	}

	public void setLedgerId(Integer ledgerId) {
		this.ledgerId = ledgerId;
	}		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ledgerHeadID")
	public AccCreateLedgerHead getAccCreateLedgerHead() {
		return this.accCreateLedgerHead;
	}

	public void setAccCreateLedgerHead(AccCreateLedgerHead accCreateLedgerHead) {
		this.accCreateLedgerHead = accCreateLedgerHead;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupSubgroupID")
	public AccAddGroupAndSubgroup getAddGroupAndSubgroup() {
		return this.addGroupAndSubgroup;
	}

	public void setAddGroupAndSubgroup(AccAddGroupAndSubgroup addGroupAndSubgroup) {
		this.addGroupAndSubgroup = addGroupAndSubgroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cschoolID")
	public AccCreateSchoolCollege getAccCreateSchoolCollege() {
		return this.accCreateSchoolCollege;
	}

	public void setAccCreateSchoolCollege(AccCreateSchoolCollege accCreateSchoolCollege) {
		this.accCreateSchoolCollege = accCreateSchoolCollege;
	}

	@Column(name = "ledgerName", length = 30)
	public String getLedgerName() {
		return this.ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	@Column(name = "openingBalance", precision = 22, scale = 0)
	public Double getOpeningBalance() {
		return this.openingBalance;
	}

	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}

	@Column(name = "AmountType", length = 2)
	public String getAmountType() {
		return this.amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	@Column(name = "seventyThirty", length = 1)
	public String getSeventyThirty() {
		return this.seventyThirty;
	}

	public void setSeventyThirty(String seventyThirty) {
		this.seventyThirty = seventyThirty;
	}

	@Column(name = "isDel", length = 1)
	public String getIsDel() {
		return this.isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "updatedBy", length = 1)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "createLedgerByDebitLedger")
//	public List<AccJournalDayBookEntry> getJournalDayBookEntriesForDebitLedger() {
//		return this.journalDayBookEntriesForDebitLedger;
//	}
//
//	public void setJournalDayBookEntriesForDebitLedger(
//			List<AccJournalDayBookEntry> journalDayBookEntriesForDebitLedger) {
//		this.journalDayBookEntriesForDebitLedger = journalDayBookEntriesForDebitLedger;
//	}
//
//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "createLedgerByCreditLedger")
//	public List<AccJournalDayBookEntry> getJournalDayBookEntriesForCreditLedger() {
//		return this.journalDayBookEntriesForCreditLedger;
//	}
//
//	public void setJournalDayBookEntriesForCreditLedger(
//			List<AccJournalDayBookEntry> journalDayBookEntriesForCreditLedger) {
//		this.journalDayBookEntriesForCreditLedger = journalDayBookEntriesForCreditLedger;
//	}
//
//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "createLedger")
//	public List<AccDepreciation> getDepreciations() {
//		return this.accDepreciations;
//	}
//
//	public void setDepreciations(List<AccDepreciation> accDepreciations) {
//		this.accDepreciations = accDepreciations;
//	}
//
//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerSubHeadMapp> getAccLedgerMapp() {
//		return accLedgerMapp;
//	}
//
//	public void setAccLedgerMapp(List<AccCreateLedgerSubHeadMapp> accLedgerMapp) {
//		this.accLedgerMapp = accLedgerMapp;
//	}
//
//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerBankMapp> getAccLedgerBankMapp() {
//		return accLedgerBankMapp;
//	}
//
//	public void setAccLedgerBankMapp(List<AccCreateLedgerBankMapp> accLedgerBankMapp) {
//		this.accLedgerBankMapp = accLedgerBankMapp;
//	}
//
//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerStandardMapp> getAccLedgerStandardMapp() {
//		return accLedgerStandardMapp;
//	}
//
//	public void setAccLedgerStandardMapp(List<AccCreateLedgerStandardMapp> accLedgerStandardMapp) {
//		this.accLedgerStandardMapp = accLedgerStandardMapp;
//	}
//
//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerStudentMapp> getAccLedgerStudentMapp() {
//		return accLedgerStudentMapp;
//	}
//
//	public void setAccLedgerStudentMapp(List<AccCreateLedgerStudentMapp> accLedgerStudentMapp) {
//		this.accLedgerStudentMapp = accLedgerStudentMapp;
//	}

//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerAllowanceMapp> getAccCreateLedgerAllowanceMapp() {
//		return accCreateLedgerAllowanceMapp;
//	}
//
//	public void setAccCreateLedgerAllowanceMapp(List<AccCreateLedgerAllowanceMapp> accCreateLedgerAllowanceMapp) {
//		this.accCreateLedgerAllowanceMapp = accCreateLedgerAllowanceMapp;
//	}

//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerDeductionMapp> getAccLedgerDeductionMapp() {
//		return accLedgerDeductionMapp;
//	}
//
//	public void setAccLedgerDeductionMapp(List<AccCreateLedgerDeductionMapp> accLedgerDeductionMapp) {
//		this.accLedgerDeductionMapp = accLedgerDeductionMapp;
//	}
//
//	@OrderColumn
//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerStaffMapp> getAccLedgerStaffMapp() {
//		return accLedgerStaffMapp;
//	}
//
//	public void setAccLedgerStaffMapp(List<AccCreateLedgerStaffMapp> accLedgerStaffMapp) {
//		this.accLedgerStaffMapp = accLedgerStaffMapp;
//	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMaster;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

//	@OrderColumn
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerFeesTaxMapp> getAccLedgerTaxMapp() {
//		return accLedgerTaxMapp;
//	}
//
//	public void setAccLedgerTaxMapp(List<AccCreateLedgerFeesTaxMapp> accLedgerTaxMapp) {
//		this.accLedgerTaxMapp = accLedgerTaxMapp;
//	}
//
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerBudgetHeadMap> getAccLedgerBudgetHeadMapp() {
//		return accLedgerBudgetHeadMapp;
//	}
//
//	public void setAccLedgerBudgetHeadMapp(List<AccCreateLedgerBudgetHeadMap> accLedgerBudgetHeadMapp) {
//		this.accLedgerBudgetHeadMapp = accLedgerBudgetHeadMapp;
//	}
//
//
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerSupplierMap> getAccLedgerSupplierMapp() {
//		return accLedgerSupplierMapp;
//	}
//
//	public void setAccLedgerSupplierMapp(List<AccCreateLedgerSupplierMap> accLedgerSupplierMapp) {
//		this.accLedgerSupplierMapp = accLedgerSupplierMapp;
//	}
//	
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accCreateLedger")
//	public List<AccCreateLedgerTaxMapp> getAccLedgerStckTaxMapp() {
//		return accLedgerStckTaxMapp;
//	}
//
//	public void setAccLedgerStckTaxMapp(List<AccCreateLedgerTaxMapp> accLedgerStckTaxMapp) {
//		this.accLedgerStckTaxMapp = accLedgerStckTaxMapp;
//	}

	
}