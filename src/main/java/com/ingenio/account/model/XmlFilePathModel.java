package com.ingenio.account.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "xml_filepath")
public class XmlFilePathModel implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer filePathId;
	private Integer schoolid;
	private Integer csSchoolid;
	private String filePath;
	private String xmlType;
	
	@Id
	@Column(name = "filePathId", unique = true, nullable = false)
	public Integer getFilePathId() {
		return filePathId;
	}
	public void setFilePathId(Integer filePathId) {
		this.filePathId = filePathId;
	}
	
	@Column(name = "schoolid")
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	
	@Column(name = "csSchoolid")
	public Integer getCsSchoolid() {
		return csSchoolid;
	}
	public void setCsSchoolid(Integer csSchoolid) {
		this.csSchoolid = csSchoolid;
	}
	
	@Column(name = "filePath")
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	@Column(name = "xmlType")
	public String getXmlType() {
		return xmlType;
	}
	public void setXmlType(String xmlType) {
		this.xmlType = xmlType;
	}
	
	

}
