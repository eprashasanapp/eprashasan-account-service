package com.ingenio.account.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity

@Table(name = "fee_paid_duetable")
public class FeePaidDueModel {

	private static final long serialVersionUID = 1L;
	private Integer paidDueId;
	private FeeStudentFeeModel feeStudentFeeModel;
	private AppUserRoleModel appUserRole;
	private SchoolMasterModel schoolMasterModel;
	private FeeHeadMasterModel feeHeadMasterModel;
	private FeeSettingMultiModel feeSettingMulti;
	private FeeSubheadMasterModel feeSubheadMaster;
	private FeeReceiptModel feeReceiptModel;
	private String cdate;
	private Double dueFee;
	private String dueDate;
	private String narration;
	private String fineClearance="0";
	private Integer isConsolation=0;
	private String isDel="0";
	private String isEdit="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";

	@Id
	@Column(name = "paidDueID", unique = true, nullable = false)
	public Integer getPaidDueId() {
		return this.paidDueId;
	}

	public void setPaidDueId(Integer paidDueId) {
		this.paidDueId = paidDueId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "studFeeID", nullable = false)
	public FeeStudentFeeModel getFeeStudentFeeModel() {
		return this.feeStudentFeeModel;
	}

	public void setFeeStudentFeeModel(FeeStudentFeeModel feeStudentFeeModel) {
		this.feeStudentFeeModel = feeStudentFeeModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return this.appUserRole;
	}

	public void setAppUserRoleModel(AppUserRoleModel appUserRole) {
		this.appUserRole = appUserRole;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID")
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "headID", nullable = false)
	public FeeHeadMasterModel getFeeHeadMasterModel() {
		return this.feeHeadMasterModel;
	}

	public void setFeeHeadMasterModel(FeeHeadMasterModel feeHeadMasterModel) {
		this.feeHeadMasterModel = feeHeadMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "feeSetMultiID", nullable = false)
	public FeeSettingMultiModel getFeeSettingMultiModel() {
		return this.feeSettingMulti;
	}

	public void setFeeSettingMultiModel(FeeSettingMultiModel feeSettingMulti) {
		this.feeSettingMulti = feeSettingMulti;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subheadID")
	public FeeSubheadMasterModel getFeeSubheadMasterModel() {
		return this.feeSubheadMaster;
	}

	public void setFeeSubheadMasterModel(FeeSubheadMasterModel feeSubheadMaster) {
		this.feeSubheadMaster = feeSubheadMaster;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptID")
	public FeeReceiptModel getFeeReceiptModel() {
		return this.feeReceiptModel;
	}

	public void setFeeReceiptModel(FeeReceiptModel feeReceiptModel) {
		this.feeReceiptModel = feeReceiptModel;
	}
	
	@Column(name = "cDate", updatable= false)
	public String getCdate() {
		return this.cdate;
	}

	public void setCdate(String cdate) {
		this.cdate = cdate;
	}

	@Column(name = "dueFee", precision = 22, scale = 0)
	public Double getDueFee() {
		return this.dueFee;
	}

	public void setDueFee(Double dueFee) {
		this.dueFee = dueFee;
	}

	@Column(name = "dueDate", length = 50)
	public String getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	@Column(name = "narration", length = 100)
	public String getNarration() {
		return this.narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	@Column(name = "fineClearance", length = 50)
	public String getFineClearance() {
		return this.fineClearance;
	}

	public void setFineClearance(String fineClearance) {
		this.fineClearance = fineClearance;
	}

	@Column(name = "isConsolation", nullable = false)
	public Integer getIsConsolation() {
		return this.isConsolation;
	}

	public void setIsConsolation(Integer isConsolation) {
		this.isConsolation = isConsolation;
	}

	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
}