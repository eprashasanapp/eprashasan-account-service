package com.ingenio.account.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;


@Entity
@Table(name = "school_master")

public class SchoolMasterModel {
	
	private static final long serialVersionUID = 1L;
	private Integer schoolid;
	private String schoolName;
	private String schoolSaunsthaName;
	private String regId;
	private String encryValideDays;
	private String startDate;
	private String endDate;
	private String suspiousDate;
	private String updatedDate;
	private String address;
	private String city;
	private String state;
	private Integer pincode;
	private String contactNo;
	private Integer faxNo;
	private String email;
	private String website;
	private String logoFileName;
	private String letterHeadFileName;
	private String lcFileName;
	private String bonafideFileName;
	private String entryDate;
	private String remark;
	private String schoolNameGL;
	private String schoolSaunsthaNameGL;
	private String encryValideDaysGL;
	private String addressGL;
	private String cityGL;
	private String stateGL;
	private String remarkGL;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private String sinkingFlag = "0";
	private Date delDate;
	private Date editDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sansthaRegNo;
	private String sansthaKey;
	private String schoolKey;
	private AppLanguageModel language;
	//private String schoolType;
	

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "schoolid", unique = true, nullable = false)
	public Integer getSchoolid() {
		return this.schoolid;
	}
	
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	
	@Column(name = "schoolName", length = 500)
	public String getSchoolName() {
		return this.schoolName;
	}
	
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
	@Column(name = "school_saunsthaName", length = 500)
	public String getSchoolSaunsthaName() {
		return this.schoolSaunsthaName;
	}
	
	public void setSchoolSaunsthaName(String schoolSaunsthaName) {
		this.schoolSaunsthaName = schoolSaunsthaName;
	}
	
	@Column(name = "regId", length = 200)
	public String getRegId() {
		return this.regId;
	}
	
	public void setRegId(String regId) {
		this.regId = regId;
	}
	
	@Column(name = "encry_ValideDays", length = 200)
	public String getEncryValideDays() {
		return this.encryValideDays;
	}
	
	public void setEncryValideDays(String encryValideDays) {
		this.encryValideDays = encryValideDays;
	}
	
	@Column(name = "start_date", length = 400)
	public String getStartDate() {
		return this.startDate;
	}
	
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	@Column(name = "end_Date", length = 400)
	public String getEndDate() {
		return this.endDate;
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	@Column(name = "suspious_Date", length = 400)
	public String getSuspiousDate() {
		return this.suspiousDate;
	}
	
	public void setSuspiousDate(String suspiousDate) {
		this.suspiousDate = suspiousDate;
	}
	
	@Column(name = "updated_Date", length = 400)
	public String getUpdatedDate() {
		return this.updatedDate;
	}
	
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "address", length = 500)
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "city", length = 50)
	public String getCity() {
		return this.city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name = "state", length = 50)
	public String getState() {
		return this.state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name = "pincode")
	public Integer getPincode() {
		return this.pincode;
	}
	
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
	
	@Column(name = "contactNo", length = 15)
	public String getContactNo() {
		return this.contactNo;
	}
	
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	
	@Column(name = "faxNo")
	public Integer getFaxNo() {
		return this.faxNo;
	}
	
	public void setFaxNo(Integer faxNo) {
		this.faxNo = faxNo;
	}
	
	@Column(name = "email", length = 50)
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "website", length = 50)
	public String getWebsite() {
		return this.website;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}
	
	@Column(name = "logoFileName", length = 50)
	public String getLogoFileName() {
		return this.logoFileName;
	}
	
	public void setLogoFileName(String logoFileName) {
		this.logoFileName = logoFileName;
	}
	
	@Column(name = "letterHeadFileName", length = 500)
	public String getLetterHeadFileName() {
		return this.letterHeadFileName;
	}
	
	public void setLetterHeadFileName(String letterHeadFileName) {
		this.letterHeadFileName = letterHeadFileName;
	}
	
	@Column(name = "lcFileName", length = 500)
	public String getLcFileName() {
		return this.lcFileName;
	}
	
	public void setLcFileName(String lcFileName) {
		this.lcFileName = lcFileName;
	}
	
	@Column(name = "BonafideFileName", length = 50)
	public String getBonafideFileName() {
		return this.bonafideFileName;
	}
	
	public void setBonafideFileName(String bonafideFileName) {
		this.bonafideFileName = bonafideFileName;
	}
	
	@Column(name = "entryDate", length = 400)
	public String getEntryDate() {
		return this.entryDate;
	}
	
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	
	@Column(name = "remark", length = 100)
	public String getRemark() {
		return this.remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name = "school_saunsthaNameGL")
	public String getSchoolSaunsthaNameGL() {
		return schoolSaunsthaNameGL;
	}
	
	public void setSchoolSaunsthaNameGL(String schoolSaunsthaNameGL) {
		this.schoolSaunsthaNameGL = schoolSaunsthaNameGL;
	}
	
	@Column(name = "encry_ValideDaysGL")
	public String getEncryValideDaysGL() {
		return encryValideDaysGL;
	}
	
	public void setEncryValideDaysGL(String encryValideDaysGL) {
		this.encryValideDaysGL = encryValideDaysGL;
	}
	
	@Column(name = "schoolNameGL")
	public String getSchoolNameGL() {
		return schoolNameGL;
	}
	
	public void setSchoolNameGL(String schoolNameGL) {
		this.schoolNameGL = schoolNameGL;
	}
	
	@Column(name = "addressGL")
	public String getAddressGL() {
		return addressGL;
	}
	
	public void setAddressGL(String addressGL) {
		this.addressGL = addressGL;
	}
	
	@Column(name = "cityGL")
	public String getCityGL() {
		return cityGL;
	}
	
	public void setCityGL(String cityGL) {
		this.cityGL = cityGL;
	}
	
	@Column(name = "stateGL")
	public String getStateGL() {
		return stateGL;
	}
	
	public void setStateGL(String stateGL) {
		this.stateGL = stateGL;
	}
	
	@Column(name = "remarkGL")
	public String getRemarkGL() {
		return remarkGL;
	}
	
	public void setRemarkGL(String remarkGL) {
		this.remarkGL = remarkGL;
	}
	
	@Column(name = "sansthaRegNo")
	public String getSansthaRegNo() {
		return sansthaRegNo;
	}

	public void setSansthaRegNo(String sansthaRegNo) {
		this.sansthaRegNo = sansthaRegNo;
	}
	
	@Column(name = "sansthaKey")
	public String getSansthaKey() {
		return sansthaKey;
	}

	public void setSansthaKey(String sansthaKey) {
		this.sansthaKey = sansthaKey;
	}

	@Column(name = "schoolKey")
	public String getSchoolKey() {
		return schoolKey;
	}

	public void setSchoolKey(String schoolKey) {
		this.schoolKey = schoolKey;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "languageId", updatable= false)	
	public AppLanguageModel getLanguage() {
		return language;
	}

	public void setLanguage(AppLanguageModel language) {
		this.language = language;
	}

}