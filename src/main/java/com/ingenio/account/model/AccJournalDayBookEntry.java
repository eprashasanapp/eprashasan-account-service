package com.ingenio.account.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "acc_journal_day_book_entry")
public class AccJournalDayBookEntry implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Long journalId;
	private AccAddVoucherType accAddVoucherType;
	private AccCreateSchoolCollege accCreateSchoolCollege;
	private AccCreateLedger createLedgerByCreditLedger;
	private AccCreateLedger createLedgerByDebitLedger;
	private String cdate;
	private String ctime;
	private String entryDate;
	private String invNo;
	private String narration;
	private String paymentDetails;
	private Double drAmount;
	private Double crAmount;
	private String entryOf;
	private String bankReconDate;
	private Double lastClosingAmount;
	private String amountType;
	private String isDel="0";
	private Integer updatedBy;
	private Integer studentId;
	private SchoolMasterModel schoolMaster;
	private AppUserRoleModel userId;
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit="0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	private String bdFlag="0";
	private AppUserRoleModel bdRejectBy;
	private Date bdRejectDate;
	private AppUserRoleModel bdApprovalBy;
	private Date bdApprovalDate;
	private String scholarshipFlag;

	// Constructors

	/** default constructor */
	public AccJournalDayBookEntry() {
	}

	

	// Property accessors
	@Id
	@Column(name = "journalID", unique = true, nullable = false)
	public Long getJournalId() {
		return this.journalId;
	}

	public void setJournalId(Long journalId) {
		this.journalId = journalId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vTypeID")
	public AccAddVoucherType getAccAddVoucherType() {
		return this.accAddVoucherType;
	}

	public void setAccAddVoucherType(AccAddVoucherType accAddVoucherType) {
		this.accAddVoucherType = accAddVoucherType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cschoolID")
	public AccCreateSchoolCollege getAccCreateSchoolCollege() {
		return this.accCreateSchoolCollege;
	}

	public void setAccCreateSchoolCollege(AccCreateSchoolCollege accCreateSchoolCollege) {
		this.accCreateSchoolCollege = accCreateSchoolCollege;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creditLedger")
	public AccCreateLedger getCreateLedgerByCreditLedger() {
		return this.createLedgerByCreditLedger;
	}

	public void setCreateLedgerByCreditLedger(
			AccCreateLedger createLedgerByCreditLedger) {
		this.createLedgerByCreditLedger = createLedgerByCreditLedger;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "debitLedger")
	public AccCreateLedger getCreateLedgerByDebitLedger() {
		return this.createLedgerByDebitLedger;
	}

	public void setCreateLedgerByDebitLedger(
			AccCreateLedger createLedgerByDebitLedger) {
		this.createLedgerByDebitLedger = createLedgerByDebitLedger;
	}

	@Column(name = "cDate",updatable=false)
	public String getCdate() {
		return this.cdate;
	}

	public void setCdate(String cdate) {
		this.cdate = cdate;
	}

	@Column(name = "cTime", length = 10)
	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Column(name = "entryDate", length = 10)
	public String getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	@Column(name = "invNo", length = 10)
	public String getInvNo() {
		return this.invNo;
	}

	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}

	@Column(name = "narration", length = 100)
	public String getNarration() {
		return this.narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	@Column(name = "paymentDetails", length = 20)
	public String getPaymentDetails() {
		return this.paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	@Column(name = "drAmount", precision = 22, scale = 0)
	public Double getDrAmount() {
		return this.drAmount;
	}

	public void setDrAmount(Double drAmount) {
		this.drAmount = drAmount;
	}

	@Column(name = "crAmount", precision = 22, scale = 0)
	public Double getCrAmount() {
		return this.crAmount;
	}

	public void setCrAmount(Double crAmount) {
		this.crAmount = crAmount;
	}

	@Column(name = "entryOf", length = 1)
	public String getEntryOf() {
		return this.entryOf;
	}

	public void setEntryOf(String entryOf) {
		this.entryOf = entryOf;
	}

	@Column(name = "BankReconDate", length = 10)
	public String getBankReconDate() {
		return this.bankReconDate;
	}

	public void setBankReconDate(String bankReconDate) {
		this.bankReconDate = bankReconDate;
	}

	@Column(name = "lastClosingAmount", precision = 22, scale = 0)
	public Double getLastClosingAmount() {
		return this.lastClosingAmount;
	}

	public void setLastClosingAmount(Double lastClosingAmount) {
		this.lastClosingAmount = lastClosingAmount;
	}

	@Column(name = "amountType", length = 2)
	public String getAmountType() {
		return this.amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	@Column(name = "isDel", length = 1)
	public String getIsDel() {
		return this.isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "updatedBy", length = 11)
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name = "studentId", length = 11)
	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMaster;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@Column(name="bdFlag", columnDefinition="default '0'")
	public String getBdFlag() {
		return bdFlag;
	}

	public void setBdFlag(String bdFlag) {
		this.bdFlag = bdFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bdRejectBy", nullable = false)
	public AppUserRoleModel getBdRejectBy() {
		return bdRejectBy;
	}

	public void setBdRejectBy(AppUserRoleModel bdRejectBy) {
		this.bdRejectBy = bdRejectBy;
	}

	@Column(name="bdRejectDate")
	public Date getBdRejectDate() {
		return bdRejectDate;
	}

	public void setBdRejectDate(Date bdRejectDate) {
		this.bdRejectDate = bdRejectDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bdApprovalBy", nullable = false)
	public AppUserRoleModel getBdApprovalBy() {
		return bdApprovalBy;
	}

	public void setBdApprovalBy(AppUserRoleModel bdApprovalBy) {
		this.bdApprovalBy = bdApprovalBy;
	}

	@Column(name="bdApprovalDate")
	public Date getBdApprovalDate() {
		return bdApprovalDate;
	}

	public void setBdApprovalDate(Date bdApprovalDate) {
		this.bdApprovalDate = bdApprovalDate;
	}



	public String getScholarshipFlag() {
		return scholarshipFlag;
	}



	public void setScholarshipFlag(String scholarshipFlag) {
		this.scholarshipFlag = scholarshipFlag;
	}
}