package com.ingenio.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.account.model.AccJournalDayBookEntry;

public interface FeesLinkToAccountRepository extends JpaRepository<AccJournalDayBookEntry, Integer> {

}
