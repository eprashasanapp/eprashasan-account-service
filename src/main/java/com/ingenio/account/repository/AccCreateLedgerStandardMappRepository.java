package com.ingenio.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.account.model.AccCreateLedgerStandardMapp;

public interface AccCreateLedgerStandardMappRepository extends JpaRepository<AccCreateLedgerStandardMapp, Integer>  {

}
