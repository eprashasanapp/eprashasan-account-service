package com.ingenio.account.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.account.bean.XMLResponseBean;
import com.ingenio.account.bean.XmlFilePathBean;
import com.ingenio.account.model.FeePayTypeModel;

@Repository
public interface AccountRepository extends JpaRepository<FeePayTypeModel, Integer>  {

	@Query("select new com.ingenio.account.bean.XMLResponseBean(a.journalId,a.entryOf,Cast(DATE_FORMAT(STR_TO_DATE(a.entryDate,'%d-%m-%Y'),'%Y%m%d') as string),"
			+ "a.narration,d.voucherName,a.invNo,b.ledgerName,c.ledgerName,COALESCE(a.drAmount,0),COALESCE(a.crAmount,0),COALESCE(a.paymentDetails,''),"
			+ "COALESCE(e.ledgerBankId,0)) "
			+ " from AccJournalDayBookEntry a "
			+ "left join AccCreateLedger b on a.createLedgerByCreditLedger.ledgerId=b.ledgerId  "
			+ "left join AccCreateLedger c on a.createLedgerByDebitLedger.ledgerId=c.ledgerId "
			+ "left join AccAddVoucherType d on a.accAddVoucherType.vtypeId=d.vtypeId "
			+ "left join AccCreateLedgerBankMapp e on e.accCreateLedger.ledgerId=c.ledgerId "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.accCreateSchoolCollege.cschoolId=:csSchoolId and a.isDel='0' "
			+ "and  STR_TO_DATE(a.entryDate,'%d-%m-%Y') between STR_TO_DATE(:date1,'%d-%m-%Y') and STR_TO_DATE(:date2,'%d-%m-%Y') and a.entryOf='0'"
			+ "and a.accAddVoucherType.vtypeId=:type  and  b.ledgerName!='Fees Discount' group by  a.invNo")
	List<XMLResponseBean> getRecordsForTally( @Param("schoolId") Integer schoolId, @Param("csSchoolId") Integer csSchoolId, @Param("date1") String date1, 
			@Param("date2") String date2, @Param("type") Integer type);
	
	
	@Query("select new com.ingenio.account.bean.XMLResponseBean(a.groupSubgroup1id,a.name) from AccAddGroupAndSubgroup1 a "
			+ "where a.schoolMasterModel.schoolid=:schoolId ")
	List<XMLResponseBean> getRecordsForGroupMasterTally(Integer schoolId);
	
	@Query("select new com.ingenio.account.bean.XMLResponseBean(a.ledgerId,a.ledgerName,a.openingBalance,b.ledgerHeadName,d.name) from AccCreateLedger a "
			+ "left join AccCreateLedgerHead b on a.accCreateLedgerHead.ledgerHeadID=b.ledgerHeadID "
			+ "left join AccAddGroupAndSubgroup c on a.addGroupAndSubgroup.groupSubgroupId =c.groupSubgroupId "
			+ "left join AccAddGroupAndSubgroup1 d on c.addGroupAndSubgroup1.groupSubgroup1id=d.groupSubgroup1id  "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.accCreateSchoolCollege.cschoolId=:csSchoolId and a.ledgerName !='P and L/Surplus or Deficit' ")
	List<XMLResponseBean> getRecordsForLedgerMasterTally(Integer schoolId, Integer csSchoolId);
	
	@Query("select new com.ingenio.account.bean.XMLResponseBean(a.journalId,a.entryOf,Cast(DATE_FORMAT(STR_TO_DATE(a.entryDate,'%d-%m-%Y'),'%Y%m%d') as string),"
			+ "a.narration,d.voucherName,a.invNo,b.ledgerName,c.ledgerName,COALESCE(a.drAmount,0),COALESCE(a.crAmount,0),COALESCE(a.paymentDetails,''),"
			+ "COALESCE(e.ledgerBankId,0)) "
			+ " from AccJournalDayBookEntry a "
			+ "left join AccCreateLedger b on a.createLedgerByCreditLedger.ledgerId=b.ledgerId  "
			+ "left join AccCreateLedger c on a.createLedgerByDebitLedger.ledgerId=c.ledgerId "
			+ "left join AccAddVoucherType d on a.accAddVoucherType.vtypeId=d.vtypeId "
			+ "left join AccCreateLedgerBankMapp e on e.accCreateLedger.ledgerId=c.ledgerId "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.accCreateSchoolCollege.cschoolId=:csSchoolId and a.isDel='0' "
			+ "and  STR_TO_DATE(a.entryDate,'%d-%m-%Y') between STR_TO_DATE(:date1,'%d-%m-%Y') and STR_TO_DATE(:date2,'%d-%m-%Y') and a.entryOf='0'"
			+ "and a.invNo=:invNo and  b.ledgerName!='Fees Discount' ")
	List<XMLResponseBean> getRecordsForTallyByInvoNo( @Param("schoolId") Integer schoolId, @Param("csSchoolId") Integer csSchoolId, @Param("date1") String date1, 
			@Param("date2") String date2, @Param("invNo") String invNo);
	
	@Query("select new com.ingenio.account.bean.XMLResponseBean(a.journalId,a.entryOf,Cast(DATE_FORMAT(STR_TO_DATE(a.entryDate,'%d-%m-%Y'),'%Y%m%d') as string),"
			+ "a.narration,d.voucherName,a.invNo,b.ledgerName,c.ledgerName,COALESCE(a.drAmount,0),COALESCE(a.crAmount,0),COALESCE(a.paymentDetails,''),"
			+ "COALESCE(e.ledgerBankId,0)) "
			+ " from AccJournalDayBookEntry a "
			+ "left join AccCreateLedger b on a.createLedgerByCreditLedger.ledgerId=b.ledgerId  "
			+ "left join AccCreateLedger c on a.createLedgerByDebitLedger.ledgerId=c.ledgerId "
			+ "left join AccAddVoucherType d on a.accAddVoucherType.vtypeId=d.vtypeId "
			+ "left join AccCreateLedgerBankMapp e on e.accCreateLedger.ledgerId=c.ledgerId "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.accCreateSchoolCollege.cschoolId=:csSchoolId and a.isDel='0' "
			+ "and  STR_TO_DATE(a.entryDate,'%d-%m-%Y') between STR_TO_DATE(:date1,'%d-%m-%Y') and STR_TO_DATE(:date2,'%d-%m-%Y') and a.entryOf='0'"
			+ "and a.invNo=:invNo and  b.ledgerName='Fees Discount' ")
	List<XMLResponseBean> getRecordsForTallyByInvoNoForDiscount( @Param("schoolId") Integer schoolId, @Param("csSchoolId") Integer csSchoolId, @Param("date1") String date1, 
			@Param("date2") String date2, @Param("invNo") String invVo);


	@Query("select new com.ingenio.account.bean.XmlFilePathBean(a.filePathId,a.schoolid,a.csSchoolid,a.filePath,a.xmlType) "
			+ " from XmlFilePathModel a "
			+ "where a.schoolid=:schoolId and a.csSchoolid=:csSchoolId and a.xmlType=:xmlType ")
	List<XmlFilePathBean> getXmlFilePathList(Integer schoolId, Integer csSchoolId, String xmlType);

}
