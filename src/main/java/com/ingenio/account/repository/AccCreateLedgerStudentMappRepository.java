package com.ingenio.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.account.model.AccCreateLedgerStudentMapp;

public interface AccCreateLedgerStudentMappRepository extends JpaRepository<AccCreateLedgerStudentMapp, Integer>  {

}
