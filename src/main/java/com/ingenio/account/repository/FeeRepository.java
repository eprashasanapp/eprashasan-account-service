package com.ingenio.account.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.account.bean.FCMTokenBean;
import com.ingenio.account.bean.StudentDetailsBean;
import com.ingenio.account.model.FeeSettingMultiModel;


public interface FeeRepository extends JpaRepository<FeeSettingMultiModel, Integer> {
	
	@Query(value="SELECT COALESCE(k.bounceCharges,0) FROM FeeReceiptModel b "
			+ "LEFT JOIN FeeDeleteFeeModel a ON a.feeReceiptModel.receiptId=b.receiptId "
			+ "LEFT JOIN FeeStudentFeeModel c ON a.feeStudentFeeModel.studFeeId=c.studFeeId "
			+ "LEFT JOIN FeeSettingModel d ON d.feeSettingId=c.feeSettingModel.feeSettingId "
			+ "LEFT JOIN StudentStandardRenewModel e1 ON e1.studentMasterModel.studentId=c.studentStandardRenewModel.renewStudentId "
			+ "AND d.standardMasterModel.standardId=e1.standardMasterModel.standardId AND d.yearMasterModel.yearId=e1.yearMasterModel.yearId "
			+ "LEFT JOIN FeeBounceChargesModel k ON b.receiptId=k.feeReceiptModel.receiptId "
			+ "WHERE e1.renewStudentId=:renewStudentId  ")
	List<Double>  getBouncedAssignedCharges(@Param("renewStudentId") Integer renewStudentId);
	
	@Query(value="SELECT COALESCE(k.bouncePaidCharges,0) FROM FeeReceiptModel b "
			+ "LEFT JOIN FeeDeleteFeeModel a ON a.feeReceiptModel.receiptId=b.receiptId "
			+ "LEFT JOIN FeeStudentFeeModel c ON a.feeStudentFeeModel.studFeeId=c.studFeeId "
			+ "LEFT JOIN FeeSettingModel d ON d.feeSettingId=c.feeSettingModel.feeSettingId "
			+ "LEFT JOIN StudentStandardRenewModel e1 ON e1.studentMasterModel.studentId=c.studentStandardRenewModel.renewStudentId "
			+ "AND d.standardMasterModel.standardId=e1.standardMasterModel.standardId AND d.yearMasterModel.yearId=e1.yearMasterModel.yearId "
			+ "LEFT JOIN FeeBounceChargesModel k ON b.receiptId=k.feeReceiptModel.receiptId "
			+ "WHERE e1.renewStudentId=:renewStudentId  and k.feeReceiptModel.receiptId=:receiptId ")
	Double getBouncedPaidCharges(@Param("renewStudentId") Integer renewStudentId,@Param("receiptId") Integer receiptId);
	
	@Query(value="SELECT COALESCE(k.bouncePaidCharges,0) FROM FeeReceiptModel b "
			+ "LEFT JOIN FeeDeleteFeeModel a ON a.feeReceiptModel.receiptId=b.receiptId "
			+ "LEFT JOIN FeeStudentFeeModel c ON a.feeStudentFeeModel.studFeeId=c.studFeeId "
			+ "LEFT JOIN FeeSettingModel d ON d.feeSettingId=c.feeSettingModel.feeSettingId "
			+ "LEFT JOIN StudentStandardRenewModel e1 ON e1.studentMasterModel.studentId=c.studentStandardRenewModel.renewStudentId "
			+ "AND d.standardMasterModel.standardId=e1.standardMasterModel.standardId AND d.yearMasterModel.yearId=e1.yearMasterModel.yearId "
			+ "LEFT JOIN FeeBounceChargesModel k ON b.receiptId=k.feeReceiptModel.receiptId "
			+ "WHERE e1.renewStudentId=:renewStudentId ")
	List<Double> getBouncedPaidCharges(@Param("renewStudentId") Integer renewStudentId);
	
	@Query(value="SELECT COALESCE(k.bouncePaidCharges,0) FROM FeeReceiptModel b "
			+ "LEFT JOIN FeeDeleteFeeModel a ON a.feeReceiptModel.receiptId=b.receiptId "
			+ "LEFT JOIN FeeStudentFeeModel c ON a.feeStudentFeeModel.studFeeId=c.studFeeId "
			+ "LEFT JOIN FeeSettingModel d ON d.feeSettingId=c.feeSettingModel.feeSettingId "
			+ "LEFT JOIN StudentStandardRenewModel e1 ON e1.studentMasterModel.studentId=c.studentStandardRenewModel.renewStudentId "
			+ "AND d.standardMasterModel.standardId=e1.standardMasterModel.standardId AND d.yearMasterModel.yearId=e1.yearMasterModel.yearId "
			+ "LEFT JOIN FeeBounceChargesModel k ON b.receiptId=k.feeReceiptModel.receiptId "
			+ "WHERE e1.renewStudentId=:renewStudentId  ")
	List<Double>  getBouncedTotalPaidCharges(@Param("renewStudentId") Integer renewStudentId);


//	@Query(value="Select new com.ingenio.account.bean.FCMTokenBean(COALESCE(d.token,''), COALESCE(d.deviceType,'2')) from StudentMasterModel a left join StudentStandardRenewModel b on a.studentId=b.studentMasterModel.studentId "
//			+ "left join AppUserModel c on c.staffId=a.studentId left join AndroidFCMTokenMaster d on d.notificationUserId.appUserRoleId=c.appUserRoleId "
//			+ "where b.renewStudentId=:renewStudentId and (c.roleName='ROLE_PARENT1') and a.isAdmissionCancelled='1' and a.isApproval='0' ")
//	List<FCMTokenBean> getToken(Integer renewStudentId);

	@Query(value="SELECT coalesce(MAX(a.paymentGatewayId),0)+1 from FeePaymentGatewayModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxAssignToId(Integer schoolId);

	@Query(value="SELECT coalesce(MAX(a.paidId),0)+1 from FeePaidFeeModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxPaidId(Integer schoolId);

	@Query(value="SELECT coalesce(MAX(a.receiptId),0)+1 from FeeReceiptModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxReceiptId(Integer schoolId);

	@Query(value="SELECT coalesce(MAX(a.paidDueId),0)+1 from FeePaidDueModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxPaidDueId(Integer schoolId);

	@Query(value="SELECT coalesce(a.receiptNo,'0') from FeeReceiptModel  a  where a.schoolMasterModel.schoolid=:schoolId   order by a.receiptId desc ")
	List<String> getMaxReceiptNo(Integer schoolId);

	
	@Query("Select new com.ingenio.account.bean.StudentDetailsBean(COALESCE(a.renewStudentId,0), COALESCE(c.standardId,0),COALESCE(c.standardName,'') , COALESCE(d.divisionId,0), COALESCE(d.divisionName,'') , "
			+ "COALESCE(e.yearId,0), COALESCE(e.year,'')) FROM StudentStandardRenewModel a "
			+ "LEFT JOIN StudentMasterModel  b ON a.studentMasterModel.studentId=b.studentId  "
			+ "LEFT JOIN StandardMasterModel c ON c.standardId=a.standardMasterModel.standardId "
			+ "LEFT JOIN DivisionMasterModel d ON d.divisionId=a.divisionMasterModel.divisionId "
			+ "LEFT JOIN YearMasterModel e ON e.yearId=a.yearMasterModel.yearId "
			+ "WHERE b.studentId=:studentId and b.isAdmissionCancelled='1' and b.isAdmissionCancelled='1' and b.isApproval='0'  ")
	List<StudentDetailsBean> getStudentDetails(Integer studentId);

	@Query(value="SELECT (a.feeChallanBankDetailsModel.bankId) from FeeBankToHeadSettingModel a where a.feeHeadMasterModel.headId=:headId ")
	Integer getBankId(Integer headId);

}
