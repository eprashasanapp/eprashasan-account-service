package com.ingenio.account.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.account.bean.FeeBean;


@Repository
@SuppressWarnings({ "unchecked", "deprecation" })
public class FeeReportsRepositoryImpl {

	@PersistenceContext
	private EntityManager entitymanager;

	public String getVoucherType(Integer csSchoolId, Integer schoolId, Integer type) {
		// TODO Auto-generated method stub
		 StringBuilder feeReportQuery = new StringBuilder();
			feeReportQuery.append("select a.voucherName as voucherName  from acc_add_voucher_type a where a.schoolid="+schoolId+" and a.cschoolID="+csSchoolId+" and a.vTypeID="+type );
			Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
		
			List<FeeBean> list = query.unwrap( org.hibernate.query.Query.class )
					 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
					 .getResultList();
			
			return list.get(0).getVoucherName();
	}
	
	

}
