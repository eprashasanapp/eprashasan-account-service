package com.ingenio.account.repository.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.account.bean.AccAddVoucherTypeBean;
import com.ingenio.account.bean.FeesToAccountLinkBean;
import com.ingenio.account.bean.FeesettingBean;

@Repository
@SuppressWarnings({ "unchecked", "deprecation" })
public class FeesLinkToAccountRepositoryImpl {
	
	@PersistenceContext
	private EntityManager entitymanager;

	public List<FeesToAccountLinkBean> getStudentDetailsForLedgers(String regNo, Integer yearId, String receiptId,
			Integer schoolId) {
		List<FeesToAccountLinkBean> getPaidFeeReportList = new ArrayList<>();
		StringBuffer getPaidFeeReportQuery=new StringBuffer();
		//Integer schoolId=sessionUserBean.getSchoolId();
		try{
			if(receiptId.equals("0")) {
				getPaidFeeReportQuery.append("select cast(a.receiptID as char) as receiptId,d.renewStudentId as renewstudentId, a.paidID as paidId, a.headId,a.subheadId,e.stud_id as studentId,e.Student_RegNo as registartionNumber,concat(Ifnull(e.studInitialName,''),' ',ifnull(e.studLName,''),' ',ifnull(e.studFName,''),' ',ifnull(e.studMName,'')) as studentName, ");
				getPaidFeeReportQuery.append("f.year as yearName,d.standardId, g.standardName,h.receiptNo,a.payFee-ifnull(a.discount,0) as paidFee,IFNULL(b.dueFee,0) as paidFine,a.paidfeeDate as receiptDate, ");
				getPaidFeeReportQuery.append("IFNULL(i.payTypeName,'Cash') as payType,a.DDCHQno,h.bankId, j.bankName,j.bankAccNo,k.headName as headName,l.subheadName as subHeadName from fee_paidfeetable a left join fee_paid_duetable b on a.feeSetMultiID=b.feeSetMultiID and b.isDel='0' and b.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join fee_studentfee c on c.studFeeID=a.studFeeID and c.isDel='0' and c.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN fee_setting m on m.feeSettingID=c.feeSettingID and m.isDel='0' and m.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=c.studRenewID and m.yearID=d.yearId and d.isDel='0' and d.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join student_master e on e.stud_id=d.studentId and e.isDel='0' and e.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join yearmaster f on f.yearId=d.yearId and f.isDel='0' and f.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join standardmaster g on g.standardId=d.standardId and g.isDel='0' and g.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join fee_receipt h on h.receiptID=a.receiptID and h.isDel='0' and h.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join fee_pay_type i on i.payTypeID=a.payType and i.isDel='0' and i.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN challanbankdetails j on j.bankId=h.bankId and j.isDel='0' and j.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN fee_head_master k on k.headID=a.headID and k.isDel='0' and k.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join fee_subhead_master l on l.subheadID=a.subheadID and l.isDel='0' and l.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("where a.schoolid='"+schoolId+"' and a.isDel='0' and h.isDel='0'  and e.isAdmissionCancelled='1' and e.isApproval='0'  and h.receiptID=(select max(a1.receiptID) from fee_receipt a1 ");
				getPaidFeeReportQuery.append("left join fee_paidfeetable b1 on a1.receiptID=b1.receiptID and b1.isDel='0' and b1.schoolid='"+schoolId+"'  left join fee_studentfee c1 ");
				getPaidFeeReportQuery.append("on b1.studFeeID=c1.studFeeID and c1.isDel='0' and c1.schoolid='"+schoolId+"'  left join fee_setting d1 on d1.feeSettingID=c1.feeSettingID and d1.isDel='0' and d1.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join student_standard_renew e1 on e1.studentId=c1.studRenewID and e1.isDel='0' and e1.schoolid='"+schoolId+"'  and e1.standardId=d1.standardID ");
				getPaidFeeReportQuery.append("left join student_master f1 on f1.stud_id=e1.studentId and f1.isDel='0' and f1.schoolid='"+schoolId+"'  where a1.schoolid='"+schoolId+"' and a1.isDel='0'  and f1.Student_RegNo='"+regNo+"' and f1.isAdmissionCancelled='1' and f1.isApproval='0' )  ");
				
				if(!yearId.equals("0")) {
					getPaidFeeReportQuery.append("and d.yearId="+yearId+" ");
				}
				
				getPaidFeeReportQuery.append("group by headId,subheadId ");
				Query query = entitymanager.createNativeQuery(getPaidFeeReportQuery.toString());
				getPaidFeeReportList = query.unwrap(org.hibernate.query.Query.class)
						.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			}
			else {
				getPaidFeeReportQuery.append("select cast(a.receiptID as char) as receiptId,d.renewStudentId as renewstudentId, a.paidID as paidId, a.headId,a.subheadId,e.stud_id as studentId,e.Student_RegNo as registartionNumber,concat(Ifnull(e.studInitialName,''),' ',ifnull(e.studLName,''),' ',ifnull(e.studFName,''),' ',ifnull(e.studMName,'')) as studentName, ");
				getPaidFeeReportQuery.append("f.year as yearName,d.standardId, g.standardName,h.receiptNo,a.payFee-ifnull(a.discount,0) as paidFee,IFNULL(b.dueFee,0) as paidFine,a.paidfeeDate as receiptDate, ");
				getPaidFeeReportQuery.append("IFNULL(i.payTypeName,'Cash') as payType,a.DDCHQno,h.bankId, j.bankName,j.bankAccNo,k.headName as headName,l.subheadName as subHeadName from fee_paidfeetable a left join fee_paid_duetable b on a.feeSetMultiID=b.feeSetMultiID and  b.isDel='0' and b.schoolid='"+schoolId+"'   ");
				getPaidFeeReportQuery.append("left join fee_studentfee c on c.studFeeID=a.studFeeID and c.isDel='0' and c.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN fee_setting m on m.feeSettingID=c.feeSettingID and m.isDel='0' and m.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=c.studRenewID and m.yearID=d.yearId and d.isDel='0' and d.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join student_master e on e.stud_id=d.studentId and e.isDel='0' and e.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join yearmaster f on f.yearId=d.yearId and f.isDel='0' and f.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join standardmaster g on g.standardId=d.standardId and g.isDel='0' and g.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join fee_receipt h on h.receiptID=a.receiptID and h.isDel='0' and h.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join fee_pay_type i on i.payTypeID=a.payType and i.isDel='0' and i.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN challanbankdetails j on j.bankId=h.bankId and j.isDel='0' and j.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("LEFT JOIN fee_head_master k on k.headID=a.headID and k.isDel='0' and k.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("left join fee_subhead_master l on l.subheadID=a.subheadID and l.isDel='0' and l.schoolid='"+schoolId+"'  ");
				getPaidFeeReportQuery.append("where a.schoolid='"+schoolId+"' and a.isDel='0' and h.isDel='0' and e.isAdmissionCancelled='1' and e.isApproval='0'  and h.receiptID="+receiptId+" group by headId,subheadId ");
				Query query = entitymanager.createNativeQuery(getPaidFeeReportQuery.toString());
				getPaidFeeReportList = query.unwrap(org.hibernate.query.Query.class)
						.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
				
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return getPaidFeeReportList;
		}

	public List<FeesToAccountLinkBean> getCurrentUserForLogin(Integer schoolId) {
		List<FeesToAccountLinkBean> getPaidFeeReportList = new ArrayList<>();
		StringBuffer getUserDetails = new StringBuffer();
		// school alias changes - srk
		getUserDetails.append("SELECT a.schoolid AS schoolId,a.schoolKey AS schoolKey FROM school_master  a WHERE a.schoolid="+schoolId+" ");
		Query query = entitymanager.createNativeQuery(getUserDetails.toString());
		return getPaidFeeReportList = query.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
		
	}

	public List<FeesettingBean> getFeesReceiptsettingForUnit(int settingId, Integer schoolId) {
		List<FeesettingBean> list;
		try {
			Query query = entitymanager.createNativeQuery("select cast(a.setting_flag as char) as settingFlag from fees_receipt_setting a  where a.schoolid='"+schoolId+"' and a.receipt_settingId='"+settingId+"' and a.isDel='0' ");
			list = query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesettingBean.class)).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
		if(list.isEmpty()){
			return null;
		}else{
			return list;
		}
	}

	public List<FeesToAccountLinkBean> getTypeOfUnitIdStdWise(Integer standardId, Integer schoolId) {
		StringBuffer fetchBasicDetailsQuery = new StringBuffer();
		try {
			fetchBasicDetailsQuery.append("select b.unitMappAccId as unitMappAccId,b.typeOfUnitId as typeOfUnitId,b.subheadWiseId as subheadId,b.studentWiseId as studentId,b.classWiseId as classId from standardmaster a ");
			fetchBasicDetailsQuery.append("left join tyep_of_unit_mapp_to_acc b on a.typeOfUnitId=b.typeOfUnitId ");
			fetchBasicDetailsQuery.append("where a.schoolid='"+schoolId+"' and a.standardId='"+standardId+"' ");
			Query query = entitymanager.createNativeQuery(fetchBasicDetailsQuery.toString());
			return  query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	@Transactional
	public int getVouherId(String voucherName, String csSchoolIdForStudent, Integer schoolId) {
		try{
			String getMaxReceiptQuery="select vTypeID as vtypeId from acc_add_voucher_type where schoolid='"+schoolId+"' and isDel='0'  and voucherName='"+voucherName+"' and cschoolID='"+csSchoolIdForStudent+"' ";
			Query query = entitymanager.createNativeQuery(getMaxReceiptQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			
			Integer vTypeID = 0;
			if(list.size()!=0) {
				vTypeID =list.get(0).getVtypeId();
			}
			return vTypeID;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
			
		}
	}

	public int getMaxReceiptId(Integer schoolId) {
		StringBuffer getMaxReceiptQuery=new StringBuffer();
		try{
			getMaxReceiptQuery.append("select cast(MAX(cast(SUBSTRING(a.invNo,4) as signed)) as char) as maxRecieptId1 from acc_journal_day_book_entry a where a.schoolid='"+schoolId+"' and a.isDel='0' and a.invNo like '%RE%'");
			Query query = entitymanager.createNativeQuery(getMaxReceiptQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			int maxReceiptId = 0;
			if(list.size()!=0) {
				maxReceiptId =Integer.parseInt(list.get(0).getMaxRecieptId1());
			}
			return maxReceiptId;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
			
		}
		
	}

	public List getLedgerId(String tableName,String columnName,String columnValue,String csSchoolId, Integer schoolId) {
		try {
			String que="select a.ledgerID,a.ledgerName from acc_create_ledger a left join "+tableName+" b on a.ledgerId = b.ledgerId  where b.schoolid='"+schoolId+"' and b.isDel='0'  and b."+columnName+"="+columnValue+" and a.cschoolID='"+csSchoolId+"' ";
			 return entitymanager.createNativeQuery(que.toString()).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public List getLedgerIdFromLedgerName(String ledgerName,String csSchoolId, Integer schoolId) {
		try {
			String que="select ledgerId,ledgerName from acc_create_ledger where schoolid='"+schoolId+"' and isDel='0'  and ledgerName='"+ledgerName+"' and cschoolID='"+csSchoolId+"' ";
			 return entitymanager.createNativeQuery(que.toString()).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public FeesToAccountLinkBean checkRecord(Integer schoolId,int voucherTypeId, Integer debitLedger, Integer creditLedger, Integer renewstudentId,String narration) {
		StringBuffer selectQuery=new StringBuffer();
		try{
			selectQuery.append("select IFNULL(a.journalID,0) as journalId,cast(a.drAmount as char) as amount,"
					+ "cast(a.narration as char) as narration, IFNULL(a.paymentDetails,'') as paymentDetails"
					+ " from acc_journal_day_book_entry a where a.vTypeID='"+voucherTypeId+"' "
					+ "and a.debitLedger='"+debitLedger+"' and a.creditLedger='"+creditLedger+"' and a.schoolid='"+schoolId+"' "
					+ "and a.studentId='"+renewstudentId+"' and a.narration like '%"+narration+"%' and a.isDel='0' ");
			 Query query = entitymanager.createNativeQuery(selectQuery.toString());
			 List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
						.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			if(CollectionUtils.isNotEmpty(list)){
				return list.get(0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void editVoucherSeries(Integer vt, String voucherSeries, Integer schoolId) {  // used
		try {
			int query = entitymanager.createNativeQuery("update AccAddVoucherType set voucherSeries='"+voucherSeries+"',isEdit='1',editDate=CURDATE() where schoolid='"+schoolId+"' and isDel='0' and vtypeId='"+vt+"'").executeUpdate();
		} catch (Exception e) {
		}		
	}

	public int isDepreciationLager(String cschoolId, String debitLedger, Integer schoolId) {
		List list = new ArrayList();
		Integer ledgerId=0;
		StringBuffer isDepreciationLagerQuery= new StringBuffer();
		try {
			isDepreciationLagerQuery.append("select a.ledgerID from acc_create_ledger a ")
			.append("left outer join acc_add_group_and_subgroup b on b.groupSubgroupID=a.groupSubgroupID and b.isDel=a.isDel and b.schoolid='"+schoolId+"'  ")
			.append("left outer join acc_add_group_and_subgroup1 c on c.groupSubgroup1ID=b.groupSubgroup1ID and c.isDel=b.isDel and c.schoolid='"+schoolId+"'  ")
			.append("where a.schoolid='"+schoolId+"' and a.isDel='0' ")
			.append("and a.cschoolID='"+cschoolId+"' ")
			.append("and c.groupSubgroup1ID='5' ")
			.append("and a.ledgerID='"+debitLedger+"'"); 
			 Query query = entitymanager.createNativeQuery(isDepreciationLagerQuery.toString());
			 list=query.unwrap(org.hibernate.query.Query.class)
						.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			if(list.size()>0){
				ledgerId=Integer.parseInt(""+list.get(0));
			}
			return ledgerId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}finally{
			
		} 
	}

	public int isExistLedger(String cschoolId, String debitLedger, Integer schoolId) {
		List list = new ArrayList();
		Integer ledgerId=0;
		StringBuffer isExistLedgerQuery= new StringBuffer();
		isExistLedgerQuery.append("select a.journalID from acc_journal_day_book_entry a ")
		.append("where a.schoolid='"+schoolId+"' and a.isDel='0' ")
		.append("and a.cschoolID='"+cschoolId+"' ")
		.append("and a.debitLedger='3' ")
		.append("and a.creditLedger='"+debitLedger+"' ")
		.append("and a.entryOf='0' and a.bdFlag='0' "); 			
		try {
			 Query query = entitymanager.createNativeQuery(isExistLedgerQuery.toString());
			 list=query.unwrap(org.hibernate.query.Query.class)
						.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			if(list.size()>0){
				ledgerId=Integer.parseInt(""+list.get(0));
			}
			return ledgerId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		finally{
			
		} 
	}

	public List<AccAddVoucherTypeBean> getVoucherType(String cschoolId, String Journal, Integer schoolId) {
		StringBuffer selectQuery=new StringBuffer();
	try{
		selectQuery.append("SELECT a.vTypeID AS vTypeID,a.voucherName AS voucherName,a.voucherSeries AS voucherSeries,a.debit AS debit,a.credit AS credit,a.isDel AS isDel FROM acc_add_voucher_type a WHERE a.schoolid='"+schoolId+"' and a.isDel='0' and a.cschoolId='"+cschoolId+"' and a.voucherName='"+Journal+"' ");
				
		 Query query = entitymanager.createNativeQuery(selectQuery.toString());
		 List<AccAddVoucherTypeBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(AccAddVoucherTypeBean.class)).getResultList();
		if(CollectionUtils.isNotEmpty(list)){
			return list;
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return null;}

	public void updateEntries(BigInteger journalId, int voucherTypeId, Integer debitLedger, Integer creditLedger, String narration, 
			String paymentDetails, Double sumOfFees, Integer renewStudentId,Integer schoolId) {
		StringBuffer selectQuery=new StringBuffer();
		try{
			selectQuery.append("update acc_journal_day_book_entry a set a.drAmount='"+sumOfFees+"',a.narration='"+narration+"'  where a.vTypeID='"+voucherTypeId+"' "
					+ "and a.debitLedger='"+debitLedger+"' and a.creditLedger='"+creditLedger+"' and a.journalID='"+journalId+"' and a.entryOf='"+0+"'  and a.studentId='"+renewStudentId+"' ");
			entitymanager.createNativeQuery(selectQuery.toString()).executeUpdate();
			
			selectQuery=new StringBuffer();
			selectQuery.append("update acc_journal_day_book_entry a set a.crAmount='"+sumOfFees+"',a.narration='"+narration+"'  where a.vTypeID='"+voucherTypeId+"' "
					+ "and a.creditLedger='"+debitLedger+"' and a.debitLedger='"+creditLedger+"' and a.entryOf='"+1+"'  and a.studentId='"+renewStudentId+"' ");
			entitymanager.createNativeQuery(selectQuery.toString()).executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public int getStudentRenewId(Integer studentId) {
		try{
			StringBuffer qu=new StringBuffer();
			qu.append("select a.renewstudentId from student_standard_renew a where a.studentId='"+studentId+"'  and a.isDel='0' ");
			List list=entitymanager.createNativeQuery(qu.toString()).getResultList();
			if(CollectionUtils.isNotEmpty(list)){
				return Integer.parseInt(""+list.get(0));
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getDisount(Integer studentId) {
		try{
			StringBuffer qu=new StringBuffer();
			qu.append("SELECT sum(b.discount) ");
			qu.append("FROM fee_studentfee a ");
			qu.append("LEFT JOIN fee_paidfeetable b ON a.studFeeID=b.studFeeID AND a.isDel='0' AND a.schoolID=b.schoolID ");
			qu.append("WHERE a.studRenewID='"+studentId+"'  AND a.isDel='0' ");
			List list=entitymanager.createNativeQuery(qu.toString()).getResultList();
			if(CollectionUtils.isNotEmpty(list)){
				return Double.parseDouble(""+list.get(0));
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getTax(Integer studentId, String receiptId) {
		try{
			StringBuffer qu=new StringBuffer();
			qu.append("SELECT sum(b.tax) ");
			qu.append("FROM fee_studentfee a ");
			qu.append("LEFT JOIN fee_paidfeetable b ON a.studFeeID=b.studFeeID AND a.isDel='0' AND a.schoolID=b.schoolID ");
			qu.append("WHERE a.studRenewID='"+studentId+"' AND a.isDel='0' and b.receiptID='"+receiptId+"' ");
			List list=entitymanager.createNativeQuery(qu.toString()).getResultList();
			if(CollectionUtils.isNotEmpty(list)){
				return Double.parseDouble(""+list.get(0));
			}				
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getExcessFees(Integer studentId, String receiptId) {
		try{
			StringBuffer qu=new StringBuffer();
			qu.append("SELECT IFNULL(SUM(b.excessFees),0) ");
			qu.append("FROM fee_studentfee a ");
			qu.append("LEFT JOIN fee_excess_studentwise b ON a.studRenewID=b.studId AND a.isDel='0' AND a.schoolID=b.schoolID ");
			qu.append("WHERE a.studRenewID='"+studentId+"' AND a.isDel='0' and b.receiptID='"+receiptId+"' ");
			List list=entitymanager.createNativeQuery(qu.toString()).getResultList();
			if(CollectionUtils.isNotEmpty(list)){
				return Double.parseDouble(""+list.get(0));
			}				
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public List<FeesToAccountLinkBean> getheadSubheadReceiptEntries(Integer schoolId, String fromDate, String toDate,
			Integer receiptID, String regNo, String yearId) {
		StringBuffer getPaidFeeReportQuery=new StringBuffer();
		getPaidFeeReportQuery.append("SELECT CAST(a.receiptID AS CHAR) AS receiptId,d.renewStudentId AS renewstudentId, a.paidID AS paidId, "
				+ "a.headId,a.subheadId,e.stud_id AS studentId,e.Student_RegNo AS registartionNumber, "
				+ "CONCAT(IFNULL(e.studInitialName,''),' ', IFNULL(e.studLName,''),' ', IFNULL(e.studFName,''),' ',  "
				+ "IFNULL(e.studMName,'')) AS studentName, f.year AS yearName,d.standardId, g.standardName,h.receiptNo, "
				+ "a.payFee AS paidFee, IFNULL(b.dueFee,0) AS paidFine,a.paidfeeDate AS receiptDate, "
				+ "IFNULL(i.payTypeName,'Cash') AS payType,IFNULL(a.DDCHQno,'000') as DDCHQno ,h.bankId, j.bankName,j.bankAccNo, "
				+ "k.headName AS headName,l.subheadName AS subHeadName,cast(h.payTypeOrPrintFlag as char) as payTypeOrPrintFlag,cast(l.isScholarshipFlag as char) as scholarshipFlag FROM fee_paidfeetable a "
				+ "LEFT JOIN fee_paid_duetable b ON a.feeSetMultiID=b.feeSetMultiID AND b.isDel='0' AND b.schoolid='"+schoolId+"' "
				+ "LEFT JOIN fee_studentfee c ON c.studFeeID=a.studFeeID AND c.isDel='0' AND c.schoolid='"+schoolId+"' "
				+ "LEFT JOIN fee_setting m ON m.feeSettingID=c.feeSettingID AND m.isDel='0' AND m.schoolid='"+schoolId+"' "
				+ "LEFT JOIN student_standard_renew d ON d.studentId=c.studRenewID AND m.yearID=d.yearId AND d.isDel='0' AND d.schoolid='"+schoolId+"' "
				+ "LEFT JOIN student_master e ON e.stud_id=d.studentId AND e.isDel='0' AND e.schoolid='"+schoolId+"' "
				+ "LEFT JOIN yearmaster f ON f.yearId=d.yearId AND f.isDel='0' AND f.schoolid='"+schoolId+"' "
				+ "LEFT JOIN standardmaster g ON g.standardId=d.standardId AND g.isDel='0' AND g.schoolid='"+schoolId+"' "
				+ "LEFT JOIN fee_receipt h ON h.receiptID=a.receiptID AND h.isDel='0' AND h.schoolid='"+schoolId+"' "
				+ "LEFT JOIN fee_pay_type i ON i.payTypeID=a.payType AND i.isDel='0' AND i.schoolid='"+schoolId+"' "
				+ "LEFT JOIN challanbankdetails j ON j.bankId=h.bankId AND j.isDel='0' AND j.schoolid='"+schoolId+"' "
				+ "LEFT JOIN fee_head_master k ON k.headID=a.headID AND k.isDel='0' AND k.schoolid='"+schoolId+"' "
				+ "LEFT JOIN fee_subhead_master l ON l.subheadID=a.subheadID AND l.isDel='0' AND l.schoolid='"+schoolId+"' "
				+ "WHERE a.schoolid='"+schoolId+"' AND h.isDel='0' AND a.isDel='0' AND e.isAdmissionCancelled='1' AND e.isApproval='0' "
				+ "and (h.payTypeOrPrintFlag=1 or (h.payTypeOrPrintFlag=2 and h.returnFlag=1)) "
 				+ "and d.renewStudentId is not null  ");
				
		if(StringUtils.isNotEmpty(fromDate)) {
			getPaidFeeReportQuery.append(" and DATE_FORMAT(STR_TO_DATE(a.paidfeeDate,'%d-%m-%Y'),'%Y-%m-%d')"
					+ " BETWEEN  date('"+fromDate+"') AND  date('"+toDate+"') ");
		}
		if(receiptID!=0) {
			getPaidFeeReportQuery.append(" and h.receiptID="+receiptID+" ");
		}
		if(StringUtils.isNotEmpty(regNo) && StringUtils.isNotEmpty(yearId)) {
			getPaidFeeReportQuery.append(" and e.Student_RegNo='"+regNo+"' and f.yearId="+yearId+" ");
		}
		getPaidFeeReportQuery.append("GROUP BY d.yearId,headId,subheadId,a.receiptID order by d.renewStudentId ");
		 Query query = entitymanager.createNativeQuery(getPaidFeeReportQuery.toString());
		 List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
	return list;
	}

	public int getApprovalId(String regNo, Integer schoolId) {
		try{
			StringBuffer fetchBasicDetailsQuery = new StringBuffer();
			fetchBasicDetailsQuery.append("select b.scholarshipAppId from student_master a ");
			fetchBasicDetailsQuery.append("left join scholarship_approval b on a.stud_id=b.studId and a.schoolid=b.schoolid ");
			fetchBasicDetailsQuery.append("where a.schoolid='"+schoolId+"'   and a.Student_RegNo='"+regNo+"' ");
			List list=entitymanager.createNativeQuery(fetchBasicDetailsQuery.toString()).getResultList();
			if(list!=null && !list.isEmpty()){
				int studRenewId = Integer.parseInt(""+list.get(0));
				return studRenewId;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public List<FeesToAccountLinkBean> getsubHeadList(Integer schoolId, String fromDate, String toDate, String type,
			Integer inputReceiptId) {
		StringBuffer getPaidFeeReportQuery=new StringBuffer();
		getPaidFeeReportQuery.append("select Cast(DATE_FORMAT(c.cDate,'%d-%m-%y') as char) as receiptDate,d.studentId,d.renewstudentId,  CONCAT(IFNULL(e.studLName,'') ,' ',");
		getPaidFeeReportQuery.append("IFNULL(e.studFName,'')) as studentName,e.Student_RegNo as registartionNumber, d.yearId,IFNULL(g.year,'') as yearName,d.standardId,IFNULL(f.standardName,'') as standardName, ");
		getPaidFeeReportQuery.append("a.headID as headId,a.subheadID as subheadId,IFNULL(a.discount,0) as discount,i.headName,j.subheadName as subHeadName, ");
		getPaidFeeReportQuery.append("cast(SUM(IFNULL(a.assignFee,0)) as char) as assignFee,cast(SUM(IFNULL(a.fine,0)) as char) as assignFine from fee_setting_multi a ");
		getPaidFeeReportQuery.append("left join fee_setting b on a.feeSettingID=b.feeSettingID ");
		getPaidFeeReportQuery.append("left join fee_studentfee c on c.feeSettingID=a.feeSettingID ");
		getPaidFeeReportQuery.append("left join student_standard_renew d on d.studentId=c.studRenewID  and d.yearId=b.yearID ");
		getPaidFeeReportQuery.append("left join student_master e on e.stud_id=d.studentId ");
		getPaidFeeReportQuery.append("left join standardmaster f on f.standardId=d.standardId ");
		getPaidFeeReportQuery.append("left join fee_head_master i on i.headId=a.headId ");
		getPaidFeeReportQuery.append("left join fee_subhead_master j on j.subheadId=a.subheadId ");
		getPaidFeeReportQuery.append("left join yearmaster g on g.yearId=d.yearId where a.schoolId='"+schoolId+"' and d.renewstudentId is not null ");
		
		if(StringUtils.isNotEmpty(fromDate)) {
			getPaidFeeReportQuery.append(" and date_format(str_to_date(a.cdate,'%d-%m-%Y'),'%Y-%m-%d') between date('"+fromDate+"') and date('"+toDate+"') ");
		}
	
		
		getPaidFeeReportQuery.append("group by a.headId,a.subheadID,d.renewstudentId order by f.standardPriority,d.yearId, d.renewStudentId ");
		Query query = entitymanager.createNativeQuery(getPaidFeeReportQuery.toString());
		 List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
		 return list;
	}

	public int getMaxJournalId(Integer schoolId) {
		try{
			String getMaxReceiptQuery="select cast(MAX(CAST(SUBSTRING(a.invNo,4) AS SIGNED)) AS CHAR) AS maxRecieptId1 from acc_journal_day_book_entry a where a.schoolid='"+schoolId+"' and a.isDel='0' and a.bdFlag='0'  and a.invNo like '%Jo%'";
			Query query = entitymanager.createNativeQuery(getMaxReceiptQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			int maxReceiptId =0;
			if(list.size()!=0) {
				maxReceiptId =Integer.parseInt(list.get(0).getMaxRecieptId1());
			}
			return maxReceiptId;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
			
		}
	}

	public Integer getLedgerIdBySubHeadId(String subheadId, Integer schoolId) {
		try {
			List list = entitymanager.createNativeQuery("select a.ledgerID from acc_create_ledger a left join acc_create_ledger_to_subhead_mapp b on a.ledgerID=b.ledgerId and b.isDel='0' and b.schoolid='"+schoolId+"'  where b.subheadId="+subheadId+" and a.schoolid='"+schoolId+"' and a.isDel='0'  ").getResultList();
			if(CollectionUtils.isNotEmpty(list)) {
				return (Integer) list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List getHeadAndSubHeadDetails(String subHeadId, Integer schoolId) {
		try {
			StringBuffer que = new StringBuffer();
			que.append("select a.subheadName,a.headID,b.headName from fee_subhead_master a left join fee_head_master b on a.headID=b.headID and b.isDel='0' and b.schoolid='"+schoolId+"'  where a.schoolid='"+schoolId+"' and a.isDel='0' and a.subheadID='"+subHeadId+"' ");
			List list=entitymanager.createNativeQuery(que.toString()).getResultList();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public Double getReceiptWiseDiscount(String receiptId) {
		StringBuffer selectQuery=new StringBuffer();
		try{
			selectQuery.append("select sum(IFNULL(a.discount,0))  as discount from fee_paidfeetable a where a.receiptId='"+receiptId+"' ");
			Query query = entitymanager.createNativeQuery(selectQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
		    if(CollectionUtils.isNotEmpty(list)) {
		    	return list.get(0).getDiscount();
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public Integer getLedgerHeadId(String ledgerHeadName, String csSchoolId, Integer schoolId) {
		try {
			String que="select ledgerHeadId from acc_create_ledger_head where schoolid='"+schoolId+"' and isDel='0' and  ledgerHeadName='"+ledgerHeadName+"' and cschoolID='"+csSchoolId+"' ";
			Query query = entitymanager.createNativeQuery(que.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			int headId = 0;
			if(list.size()!=0) {
				headId =(int) list.get(0).getLedgerHeadId();
			}
			return headId;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Integer getGroupSubGroup(String groupName, String csSchoolId, Integer schoolId) {
		try {
			StringBuffer que = new StringBuffer();
			que.append("select a.groupSubgroupID as groupSubgroupID from acc_add_group_and_subgroup a left join acc_add_group_and_subgroup1 b ");
			que.append("on a.groupSubgroup1ID=b.groupSubgroup1ID and b.schoolid='"+schoolId+"' and b.isDel='0'  where a.schoolid='"+schoolId+"' and a.isDel='0'  and b.name='"+groupName+"' and a.cschoolID='"+csSchoolId+"' and a.groupSubgroup2ID is null ");
			Query query = entitymanager.createNativeQuery(que.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			int groupId = 0;
			if(list.size()!=0) {
				groupId =(int) list.get(0).getGroupSubgroupID();
			}
			return groupId;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Long getMaxjourNalId(Integer schoolId) {
		try{
			String getMaxReceiptQuery="SELECT max(a.journalID)+1 AS journalId FROM acc_journal_day_book_entry a where a.schoolid='"+schoolId+"' ";
			Query query = entitymanager.createNativeQuery(getMaxReceiptQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			String maxReceiptId = null;
			if(list.size()!=0) {
				maxReceiptId =""+list.get(0).getJournalId();
			}
			return Long.parseLong(maxReceiptId);
		}catch(Exception e){
			e.printStackTrace();
			return (long) 0;
		}
	}

	public int getMaxLegerId(Integer schoolId) {
		try{
			String getMaxReceiptQuery="SELECT cast(max(a.ledgerID)+1 as char) AS maxRecieptId1 FROM acc_create_ledger a where a.schoolid='"+schoolId+"' ";
			Query query = entitymanager.createNativeQuery(getMaxReceiptQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			String ledgerId = null;
			if(list.size()!=0) {
				ledgerId =""+list.get(0).getMaxRecieptId1();
			}
			return Integer.parseInt(ledgerId);
		}catch(Exception e){
			e.printStackTrace();
			return  0;
		}
	}

	public int getMaxLegerStudMappId(Integer schoolId) {
		try{
			String getMaxReceiptQuery="SELECT cast(max(a.ledgerStudentMappId)+1 as char) AS maxRecieptId1 FROM acc_create_ledger_to_student_mapp a where a.schoolid='"+schoolId+"' ";
			Query query = entitymanager.createNativeQuery(getMaxReceiptQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			String ledgerId = null;
			if(list.size()!=0) {
				ledgerId =""+list.get(0).getMaxRecieptId1();
			}
			return Integer.parseInt(ledgerId);
		}catch(Exception e){
			e.printStackTrace();
			return  0;
		}
	}

	public int getMaxAccDeprecationId(Integer schoolId) {
		try{
			String getMaxReceiptQuery="SELECT cast(max(a.depreID)+1 as char) AS maxRecieptId1 FROM acc_depreciation a where a.schoolid='"+schoolId+"' ";
			Query query = entitymanager.createNativeQuery(getMaxReceiptQuery.toString());
			List<FeesToAccountLinkBean> list=query.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.aliasToBean(FeesToAccountLinkBean.class)).getResultList();
			String ledgerId = null;
			if(list.size()!=0) {
				ledgerId =""+list.get(0).getMaxRecieptId1();
			}
			return Integer.parseInt(ledgerId);
		}catch(Exception e){
			e.printStackTrace();
			return  0;
		}
	}

}
