package com.ingenio.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.account.model.AccCreateLedger;

@Repository
public interface AccCreateLedgerRepository extends JpaRepository<AccCreateLedger, Integer>  {

}
