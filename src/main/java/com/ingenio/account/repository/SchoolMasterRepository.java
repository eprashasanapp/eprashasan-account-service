package com.ingenio.account.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.account.model.SchoolMasterModel;


public interface SchoolMasterRepository extends JpaRepository<SchoolMasterModel, Integer> {

	SchoolMasterModel findBySchoolid(Integer schoolId);
	
	List<SchoolMasterModel> findAll();
	
	
}
