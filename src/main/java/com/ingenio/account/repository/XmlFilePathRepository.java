package com.ingenio.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.account.model.XmlFilePathModel;


@Repository
public interface XmlFilePathRepository extends JpaRepository<XmlFilePathModel, Integer>  {

	@Transactional
	@Modifying
	@Query(value = "update XmlFilePathModel d set d.filePath=:downloadUrl WHERE d.schoolid =:schoolId and d.csSchoolid=:csSchoolId and d.xmlType=:xmlType ")
	void updateFilePath(Integer schoolId, Integer csSchoolId, String xmlType, String downloadUrl);

	
}
