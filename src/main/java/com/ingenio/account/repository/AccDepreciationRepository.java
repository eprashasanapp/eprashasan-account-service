package com.ingenio.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.account.model.AccDepreciation;

public interface AccDepreciationRepository extends JpaRepository<AccDepreciation, Integer>  {

}
