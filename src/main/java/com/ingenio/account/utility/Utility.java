package com.ingenio.account.utility;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Strings;

public class Utility {
	
	public static String convertInUTFFormat(String str) {
		if (StringUtils.isNotEmpty(str)&& !str.equals("null")) {
			try {
				str = new String(str.getBytes("iso-8859-1"), "UTF-8");
				return str;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	public static String generatePrimaryKey(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
		
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				newGenetatedKey=globalDbSchoolKey+("0000000000".substring(0, ("0000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
				System.out.println(newGenetatedKey);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
	
	public static String pushNotification(List<String> deviceTokenList,String title, String message1) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization", "key=AAAAwvdf7Qo:APA91bGSz4zXr2-CfrxPpPZtQLxyf1qU5vjLn9f_pCZex5_sD9q7xJjow8VdWK_O64TGyJ_8gmzK6Il8rPq0lv_GdENCng2acls-6fojq1BUlbI3EcIADU_9Rwfgl9BI0khV1uA8rcdA");
		JSONObject message = new org.json.JSONObject();
		
		try {
			message.put("registration_ids",deviceTokenList);
			message.put("priority", "high");
			JSONObject notification = new JSONObject();
			notification.put("title", title);
			notification.put("body", message1);
			message.put("notification", notification);
			
			JSONObject data = new JSONObject();
			data.put("title", title);
			data.put("body", message1);
			message.put("data",data);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		post.setEntity(new StringEntity(message.toString(), "UTF-8"));
		try {
			client.execute(post);
		} catch (Exception e) { 
			e.printStackTrace();
		}
	
		return "200";

	}
	
	public static String convertNumberToWord(String number) {
		String number1="";
		if(number!=null && number.length()>=5) {
			Double numberDouble =Double.parseDouble(number);
			number = "" + numberDouble.intValue(); 
			if(number.length()==5) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("K");
			}
			if(number.length()==6) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("L");
			}
			if(number.length()==7) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("L");
			}
				if(number.length()==8) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("Cr");
			}
			if(number.length()==9) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("Cr");
			}
			
			if(number1.equals("")) {
				return number;
			}
			return number1;
		}else {
			return number;
		}
	}
	
	public static String convertEnglishToOther(String num,String language) {
		String newStr = new String();
		if (!Strings.isNullOrEmpty(num)) {
			if (language.equals("1")) {
				return num;
			}
			if (!language.equals("1")) {
				if (language.equals("2")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							case '-':
								newStr += "-";
								break;
								
							case '.':
								newStr += ".";
								break;
							// English to Marathi
							case '1':
								newStr += "१";
								break;
							
							case '2':
								newStr += "२";
								break;
							
							case '3':
								newStr += "३";
								break;
							
							case '4':
								newStr += "४";
								break;
							
							case '5':
								newStr += "५";
								break;
							
							case '6':
								newStr += "६";
								break;
							
							case '7':
								newStr += "७";
								break;
							
							case '8':
								newStr += "८";
								break;
							
							case '9':
								newStr += "९";
								break;
							
							case '0':
								newStr += "०";
								break;
							
							case '१':
								newStr += "१";
								break;
							
							case '२':
								newStr += "२";
								break;
							
							case '३':
								newStr += "३";
								break;
							
							case '४':
								newStr += "४";
								break;
							
							case '५':
								newStr += "५";
								break;
							
							case '६':
								newStr += "६";
								break;
							
							case '७':
								newStr += "७";
								break;
							
							case '८':
								newStr += "८";
								break;
							case '९':
								newStr += "९";
								break;
							
							case '०':
								newStr += "०";
								break;
						}
					}
				}
				
				if (language.equals("3")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Hindi
							case '1':
								newStr += "१";
								break;
							case '2':
								newStr += "२";
								break;
							case '3':
								newStr += "३";
								break;
							case '4':
								newStr += "४";
								break;
							case '5':
								newStr += "५";
								break;
							case '6':
								newStr += "६";
								break;
							case '7':
								newStr += "७";
								break;
							case '8':
								newStr += "८";
								break;
							case '9':
								newStr += "९";
								break;
							case '0':
								newStr += "०";
								break;
							case '१':
								newStr += "१";
								break;
							case '२':
								newStr += "२";
								break;
							case '३':
								newStr += "३";
								break;
							case '४':
								newStr += "४";
								break;
							case '५':
								newStr += "५";
								break;
							case '६':
								newStr += "६";
								break;
							case '७':
								newStr += "७";
								break;
							case '८':
								newStr += "८";
								break;
							case '९':
								newStr += "९";
								break;
							case '०':
								newStr += "०";
								break;
						}
					}
				}
				
				if (language.equals("4")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Gujrati
							case '1':
								newStr += "૧";
								break;
							
							case '2':
								newStr += "૨";
								break;
							
							case '3':
								newStr += "૩";
								break;
							
							case '4':
								newStr += "૪";
								break;
							
							case '5':
								newStr += "૫";
								break;
							
							case '6':
								newStr += "૬";
								break;
							
							case '7':
								newStr += "૭";
								break;
							
							case '8':
								newStr += "૮";
								break;
							
							case '9':
								newStr += "૯";
								break;
							
							case '0':
								newStr += "૦";
								break;
							
							case '૧':
								newStr += "૧";
								break;
							
							case '૨':
								newStr += "૨";
								break;
							
							case '૩':
								newStr += "૩";
								break;
							
							case '૪':
								newStr += "૪";
								break;
							
							case '૫':
								newStr += "૫";
								break;
							
							case '૬':
								newStr += "૬";
								break;
							
							case '૭':
								newStr += "૭";
								break;
							
							case '૮':
								newStr += "૮";
								break;
							case '૯':
								newStr += "૯";
								break;
							
							case '૦':
								newStr += "૦";
								break;
						}
					}
				}
			}
		}else {
			return num;
		}
		return newStr;
	}
	
	public static String getLanguage(Integer langId) {
		String language="en";
		switch (langId) {
		case 1:
			language="en";
			break;
		
		case 2:
			language="mr";
			break;
		
		case 3:
			language="gu";
			break;
		
		case 4:
			language="hi";
			break;
	}
		return language;
	}
	
	 public static String convertOtherToEnglish(String num) {
			String newStr = new String();
			if (StringUtils.isNotEmpty(num) && !num.equals("null")) {
				for (int i = 0; i < num.length(); i++ ) {
					char str = num.charAt(i);				
					switch (str) {
						case '-':
							newStr += "-";
							break;
							
						case '.':
							newStr += ".";
							break;
						
						// Start English
						case '1':
							newStr += "1";
							break;
						
						case '2':
							newStr += "2";
							break;
						
						case '3':
							newStr += "3";
							break;
						
						case '4':
							newStr += "4";
							break;
						
						case '5':
							newStr += "5";
							break;
						
						case '6':
							newStr += "6";
							break;
						
						case '7':
							newStr += "7";
							break;
						
						case '8':
							newStr += "8";
							break;
						
						case '9':
							newStr += "9";
							break;
						
						case '0':
							newStr += "0";
							break;
						
						// Start Marathi to English
						case '१':
							newStr += "1";
							break;
						
						case '२':
							newStr += "2";
							break;
						
						case '३':
							newStr += "3";
							break;
						
						case '४':
							newStr += "4";
							break;
						
						case '५':
							newStr += "5";
							break;
						
						case '६':
							newStr += "6";
							break;
						
						case '७':
							newStr += "7";
							break;
						
						case '८':
							newStr += "8";
							break;
						
						case '९':
							newStr += "9";
							break;
						
						case '०':
							newStr += "0";
							break;
						
						// Start Gujrati to English
						case '૧':
							newStr += "1";
							break;
						
						case '૨':
							newStr += "2";
							break;
						
						case '૩':
							newStr += "3";
							break;
						
						case '૪':
							newStr += "4";
							break;
						
						case '૫':
							newStr += "5";
							break;
						
						case '૬':
							newStr += "6";
							break;
						
						case '૭':
							newStr += "7";
							break;
						
						case '૮':
							newStr += "8";
							break;
						
						case '૯':
							newStr += "9";
							break;
						
						case '૦':
							newStr += "0";
							break;
					}
				}
			}else {
				return "";
			}
			return newStr;
		}
}
