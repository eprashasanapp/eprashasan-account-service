package com.ingenio.account.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.collections4.CollectionUtils;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.ingenio.account.bean.UploadFileBean;
import com.ingenio.account.bean.XMLResponseBean;
import com.ingenio.account.bean.XmlFilePathBean;
import com.ingenio.account.config.ConfigurationProperties;
import com.ingenio.account.model.XmlFilePathModel;
import com.ingenio.account.repository.AccountRepository;
import com.ingenio.account.repository.FeeRepository;
import com.ingenio.account.repository.SchoolMasterRepository;
import com.ingenio.account.repository.XmlFilePathRepository;
import com.ingenio.account.repository.impl.FeeReportsRepositoryImpl;
import com.ingenio.account.service.AccountsService;
import com.ingenio.account.utility.Utility;

@Component
public class AccountServiceImpl implements AccountsService {

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	XmlFilePathRepository xmlFilePathRepository;
	
	@Autowired
	FeeRepository feeRepository;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	FeeReportsRepositoryImpl feeReportsRepositoryImpl;
	
	@Autowired
	AmazonS3 amazonS3;
	
	
	@Override
	public String generateTallyMasters(Integer schoolId, Integer csSchoolId) {
		 try {
			//List<XMLResponseBean> groupList  = accountRepository.getRecordsForGroupMasterTally(schoolId);
			
			String[] groupArray = {
				"Branch / Divisions","Capital Account","Current Assets","Current Liabilities","Direct Expenses","Direct Incomes",
				"Fixed Assets","Indirect Expenses","Indirect Incomes","Investments","Loans (Liability)","Purchase Accounts","Sales Accounts","Suspense A/c",
				"Bank Accounts","Bank OD A/c","Cash-in-Hand","Deposits (Asset)","Duties &amp; Taxes","Loans &amp; Advances (Asset)","Provisions","Reserves &amp; Surplus",
				"Secured Loans","Stock-in-Hand","Sundry Creditors","Sundry Debtors","Unsecured Loans"
					
			};
			
			 DocumentBuilderFactory dbFactory =  DocumentBuilderFactory.newInstance();
			 DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			 Document doc = dBuilder.newDocument();
			 
			 
				// root element
	         Element ENVELOPE = doc.createElement("ENVELOPE");
	         doc.appendChild(ENVELOPE);

	         // Header element
	         Element HEADER = doc.createElement("HEADER");
	         ENVELOPE.appendChild(HEADER);
	         

			 // tallyRequest element
			 Element TALLYREQUEST = doc.createElement("TALLYREQUEST");
			 TALLYREQUEST.appendChild(doc.createTextNode("Import Data"));
			 HEADER.appendChild(TALLYREQUEST);

			 
			 Element BODY = doc.createElement("BODY");
			 ENVELOPE.appendChild(BODY);
			 
			 
			 Element IMPORTDATA = doc.createElement("IMPORTDATA");
			 BODY.appendChild(IMPORTDATA);
			 
			 
			 Element REQUESTDESC = doc.createElement("REQUESTDESC");
			 IMPORTDATA.appendChild(REQUESTDESC);
				 
		 
			 Element REPORTNAME = doc.createElement("REPORTNAME");
			 REPORTNAME.appendChild(doc.createTextNode("All Masters"));
			 REQUESTDESC.appendChild(REPORTNAME);
					 
					 
			 Element STATICVARIABLES = doc.createElement("STATICVARIABLES");
			 REQUESTDESC.appendChild(STATICVARIABLES);

						
			 Element SVCURRENTCOMPANY = doc.createElement("SVCURRENTCOMPANY");
			 SVCURRENTCOMPANY.appendChild(doc.createTextNode("Ingenio"));
			 STATICVARIABLES.appendChild(SVCURRENTCOMPANY);
						
			 Element REQUESTDATA = doc.createElement("REQUESTDATA");
			 IMPORTDATA.appendChild(REQUESTDATA);
			
			 // This content will be replaced by master file of groups
			 Element GroupTALLYMESSAGE = doc.createElement("TALLYGroupMESSAGE");
			 REQUESTDATA.appendChild(GroupTALLYMESSAGE);
			 
			 String groupNames[] = {"GUID","PARENT","GRPDEBITPARENT","GRPCREDITPARENT","ISBILLWISEON","ISCOSTCENTRESON","ISADDABLE","ISUPDATINGTARGETID",
					 				"ASORIGINAL","ISSUBLEDGER","ISREVENUE","AFFECTSGROSSPROFIT","ISDEEMEDPOSITIVE","TRACKNEGATIVEBALANCES","ISCONDENSED",
					 				"AFFECTSSTOCK","ISGROUPFORLOANRCPT","ISGROUPFORLOANPYMNT","ISRATEINCLUSIVEVAT","ISINVDETAILSENABLE","SORTPOSITION","ALTERID",
					 				"LANGUAGENAME.LIST"};
		/*	 
			 String groupNames[] = {"GUID","PARENT","GRPDEBITPARENT","GRPCREDITPARENT","ISBILLWISEON","ISCOSTCENTRESON","ISADDABLE","ISUPDATINGTARGETID",
		 				"ASORIGINAL","ISSUBLEDGER","ISREVENUE","AFFECTSGROSSPROFIT","ISDEEMEDPOSITIVE","TRACKNEGATIVEBALANCES","ISCONDENSED",
		 				"AFFECTSSTOCK","ISGROUPFORLOANRCPT","ISGROUPFORLOANPYMNT","ISRATEINCLUSIVEVAT","ISINVDETAILSENABLE","SORTPOSITION","ALTERID",
		 				"SERVICETAXDETAILS.LIST","VATDETAILS.LIST","SALESTAXCESSDETAILS.LIST","GSTDETAILS.LIST","LANGUAGENAME.LIST","XBRLDETAIL.LIST",
		 				"AUDITDETAILS.LIST","SCHVIDETAILS.LIST","EXCISETARIFFDETAILS.LIST","TCSCATEGORYDETAILS.LIST","TDSCATEGORYDETAILS.LIST",
		 				"GSTCLASSFNIGSTRATES.LIST","EXTARIFFDUTYHEADDETAILS.LIST"};
			 */
			/* for(int i=0;i<groupArray.length;i++) {
					Element TALLYMESSAGE = doc.createElement("TALLYMESSAGE");
					Attr attr = doc.createAttribute("xmlns:UDF");
					attr.setValue("TallyUDF");
					TALLYMESSAGE.setAttributeNode(attr);
					REQUESTDATA.appendChild(TALLYMESSAGE);
				 
				 	Element group = doc.createElement("GROUP");
			 		Attr voucherAttr = doc.createAttribute("NAME");
			 		voucherAttr.setValue(groupArray[i].toString());
			 		group.setAttributeNode(voucherAttr);
			 		
			 		voucherAttr = doc.createAttribute("RESERVEDNAME");
			 		voucherAttr.setValue(groupArray[i].toString());
			 		group.setAttributeNode(voucherAttr);
			 		
			 	
			 		 TALLYMESSAGE.appendChild(group);
					for(int j=0;j<groupNames.length;j++) {
						
						if(groupNames[j].equals("GUID")) {
							Element element = doc.createElement(groupNames[j]);
							element.appendChild(doc.createTextNode(""+i));
							group.appendChild(element);
						}
						else if(groupNames[j].equals("LANGUAGENAME.LIST")) {
							Element languageList = doc.createElement(groupNames[j]);
							group.appendChild(languageList);
							
							Element nameElement = doc.createElement("NAME.LIST");
							attr = doc.createAttribute("TYPE");
							attr.setValue("String");
							nameElement.setAttributeNode(attr);
							languageList.appendChild(nameElement);
							
							Element ledgerElement = doc.createElement("NAME");
							ledgerElement.appendChild(doc.createTextNode(groupArray[i].toString()));
							nameElement.appendChild(ledgerElement);
							
							ledgerElement = doc.createElement("LANGUAGEID");
							ledgerElement.appendChild(doc.createTextNode(""+i));
							languageList.appendChild(ledgerElement);
						}
						else if(j>3 && j<20) {
							Element element = doc.createElement(groupNames[j]);
							element.appendChild(doc.createTextNode("NO"));
							group.appendChild(element);
						}
						else {
							Element element = doc.createElement(groupNames[j]);
							//element.appendChild(doc.createTextNode("NO"));
							group.appendChild(element);
						}
					}
				 		
				 	
			 }*/
			 
			 
			/*String ledgerNames[] = {"GUID","CURRENCYNAME","VATDEALERTYPE","PARENT","TAXCLASSIFICATIONNAME","TAXTYPE","LEDADDLALLOCTYPE","GSTTYPE",
		 				"APPROPRIATEFOR","SERVICECATEGORY","EXCISELEDGERCLASSIFICATION","EXCISEDUTYTYPE","EXCISENATUREOFPURCHASE","LEDGERFBTCATEGORY","VATAPPLICABLE",
		 				"ISBILLWISEON","ISCOSTCENTRESON","ISINTERESTON","ALLOWINMOBILE","ISCOSTTRACKINGON","ISBENEFICIARYCODEON","PLASINCOMEEXPENSE",
		 				"ISUPDATINGTARGETID","ASORIGINAL","ISCONDENSED","AFFECTSSTOCK","ISRATEINCLUSIVEVAT","FORPAYROLL",
		 				"ISABCENABLED","ISCREDITDAYSCHKON","INTERESTONBILLWISE","OVERRIDEINTEREST","OVERRIDEADVINTEREST",
		 				"USEFORVAT","IGNORETDSEXEMPT","ISTCSAPPLICABLE","ISTDSAPPLICABLE","ISFBTAPPLICABLE",
		 				"ISTDSEXPENSE","ISEDLIAPPLICABLE","ISRELATEDPARTY","USEFORESIELIGIBILITY","ISINTERESTINCLLASTDAY","APPROPRIATETAXVALUE","ISBEHAVEASDUTY",
		 				"INTERESTINCLDAYOFADDITION","INTERESTINCLDAYOFDEDUCTION","ISOTHTERRITORYASSESSEE","OVERRIDECREDITLIMIT","ISAGAINSTFORMC",
		 				"ISCHEQUEPRINTINGENABLED","ISPAYUPLOAD","ISPAYBATCHONLYSAL","ISBNFCODESUPPORTED","ALLOWEXPORTWITHERRORS","CONSIDERPURCHASEFOREXPORT",
		 				"ISTRANSPORTER","USEFORNOTIONALITC","ISECOMMOPERATOR","SHOWINPAYSLIP","SHOWINPAYSLIP","USEFORGRATUITY","ISTDSPROJECTED","FORSERVICETAX",
		 				"ISEXEMPTED","ISABATEMENTAPPLICABLE","ISSTXPARTY","ISSTXNONREALIZEDTYPE","ISUSEDFORCVD","LEDBELONGSTONONTAXABLE","ISEXCISEMERCHANTEXPORTER",
		 				"ISPARTYEXEMPTED","ISSEZPARTY","TDSDEDUCTEEISSPECIALRATE","ISECHEQUESUPPORTED","ISEDDSUPPORTED","HASECHEQUEDELIVERYMODE","HASECHEQUEDELIVERYTO",
		 				"HASECHEQUEPRINTLOCATION","HASECHEQUEPAYABLELOCATION","HASECHEQUEBANKLOCATION","HASEDDDELIVERYMODE","HASEDDDELIVERYTO","HASEDDPRINTLOCATION",
		 				"HASEDDPAYABLELOCATION","HASEDDBANKLOCATION","ISEBANKINGENABLED","ISEXPORTFILEENCRYPTED","ISBATCHENABLED","ISPRODUCTCODEBASED","ISPRODUCTCODEBASED",
		 				"HASECHEQUECITY","ISFILENAMEFORMATSUPPORTED","HASCLIENTCODE","PAYINSISBATCHAPPLICABLE","PAYINSISFILENUMAPP","ISSALARYTRANSGROUPEDFORBRS",
		 				"ISEBANKINGSUPPORTED","ISSCBUAE","ISBANKSTATUSAPP","ISSALARYGROUPED","USEFORPURCHASETAX","AUDITED","SORTPOSITION","ALTERID",
		 				"SERVICETAXDETAILS.LIST","LBTREGNDETAILS.LIST","VATDETAILS.LIST","SALESTAXCESSDETAILS.LIST","GSTDETAILS.LIST","LANGUAGENAME.LIST",
		 				"XBRLDETAIL.LIST","AUDITDETAILS.LIST","SCHVIDETAILS.LIST","EXCISETARIFFDETAILS.LIST","TCSCATEGORYDETAILS.LIST","TDSCATEGORYDETAILS.LIST",
		 				"SLABPERIOD.LIST","GRATUITYPERIOD.LIST","ADDITIONALCOMPUTATIONS.LIST","EXCISEJURISDICTIONDETAILS.LIST","EXCLUDEDTAXATIONS.LIST","BANKALLOCATIONS.LIST",
		 				"PAYMENTDETAILS.LIST","BANKEXPORTFORMATS.LIST","BILLALLOCATIONS.LIST","INTERESTCOLLECTION.LIST","LEDGERCLOSINGVALUES.LIST","LEDGERAUDITCLASS.LIST",
		 				"OLDAUDITENTRIES.LIST","TDSEXEMPTIONRULES.LIST","DEDUCTINSAMEVCHRULES.LIST","LOWERDEDUCTION.LIST","STXABATEMENTDETAILS.LIST","LEDMULTIADDRESSLIST.LIST",
		 				"STXTAXDETAILS.LIST","CHEQUERANGE.LIST","DEFAULTVCHCHEQUEDETAILS.LIST","ACCOUNTAUDITENTRIES.LIST","AUDITENTRIES.LIST","BRSIMPORTEDINFO.LIST",
		 				"AUTOBRSCONFIGS.LIST","BANKURENTRIES.LIST","DEFAULTCHEQUEDETAILS.LIST","DEFAULTOPENINGCHEQUEDETAILS.LIST","CANCELLEDPAYALLOCATIONS.LIST","ECHEQUEPRINTLOCATION.LIST",
		 				"ECHEQUEPAYABLELOCATION.LIST","EDDPRINTLOCATION.LIST","EDDPAYABLELOCATION.LIST","AVAILABLETRANSACTIONTYPES.LIST","LEDPAYINSCONFIGS.LIST","TYPECODEDETAILS.LIST",
		 				"FIELDVALIDATIONDETAILS.LIST","INPUTCRALLOCS.LIST","GSTCLASSFNIGSTRATES.LIST","EXTARIFFDUTYHEADDETAILS.LIST","VOUCHERTYPEPRODUCTCODES.LIST",
			 			}; */
			
			
			/* String noArray[] = {"ISBILLWISEON","ISCOSTCENTRESON","ISINTERESTON","ALLOWINMOBILE","ISCOSTTRACKINGON","ISBENEFICIARYCODEON","PLASINCOMEEXPENSE",
		 				"ISUPDATINGTARGETID","ASORIGINAL","ISCONDENSED","AFFECTSSTOCK","ISRATEINCLUSIVEVAT","FORPAYROLL",
		 				"ISABCENABLED","ISCREDITDAYSCHKON","INTERESTONBILLWISE","OVERRIDEINTEREST","OVERRIDEADVINTEREST",
		 				"USEFORVAT","IGNORETDSEXEMPT","ISTCSAPPLICABLE","ISTDSAPPLICABLE","ISFBTAPPLICABLE",
		 				"ISTDSEXPENSE","ISEDLIAPPLICABLE","ISRELATEDPARTY","USEFORESIELIGIBILITY","ISINTERESTINCLLASTDAY","APPROPRIATETAXVALUE","ISBEHAVEASDUTY",
		 				"INTERESTINCLDAYOFADDITION","INTERESTINCLDAYOFDEDUCTION","ISOTHTERRITORYASSESSEE","OVERRIDECREDITLIMIT","ISAGAINSTFORMC",
		 				"ISCHEQUEPRINTINGENABLED","ISPAYUPLOAD","ISPAYBATCHONLYSAL","ISBNFCODESUPPORTED","ALLOWEXPORTWITHERRORS","CONSIDERPURCHASEFOREXPORT",
		 				"ISTRANSPORTER","USEFORNOTIONALITC","ISECOMMOPERATOR","SHOWINPAYSLIP","SHOWINPAYSLIP","USEFORGRATUITY","ISTDSPROJECTED","FORSERVICETAX",
		 				"ISEXEMPTED","ISABATEMENTAPPLICABLE","ISSTXPARTY","ISSTXNONREALIZEDTYPE","ISUSEDFORCVD","LEDBELONGSTONONTAXABLE","ISEXCISEMERCHANTEXPORTER",
		 				"ISPARTYEXEMPTED","ISSEZPARTY","TDSDEDUCTEEISSPECIALRATE","ISECHEQUESUPPORTED","ISEDDSUPPORTED","HASECHEQUEDELIVERYMODE","HASECHEQUEDELIVERYTO",
		 				"HASECHEQUEPRINTLOCATION","HASECHEQUEPAYABLELOCATION","HASECHEQUEBANKLOCATION","HASEDDDELIVERYMODE","HASEDDDELIVERYTO","HASEDDPRINTLOCATION",
		 				"HASEDDPAYABLELOCATION","HASEDDBANKLOCATION","ISEBANKINGENABLED","ISEXPORTFILEENCRYPTED","ISBATCHENABLED","ISPRODUCTCODEBASED","ISPRODUCTCODEBASED",
		 				"HASECHEQUECITY","ISFILENAMEFORMATSUPPORTED","HASCLIENTCODE","PAYINSISBATCHAPPLICABLE","PAYINSISFILENUMAPP","ISSALARYTRANSGROUPEDFORBRS",
		 				"ISEBANKINGSUPPORTED","ISSCBUAE","ISBANKSTATUSAPP","ISSALARYGROUPED","USEFORPURCHASETAX","AUDITED"
			 };*/
			 
			// List noArrayList = Arrays.asList(noArray);
			 
			String ledgerNames[] = {"GUID","CURRENCYNAME","VATDEALERTYPE","PARENT","TAXCLASSIFICATIONNAME","TAXTYPE","LANGUAGENAME.LIST"};
			 
			List<XMLResponseBean> ledgerList  = accountRepository.getRecordsForLedgerMasterTally(schoolId,csSchoolId);
				ledgerList.parallelStream().forEach(admin -> {
					if(!admin.getLedgerName().equals("P and L/Surplus or Deficit")) {
					Element TALLYMESSAGE = doc.createElement("TALLYMESSAGE");
					Attr attr = doc.createAttribute("xmlns:UDF");
					attr.setValue("TallyUDF");
					TALLYMESSAGE.setAttributeNode(attr);
					REQUESTDATA.appendChild(TALLYMESSAGE);
				 
				 	Element ledger = doc.createElement("LEDGER");
					Attr voucherAttr = doc.createAttribute("NAME");
					voucherAttr.setValue(admin.getLedgerName().toString());
					ledger.setAttributeNode(voucherAttr);
					
					voucherAttr = doc.createAttribute("RESERVEDNAME");
					voucherAttr.setValue(admin.getLedgerName().toString());
					ledger.setAttributeNode(voucherAttr);
				
					TALLYMESSAGE.appendChild(ledger);
					Element element = doc.createElement("GUID");
					element.appendChild(doc.createTextNode(admin.getLedgerId().toString()));
					ledger.appendChild(element);
					
					element = doc.createElement("CURRENCYNAME");
					element.appendChild(doc.createTextNode(""));
					ledger.appendChild(element);
					
					element = doc.createElement("VATDEALERTYPE");
					element.appendChild(doc.createTextNode("Regular"));
					ledger.appendChild(element);
					
					element = doc.createElement("PARENT");
					element.appendChild(doc.createTextNode(admin.getGroupName().toString()));
					ledger.appendChild(element);
					
					element = doc.createElement("TAXTYPE");
					element.appendChild(doc.createTextNode("OTHERS"));
					ledger.appendChild(element);
					
					/*element = doc.createElement("SERVICECATEGORY");
					element.appendChild(doc.createTextNode("&#4; Not Applicable"));
					ledger.appendChild(element);
					
					element = doc.createElement("VATAPPLICABLE");
					element.appendChild(doc.createTextNode("&#4; Not Applicable"));
					ledger.appendChild(element);*/
					
					
					Element languageList = doc.createElement("LANGUAGENAME.LIST");
					ledger.appendChild(languageList);
					
					Element nameElement = doc.createElement("NAME.LIST");
					attr = doc.createAttribute("TYPE");
					attr.setValue("String");
					nameElement.setAttributeNode(attr);
					languageList.appendChild(nameElement);
					
					Element ledgerElement = doc.createElement("NAME");
					ledgerElement.appendChild(doc.createTextNode(admin.getLedgerName().toString()));
					nameElement.appendChild(ledgerElement);
					
					ledgerElement = doc.createElement("LANGUAGEID");
					ledgerElement.appendChild(doc.createTextNode(admin.getLedgerId().toString()));
					languageList.appendChild(ledgerElement);
					
					element = doc.createElement("ISCOSTCENTRESON");
					element.appendChild(doc.createTextNode("Yes"));
					ledger.appendChild(element);
					
					element = doc.createElement("ASORIGINAL");
					element.appendChild(doc.createTextNode("Yes"));
					ledger.appendChild(element);
				}
					
				});
				  // write the content into xml file
		         TransformerFactory transformerFactory = TransformerFactory.newInstance();
		         Transformer transformer = transformerFactory.newTransformer();
		         DOMSource source = new DOMSource(doc);
		         String folderPath = "C:\\ePrashasan\\MainDB\\cms\\TallyXml\\"+schoolId;
		         
		         File dir = new File(folderPath);
		         if (!dir.exists()) {
					dir.mkdirs();
		         }
						
			     String xmlPath = folderPath+"\\AccountsMasterEntry_"+schoolId+".xml";
		         transformer.setOutputProperty(OutputKeys.INDENT, "yes"  );
		         transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,"yes"  );
		         transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "5"  );
		         System.out.println("xml created");
		         StreamResult result = new StreamResult(new File(xmlPath));
		         transformer.transform(source, result);
		        
		         String groupMasterPath = "C:\\ePrashasan\\MainDB\\cms\\GroupMaster.xml";
		        		 
		         FileReader fr = new FileReader(groupMasterPath);
		         BufferedReader br = new BufferedReader(fr);
		         StringBuffer groupTags = new StringBuffer();
		         String line;
				while ((line = br.readLine()) != null) {
					groupTags.append(line).append("\n");
					
				}
				
				System.out.println(groupTags.toString());
				FileInputStream fis = new FileInputStream(xmlPath);
				String content = IOUtils.toString(fis, Charset.defaultCharset());
				content = content.replaceAll("<TALLYGroupMESSAGE/>", groupTags.toString());
				content = content.replaceAll("&amp;#4;", "&#4;");
				System.out.println("contents replaced");
				FileOutputStream fos = new FileOutputStream(xmlPath);
				IOUtils.write(content, new FileOutputStream(xmlPath), Charset.defaultCharset());
				fis.close();
				fos.close();
				br.close();
	
		         // Output to console for testing
		       // StreamResult consoleResult = new StreamResult(System.out);
		       // transformer.transform(source, consoleResult);
		         String downloadUrl=ConfigurationProperties.tallyXmlBaseUrl+schoolId+"/AccountsMasterEntry_"+schoolId+".xml";
		         String fileName = "AccountsMasterEntry_"+schoolId+"";
		         System.out.println(downloadUrl);
		         return folderPath+"/#" + fileName;
			} catch (ParserConfigurationException | TransformerException | IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	@Override
	public String generateTallyReport(Integer schoolId, Integer csSchoolId,String fromDate,String toDate,Integer type) {
		
		String xmlPath = null;
		try {
			String voucherName="";
			List<XMLResponseBean> adminList = new ArrayList<>();
//			String yearName1= year.split("-")[0];
//			String yearName2= year.split("-")[1];
//			
//			String date1 = "01-04-"+yearName1;
//			String date2 = "31-03-"+yearName2;
			 adminList = accountRepository.getRecordsForTally(schoolId,csSchoolId,fromDate,toDate,type);
	         DocumentBuilderFactory dbFactory =  DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.newDocument();
	         	         
		 	// root element
	         Element ENVELOPE = doc.createElement("ENVELOPE");
	         doc.appendChild(ENVELOPE);

	         // Header element
	         Element HEADER = doc.createElement("HEADER");
	         ENVELOPE.appendChild(HEADER);
	         

			 // tallyRequest element
			 Element TALLYREQUEST = doc.createElement("TALLYREQUEST");
			 TALLYREQUEST.appendChild(doc.createTextNode("Import Data"));
			 HEADER.appendChild(TALLYREQUEST);

			 
			 Element BODY = doc.createElement("BODY");
			 ENVELOPE.appendChild(BODY);
			 
			 
			 Element IMPORTDATA = doc.createElement("IMPORTDATA");
			 BODY.appendChild(IMPORTDATA);
			 
			 
			 Element REQUESTDESC = doc.createElement("REQUESTDESC");
			 IMPORTDATA.appendChild(REQUESTDESC);
				 
		 
			 Element REPORTNAME = doc.createElement("REPORTNAME");
			 REPORTNAME.appendChild(doc.createTextNode("All Masters"));
			 REQUESTDESC.appendChild(REPORTNAME);
					 
					 
			 Element STATICVARIABLES = doc.createElement("STATICVARIABLES");
			 REQUESTDESC.appendChild(STATICVARIABLES);

						
			 Element SVCURRENTCOMPANY = doc.createElement("SVCURRENTCOMPANY");
			 SVCURRENTCOMPANY.appendChild(doc.createTextNode("Ingenio"));
			 STATICVARIABLES.appendChild(SVCURRENTCOMPANY);
						
			 Element REQUESTDATA = doc.createElement("REQUESTDATA");
			 IMPORTDATA.appendChild(REQUESTDATA);
				 
		     ArrayList<Object> voucherList = new ArrayList<>();
				 
			/* String voucherArray[] = {"DATE","GUID","NARRATION","VOUCHERTYPENAME","VOUCHERNUMBER","PARTYLEDGERNAME",
							"CSTFORMISSUETYPE","CSTFORMRECVTYPE","FBTPAYMENTTYPE","VCHGSTCLASS","DIFFACTUALQTY","AUDITED",
							"FORJOBCOSTING","ISOPTIONAL","EFFECTIVEDATE","USEFORINTEREST","USEFORGAINLOSS","USEFORGODOWNTRANSFER",
							"USEFORCOMPOUND","ALTERID","EXCISEOPENING","USEFORFINALPRODUCTION","ISCANCELLED","HASCASHFLOW",
							"ISPOSTDATED","USETRACKINGNUMBER","ISINVOICE","MFGJOURNAL","HASDISCOUNTS","ASPAYSLIP","ISCOSTCENTRE",
							"ISDELETED","ASORIGINAL","ALLLEDGERENTRIES.LIST"};
			 
			 String noValueArray[] = {"DIFFACTUALQTY","AUDITED","FORJOBCOSTING","ISOPTIONAL","USEFORINTEREST","USEFORGAINLOSS","USEFORGODOWNTRANSFER",
					 "USEFORCOMPOUND","EXCISEOPENING","USEFORFINALPRODUCTION","ISCANCELLED","HASCASHFLOW","ISPOSTDATED","USETRACKINGNUMBER","ISINVOICE",
					 "MFGJOURNAL","HASDISCOUNTS","ASPAYSLIP","ISCOSTCENTRE","ISDELETED","ASORIGINAL",};
			 
			 List noValuesList = Arrays.asList(noValueArray);*/
		     
		     String voucherArray[] = {"DATE","NARRATION","VOUCHERTYPENAME","VOUCHERNUMBER","ALLLEDGERENTRIES.LIST"};
			 
			 for(int i=0;i<adminList.size();i++) {
				 	XMLResponseBean admin = adminList.get(i);
					Element TALLYMESSAGE = doc.createElement("TALLYMESSAGE");
					Attr attr = doc.createAttribute("xmlns:UDF");
					attr.setValue("TallyUDF");
					TALLYMESSAGE.setAttributeNode(attr);
					REQUESTDATA.appendChild(TALLYMESSAGE);
				 
				 	Element VOUCHER = doc.createElement("VOUCHER");
				 	voucherName=admin.getVouvherTypeName().toString();
			 		Attr voucherAttr2 = doc.createAttribute("VCHTYPE");
			 		voucherAttr2.setValue(admin.getVouvherTypeName().toString());
			 		VOUCHER.setAttributeNode(voucherAttr2);
			 		
			 		Attr voucherAttr3 = doc.createAttribute("ACTION");
			 		voucherAttr3.setValue("Create");
			 		VOUCHER.setAttributeNode(voucherAttr3);
				 		
				 	TALLYMESSAGE.appendChild(VOUCHER);
				 	
						for(String voucherChild : voucherArray) {
							
							/*if(voucherChild.equals("GUID")) {
								Element element = doc.createElement(voucherChild);
								element.appendChild(doc.createTextNode(admin.getJournalId().toString()));
								VOUCHER.appendChild(element);
							}*/
							if(voucherChild.equals("VOUCHERTYPENAME")) {
								Element element = doc.createElement(voucherChild);
								element.appendChild(doc.createTextNode(admin.getVouvherTypeName().toString()));
								VOUCHER.appendChild(element);
							}
							else if(voucherChild.equals("VOUCHERNUMBER")) {
								Element element = doc.createElement(voucherChild);
								element.appendChild(doc.createTextNode(admin.getVoucherNumber().split("-")[1].toString()));
								VOUCHER.appendChild(element);
							}
							else if(voucherChild.equals("NARRATION")) {
									Element element = doc.createElement(voucherChild);
									element.appendChild(doc.createTextNode(admin.getNarration().toString()));
									VOUCHER.appendChild(element);
							}
						/*	else if(voucherChild.equals("PARTYLEDGERNAME")) {
								Element element = doc.createElement(voucherChild);
								element.appendChild(doc.createTextNode(admin.getDebitLedgerName()));
								VOUCHER.appendChild(element);
							}*/
							else if(voucherChild.equals("DATE") || voucherChild.equals("EFFECTIVEDATE")) {
								Element element = doc.createElement(voucherChild);
								element.appendChild(doc.createTextNode(admin.getDate().toString()));
								VOUCHER.appendChild(element);
							}
						/*	else if(voucherChild.equals("FBTPAYMENTTYPE")) {
								Element element = doc.createElement(voucherChild);
								element.appendChild(doc.createTextNode("DEFAULT"));
								VOUCHER.appendChild(element);
							}
							else if(voucherChild.equals("ALTERID")) {
								Element element = doc.createElement(voucherChild);
								//element.appendChild(doc.createTextNode("DEFAULT"));
								VOUCHER.appendChild(element);
							}*/
							else if(voucherChild.equals("ALLLEDGERENTRIES.LIST")) {
								List<XMLResponseBean> invoNoList = accountRepository.getRecordsForTallyByInvoNo(
										schoolId,csSchoolId,fromDate,toDate,admin.getVoucherNumber()); 
								Double debitAmount = 0.0;
								Double debitAmountWithoutDiscount=0.0;
								for(int inv=0;inv<invoNoList.size();inv++) {
									XMLResponseBean invoBean = invoNoList.get(inv);
									Element ledgerCreditList = doc.createElement(voucherChild);
									VOUCHER.appendChild(ledgerCreditList);
									
									Element ledgerCreditElement = doc.createElement("LEDGERNAME");
									ledgerCreditElement.appendChild(doc.createTextNode(invoBean.getCreditLedgerName().toString()));
									ledgerCreditList.appendChild(ledgerCreditElement);
									
									ledgerCreditElement = doc.createElement("ISDEEMEDPOSITIVE");
									ledgerCreditElement.appendChild(doc.createTextNode("NO"));
									ledgerCreditList.appendChild(ledgerCreditElement);
									
									ledgerCreditElement = doc.createElement("AMOUNT");
									ledgerCreditElement.appendChild(doc.createTextNode(invoBean.getDrAmount().toString()));
									ledgerCreditList.appendChild(ledgerCreditElement);
									
									ledgerCreditElement = doc.createElement("VATEXPAMOUNT");
									ledgerCreditElement.appendChild(doc.createTextNode(invoBean.getDrAmount().toString()));
									ledgerCreditList.appendChild(ledgerCreditElement);
									
									debitAmount+=(invoBean.getDrAmount());
									debitAmountWithoutDiscount=debitAmount;
								}
								
								List<XMLResponseBean> discountLedgerList = accountRepository.getRecordsForTallyByInvoNoForDiscount(
										schoolId,csSchoolId,fromDate,toDate,admin.getVoucherNumber()); 
								if(CollectionUtils.isNotEmpty(discountLedgerList) && discountLedgerList.get(0)!=null &&  discountLedgerList.get(0).getDrAmount()!=0 ) {
									XMLResponseBean discountBean = discountLedgerList.get(0);
									debitAmountWithoutDiscount+=discountBean.getDrAmount();
								}
								
								Element ledgerList = doc.createElement(voucherChild);
								VOUCHER.appendChild(ledgerList);
								
								Element ledgerElement = doc.createElement("LEDGERNAME");
								ledgerElement.appendChild(doc.createTextNode(admin.getDebitLedgerName().toString()));
								ledgerList.appendChild(ledgerElement);
								
								ledgerElement = doc.createElement("ISDEEMEDPOSITIVE");
								ledgerElement.appendChild(doc.createTextNode("YES"));
								ledgerList.appendChild(ledgerElement);
								
								ledgerElement = doc.createElement("AMOUNT");
								ledgerElement.appendChild(doc.createTextNode("-"+debitAmountWithoutDiscount));
								ledgerList.appendChild(ledgerElement);
								
								ledgerElement = doc.createElement("VATEXPAMOUNT");
								ledgerElement.appendChild(doc.createTextNode(("-"+debitAmountWithoutDiscount)));
								ledgerList.appendChild(ledgerElement);
								
								if(admin.getVouvherTypeName().equals("Receipt") && admin.getBankId()!=0) {
									Element bankList = doc.createElement("BANKALLOCATIONS.LIST");
									ledgerList.appendChild(bankList);
									
									Element bankElement = doc.createElement("DATE");
									bankElement.appendChild(doc.createTextNode(admin.getDate().toString()));
									bankList.appendChild(bankElement);
									
									bankElement = doc.createElement("INSTRUMENTDATE");
									bankElement.appendChild(doc.createTextNode(admin.getDate().toString()));
									bankList.appendChild(bankElement);
									
									bankElement = doc.createElement("TRANSACTIONTYPE");
									bankElement.appendChild(doc.createTextNode("Cheque"));
									bankList.appendChild(bankElement);
									
									if((admin.getPaymentDetails().contains("--"))) {
										bankElement = doc.createElement("INSTRUMENTNUMBER");
										bankElement.appendChild(doc.createTextNode(admin.getPaymentDetails().split("--")[1].toString()));
										bankList.appendChild(bankElement);
									}
									else {
										bankElement = doc.createElement("INSTRUMENTNUMBER");
										bankElement.appendChild(doc.createTextNode("0"));
										bankList.appendChild(bankElement);
									}
								
									
									bankElement = doc.createElement("STATUS");
									bankElement.appendChild(doc.createTextNode("NO"));
									bankList.appendChild(bankElement);
									
									bankElement = doc.createElement("PAYMENTMODE");
									bankElement.appendChild(doc.createTextNode("TRANSACTED"));
									bankList.appendChild(bankElement);
									
									bankElement = doc.createElement("BANKPARTYNAME");
									bankElement.appendChild(doc.createTextNode(admin.getDebitLedgerName().toString()));
									bankList.appendChild(bankElement);
									
									bankElement = doc.createElement("ISCONNECTEDPAYMENT");
									bankElement.appendChild(doc.createTextNode("NO"));
									bankList.appendChild(bankElement);
									
									bankElement = doc.createElement("ISSPLIT");
									bankElement.appendChild(doc.createTextNode("NO"));
									bankList.appendChild(bankElement);
									
									bankElement = doc.createElement("AMOUNT");
									bankElement.appendChild(doc.createTextNode("-"+debitAmountWithoutDiscount));
									bankList.appendChild(bankElement);
									
									
								}
								
								if(CollectionUtils.isNotEmpty(discountLedgerList) && discountLedgerList.get(0)!=null &&  discountLedgerList.get(0).getDrAmount()!=0 ) {
									XMLResponseBean discountBean = discountLedgerList.get(0);
									Element ledgerCreditList = doc.createElement(voucherChild);
									VOUCHER.appendChild(ledgerCreditList);
									
									Element ledgerCreditElement = doc.createElement("LEDGERNAME");
									ledgerCreditElement.appendChild(doc.createTextNode(discountBean.getCreditLedgerName().toString()));
									ledgerCreditList.appendChild(ledgerCreditElement);
									
									ledgerCreditElement = doc.createElement("ISDEEMEDPOSITIVE");
									ledgerCreditElement.appendChild(doc.createTextNode("YES"));
									ledgerCreditList.appendChild(ledgerCreditElement);
									
									ledgerCreditElement = doc.createElement("AMOUNT");
									ledgerCreditElement.appendChild(doc.createTextNode(discountBean.getDrAmount().toString()));
									ledgerCreditList.appendChild(ledgerCreditElement);
								}
								
								
								/*ledgerElement = doc.createElement("LEDGERFROMITEM");
								ledgerElement.appendChild(doc.createTextNode("NO"));
								ledgerList.appendChild(ledgerElement);
								
								ledgerElement = doc.createElement("REMOVEZEROENTRIES");
								ledgerElement.appendChild(doc.createTextNode("NO"));
								ledgerList.appendChild(ledgerElement);
								
								ledgerElement = doc.createElement("ISPARTYLEDGER");
								ledgerElement.appendChild(doc.createTextNode("YES"));
								ledgerList.appendChild(ledgerElement);*/
									
								
								
								/*ledgerCreditElement = doc.createElement("LEDGERFROMITEM");
								ledgerCreditElement.appendChild(doc.createTextNode("NO"));
								ledgerCreditList.appendChild(ledgerCreditElement);
								
								ledgerCreditElement = doc.createElement("REMOVEZEROENTRIES");
								ledgerCreditElement.appendChild(doc.createTextNode("NO"));
								ledgerCreditList.appendChild(ledgerCreditElement);
								
								ledgerCreditElement = doc.createElement("ISPARTYLEDGER");
								ledgerCreditElement.appendChild(doc.createTextNode("YES"));
								ledgerCreditList.appendChild(ledgerCreditElement);*/
								
								
							}
							/*else if(noValuesList.contains(voucherChild)) {
								Element element = doc.createElement(voucherChild);
								element.appendChild(doc.createTextNode("NO"));
								VOUCHER.appendChild(element);
							}*/
							else {
								Element element = doc.createElement(voucherChild);
								//element.appendChild(doc.createTextNode("NO"));
								VOUCHER.appendChild(element);
							}
							
						}
				 }
				 
					
						
	         // write the content into xml file
	         TransformerFactory transformerFactory = TransformerFactory.newInstance();
	         Transformer transformer = transformerFactory.newTransformer();
	         DOMSource source = new DOMSource(doc);
	         String folderPath = "C:\\ePrashasan\\MainDB\\cms\\TallyXml\\"+schoolId;
	         
	         File dir = new File(folderPath);
	         if (!dir.exists()) {
				dir.mkdirs();
	         }
					
		     	xmlPath = folderPath+"\\"+voucherName+"VoucherEntry_"+schoolId+"from"+fromDate+"to"+toDate+".xml";
	         transformer.setOutputProperty(OutputKeys.INDENT, "yes"  );
	         transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,"yes"  );
	         transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "5"  );
	        
	         StreamResult result = new StreamResult(new File(xmlPath));
	         transformer.transform(source, result);
	         StreamResult consoleResult = new StreamResult(System.out);
	         transformer.transform(source, consoleResult);
//	         String downloadUrl=ConfigurationProperties.tallyXmlBaseUrl+schoolId+"\\"+voucherName+"VoucherEntry_"+schoolId+"from"+fromDate+"to"+toDate+".xml";;
	         // Output to console for testing
	         
	         String downloadUrl=ConfigurationProperties.tallyXmlBaseUrl+schoolId+"/VoucherEntry_"+schoolId+".xml";
	         String fileName = voucherName+"VoucherEntry_"+schoolId+"from"+fromDate+"to"+toDate;
	         System.out.println(downloadUrl);
	         return folderPath+"/#" + fileName;
	        
	         
	      
	      } catch (Exception e) {
	         e.printStackTrace();
	      }	
		return "";
	}
	@Override
	public String uploadXMLToAWS(String fileFolderPath, String fileName, HttpServletResponse response, Integer schoolId,
			Integer csSchoolId,File file,Integer type) {
		// TODO Auto-generated method stub

		try {
			Path filePath = Paths.get(fileFolderPath + "\\" + fileName + ".xml");
			Files.copy(filePath, response.getOutputStream());
			String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
			String folderName = null;
			String xmlType = null;
			if(type == 0) {
				 folderName = "cms" + File.separator + "LedgerXML" + File.separator + schoolKey + File.separator+ schoolId;
				  xmlType = "LedgerMaster";
			}else {
				folderName = "cms" + File.separator + "VoucherXML" + File.separator + schoolKey + File.separator+ schoolId;
				xmlType = feeReportsRepositoryImpl.getVoucherType(csSchoolId,schoolId,type);
			}
			
			StringBuffer attachmentFileName = new StringBuffer();
			String[] splitFileName = file.getName().toString().split("\\.");
			attachmentFileName.append(splitFileName[1]);
			Path path1 = Paths.get(fileFolderPath + "\\" + fileName + ".xml");
			byte[] content = Files.readAllBytes(path1);
			String postUri = "http://AWSUTILITY-SERVICE/uploadFileToS3/";
			URI uri;
			try {
				uri = new URI(postUri);
				File convFile = new File(file.getName());
				convFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(convFile);
				fos.write(content);
				fos.close();
				UploadFileBean bean = new UploadFileBean();
				bean.setFileName(attachmentFileName.toString());
				bean.setFile(convFile);
				bean.setFilePath("maindb" + "/" + folderName);
				String path = uploadSingleFile(bean);
				convFile.delete();
				response.getOutputStream().flush();
				
				
		         List<XmlFilePathBean> xmlFileList = accountRepository.getXmlFilePathList(schoolId,csSchoolId,xmlType);
		         if(xmlFileList.isEmpty()) {
		        	 XmlFilePathModel xmlFilePathModel = new XmlFilePathModel();
//		        	 String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		 			String globalDbSchoolKey = "1".concat("" + schoolKey);
		        	 String incrementedId = ""+ feeRepository.getMaxAssignToId(schoolId);
					 String xmlFilePathID = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
		        	 xmlFilePathModel.setFilePathId(Integer.parseInt(xmlFilePathID));
		        	 xmlFilePathModel.setSchoolid(schoolId);
		        	 xmlFilePathModel.setCsSchoolid(csSchoolId);
		        	 if (!path.equals("") && !path.equals(null)) {
		        		 xmlFilePathModel.setFilePath(path);
						}
		        	 xmlFilePathModel.setXmlType(xmlType);
		        	 xmlFilePathRepository.save(xmlFilePathModel);
		         }else {
		        	 xmlFilePathRepository.updateFilePath(schoolId,csSchoolId,xmlType,path);
		         }

				return path;
			} catch (Exception e) {
				e.printStackTrace();
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	
	} 
	public String uploadSingleFile(UploadFileBean fileBean) {
		try {
			String bucketName = "eprashasan1";
			String key = fileBean.getFilePath();
			key = key.replace("\\", "/");
			String returnPath = UploadFile(bucketName, key, fileBean.getFile());
			return returnPath;
		} catch (Exception e) {
			return null;
		}
	}

	public String UploadFile(String bucketName, String keyName, File file) {
		try {
			keyName = keyName + "/" + file.getName();
			keyName = keyName.replace(" ", "_");
			amazonS3.putObject(
					new PutObjectRequest(bucketName, keyName, file).withCannedAcl(CannedAccessControlList.PublicRead));
			return ((AmazonS3Client) amazonS3).getResourceUrl(bucketName, keyName);
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		}
		return "";
	}

}
