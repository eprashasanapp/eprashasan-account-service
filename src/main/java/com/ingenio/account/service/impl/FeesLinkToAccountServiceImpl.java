package com.ingenio.account.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import com.ingenio.account.bean.AccAddVoucherTypeBean;
import com.ingenio.account.bean.AssignFeeBean;
import com.ingenio.account.bean.FeesToAccountLinkBean;
import com.ingenio.account.bean.FeesettingBean;
import com.ingenio.account.model.AccAddGroupAndSubgroup;
import com.ingenio.account.model.AccAddVoucherType;
import com.ingenio.account.model.AccCreateLedger;
import com.ingenio.account.model.AccCreateLedgerHead;
import com.ingenio.account.model.AccCreateLedgerStandardMapp;
import com.ingenio.account.model.AccCreateLedgerStudentMapp;
import com.ingenio.account.model.AccCreateSchoolCollege;
import com.ingenio.account.model.AccDepreciation;
import com.ingenio.account.model.AccJournalDayBookEntry;
import com.ingenio.account.model.AppUserRoleModel;
import com.ingenio.account.model.SchoolMasterModel;
import com.ingenio.account.model.StandardMasterModel;
import com.ingenio.account.model.StudentMasterModel;
import com.ingenio.account.repository.AccCreateLedgerRepository;
import com.ingenio.account.repository.AccCreateLedgerStandardMappRepository;
import com.ingenio.account.repository.AccCreateLedgerStudentMappRepository;
import com.ingenio.account.repository.AccDepreciationRepository;
import com.ingenio.account.repository.FeesLinkToAccountRepository;
import com.ingenio.account.repository.impl.FeesLinkToAccountRepositoryImpl;
import com.ingenio.account.service.FeesLinkToAccountService;

@Component
public class FeesLinkToAccountServiceImpl implements FeesLinkToAccountService {
	
	@Autowired
	FeesLinkToAccountRepositoryImpl feesLinkToAccountRepositoryImpl;
	
	@Autowired
	FeesLinkToAccountRepository feesLinkToAccountRepository;
	
	@Autowired
	AccCreateLedgerRepository accCreateLedgerRepository;
	
	@Autowired
	AccCreateLedgerStudentMappRepository accCreateLedgerStudentMappRepository;
	
	@Autowired
	AccDepreciationRepository accDepreciationRepository;
	
	@Autowired
	AccCreateLedgerStandardMappRepository accCreateLedgerStandardMappRepository;

	@Override
	public List<AssignFeeBean> passEntryInAccount(String regNo, Integer yearId, Integer schoolId) {
		try {
			createReceiptEntriesForAccount(regNo, yearId, schoolId);
			createHeadSubheadEntries("","","1",0,regNo,""+yearId,schoolId);
			return new ArrayList<AssignFeeBean>();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	private void createHeadSubheadEntries(String fromDate, String toDate, String type,Integer inputReceiptId, String regNo, String yearId,Integer schoolId) {
		try {
			List<FeesToAccountLinkBean> receiptEntriesList = feesLinkToAccountRepositoryImpl.getheadSubheadReceiptEntries(schoolId,fromDate,toDate,inputReceiptId,regNo,yearId);
			List<FeesToAccountLinkBean> schoolMasterBean= feesLinkToAccountRepositoryImpl.getCurrentUserForLogin(schoolId);
			String globalDbSchoolKey=(""+schoolMasterBean.get(0).getSchoolId().toString().subSequence(0, 1)).concat(schoolMasterBean.get(0).getSchoolKey());
			String typeSettingId=generatePrimaryKey("10","ORIGINAL",globalDbSchoolKey);
			List<FeesettingBean>  validateimage1 = feesLinkToAccountRepositoryImpl.getFeesReceiptsettingForUnit(Integer.parseInt(typeSettingId),schoolId);
			int unitSettingFlag=0;
			if(CollectionUtils.isNotEmpty(validateimage1)) {
				unitSettingFlag=Integer.parseInt(validateimage1.get(0).getSettingFlag());
			}
			String csSchoolId="";
			Integer standardId = receiptEntriesList.get(0).getStandardId();
			List<FeesToAccountLinkBean> list2=feesLinkToAccountRepositoryImpl.getTypeOfUnitIdStdWise(standardId,schoolId);
			if(unitSettingFlag == 1) {
				if(list2.get(0).getClassId()!=null){
					csSchoolId=generatePrimaryKey(truncateKey(""+list2.get(0).getSubheadId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
				}else {
					csSchoolId = generatePrimaryKey("1","ORIGINAL",globalDbSchoolKey);
				}
			}else {
				csSchoolId = generatePrimaryKey("1","ORIGINAL",globalDbSchoolKey);
			}
			
			ModelMap modelmap = new ModelMap();
			String oldEntries="1";
			String updatedBy="1";
			String feesCsSchoolId=csSchoolId;
			String paymentDetails="";
			Long journalId=Long.parseLong("0");
			int voucherTypeIdForJournal =  feesLinkToAccountRepositoryImpl.getVouherId("Journal",csSchoolId,schoolId);
			
			int scholarshipApprId=feesLinkToAccountRepositoryImpl.getApprovalId(regNo,schoolId);
			
			if(type.equals("0")) {
				
				Integer creditLedgerForSubHeadFees=null;
				List creditLedgerListForSubHeadFees = new ArrayList<>();
				creditLedgerListForSubHeadFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("School Fees",csSchoolId,schoolId);
				if(CollectionUtils.isNotEmpty(creditLedgerListForSubHeadFees) &&  creditLedgerListForSubHeadFees.get(0) !=null) {
					Object[] obj = (Object[]) creditLedgerListForSubHeadFees.get(0);
					creditLedgerForSubHeadFees =(Integer) obj[0];
				}
				
				Integer creditLedgerForSubHeadScholarship=null;
				List creditLedgerForSubHeadScholarships = new ArrayList<>();
				creditLedgerForSubHeadScholarships= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Excess Fees",csSchoolId,schoolId);
				if(CollectionUtils.isNotEmpty(creditLedgerForSubHeadScholarships) &&  creditLedgerForSubHeadScholarships.get(0) !=null) {
					Object[] obj = (Object[]) creditLedgerForSubHeadScholarships.get(0);
					creditLedgerForSubHeadScholarship =(Integer) obj[0];
				}
				
				Integer creditLedgerForSubHeadFine=null;
				List creditLedgerListForSubHeadFine = new ArrayList<>();
				creditLedgerListForSubHeadFine= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Late Fees",csSchoolId,schoolId);
				if(CollectionUtils.isNotEmpty(creditLedgerListForSubHeadFine) &&  creditLedgerListForSubHeadFine.get(0) !=null) {
					Object[] obj = (Object[]) creditLedgerListForSubHeadFine.get(0);
					creditLedgerForSubHeadFine =(Integer) obj[0];
				}
				
				List<FeesToAccountLinkBean> subheadList = feesLinkToAccountRepositoryImpl.getsubHeadList(schoolId,fromDate,toDate,type,inputReceiptId);
				System.out.println("--------------------------subheadsize---------"+subheadList.size());
			
				if(CollectionUtils.isNotEmpty(subheadList)) {
					for(int i=0;i<subheadList.size();i++) {
						int index = feesLinkToAccountRepositoryImpl.getMaxJournalId(schoolId);
						Integer debitLedgerForSubHead=feesLinkToAccountRepositoryImpl.getLedgerIdBySubHeadId(""+subheadList.get(i).getSubheadId(),schoolId);
						if(Double.parseDouble(subheadList.get(i).getAssignFee())>0) {
							
							StringBuffer voucherNo = new StringBuffer();
							voucherNo.append("Jo").append("-").append(String.valueOf(index+1));
							StringBuffer voucherType = new StringBuffer(); 
							voucherType.append(""+voucherTypeIdForJournal).append("_").append(voucherNo.toString());
							
							
							StringBuffer narration = new StringBuffer();
							narration.append(subheadList.get(i).getYearName()).append("-").append(subheadList.get(i).getStandardName()).append("-")
									.append( subheadList.get(i).getRegistartionNumber()).append("-")
									 .append(subheadList.get(i).getStudentName()).append("-").append(subheadList.get(i).getHeadName()).append("-")
									 .append(subheadList.get(i).getSubHeadName()).append("-").append("SubHeadWise-Fee");
							if(scholarshipApprId!=0) {
								FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForJournal,debitLedgerForSubHead,
										creditLedgerForSubHeadScholarship,subheadList.get(i).getRenewstudentId(),"SubHeadWise-Fee");
								
								if(existingRecord==null) {
									System.out.println("Credit"+debitLedgerForSubHead);
									System.out.println("Debit"+creditLedgerForSubHeadScholarship);
									addJournalEntry(modelmap,journalId,subheadList.get(i).getReceiptDate(),voucherType.toString(),
											debitLedgerForSubHead,creditLedgerForSubHeadScholarship,
											  narration.toString(),paymentDetails,voucherNo.toString(),Double.parseDouble(subheadList.get(i).getAssignFee()),"0",
											  ""+subheadList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
									System.out.println("Done");
								}
								else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+subheadList.get(i).getAssignFee())) || !existingRecord.getNarration().equals(""+narration+"-ClassWise-Fees")) {
									System.out.println("Credit"+debitLedgerForSubHead);
									System.out.println("Debit"+creditLedgerForSubHeadScholarship);
									feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForJournal,
											debitLedgerForSubHead,creditLedgerForSubHeadScholarship,
											  narration.toString(),paymentDetails,Double.parseDouble(subheadList.get(i).getAssignFee()),
											  subheadList.get(i).getRenewstudentId(),schoolId);
									System.out.println("Done");
								}
							}else {
								FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForJournal,debitLedgerForSubHead,
										creditLedgerForSubHeadFees,subheadList.get(i).getRenewstudentId(),"SubHeadWise-Fee");
								
								if(existingRecord==null) {
									System.out.println("Credit"+debitLedgerForSubHead);
									System.out.println("Debit"+creditLedgerForSubHeadFees);
									addJournalEntry(modelmap,journalId,subheadList.get(i).getReceiptDate(),voucherType.toString(),
											debitLedgerForSubHead,creditLedgerForSubHeadFees,
											  narration.toString(),paymentDetails,voucherNo.toString(),Double.parseDouble(subheadList.get(i).getAssignFee()),"0",
											  ""+subheadList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
									System.out.println("Done");
								}
								else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+subheadList.get(i).getAssignFee())) || !existingRecord.getNarration().equals(""+narration+"-ClassWise-Fees")) {
									System.out.println("Credit"+debitLedgerForSubHead);
									System.out.println("Debit"+creditLedgerForSubHeadFees);
									feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForJournal,
											debitLedgerForSubHead,creditLedgerForSubHeadFees,
											  narration.toString(),paymentDetails,Double.parseDouble(subheadList.get(i).getAssignFee()),
											  subheadList.get(i).getRenewstudentId(),schoolId);
									System.out.println("Done");
								}
							}
							
							
							
							
							
						}
						if(StringUtils.isNotEmpty(subheadList.get(i).getAssignFine()) && Double.parseDouble(subheadList.get(i).getAssignFine())>0) {
							
							StringBuffer voucherNo = new StringBuffer();
							voucherNo.append("Jo").append("-").append(String.valueOf(index+2));
							StringBuffer voucherType = new StringBuffer(); 
							voucherType.append(""+voucherTypeIdForJournal).append("_").append(voucherNo.toString());
							StringBuffer narration = new StringBuffer();
							String headName = "";
							String subHeadName ="";
							List list = feesLinkToAccountRepositoryImpl.getHeadAndSubHeadDetails(""+subheadList.get(i).getSubheadId(),schoolId);
							if(CollectionUtils.isNotEmpty(list) && list.get(0) !=null) {
								Object[] obj = (Object[]) list.get(0);
								headName = (String) obj[2];
								subHeadName = (String) obj[0];
							}
						
							narration = new StringBuffer();
							narration.append(subheadList.get(i).getYearName()).append("-").append(subheadList.get(i).getStandardName()).append("-")							
									  .append( subheadList.get(i).getRegistartionNumber()).append("-")
									 .append(subheadList.get(i).getStudentName()).append("-").append(headName).append("-")
									 .append(subHeadName).append("-").append("-").append("SubHeadWise-Fine");
							
							FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForJournal,debitLedgerForSubHead,
									creditLedgerForSubHeadFine,subheadList.get(i).getRenewstudentId(),"SubHeadWise-Fine");
							if(existingRecord==null) {
								System.out.println("Credit"+debitLedgerForSubHead);
								System.out.println("Debit"+creditLedgerForSubHeadFine);
								addJournalEntry(modelmap,journalId,subheadList.get(i).getReceiptDate(),voucherType.toString(),
										debitLedgerForSubHead,creditLedgerForSubHeadFine,
										  narration.toString(),paymentDetails,voucherNo.toString(),Double.parseDouble(subheadList.get(i).getAssignFine()),"0",
										  ""+subheadList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
								System.out.println("Done");
							}
							else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+subheadList.get(i).getAssignFine())) || !existingRecord.getNarration().equals(""+narration+"-ClassWise-Fees")) {
								System.out.println("Credit"+debitLedgerForSubHead);
								System.out.println("Debit"+creditLedgerForSubHeadFine);
								feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForJournal,
										debitLedgerForSubHead,creditLedgerForSubHeadFine,
										  narration.toString(),paymentDetails,Double.parseDouble(subheadList.get(i).getAssignFine()),
										  subheadList.get(i).getRenewstudentId(),schoolId);
								System.out.println("Done");
								
							}
							System.out.println("Inserted journal record for "+ i + "student renew " + subheadList.get(i).getRenewstudentId());
							
						}
					}
					
				}
			}
			else {
				List<FeesToAccountLinkBean> discountBean = new ArrayList<>();
				
				Integer cashOrBankLedger =0;
				Integer creditLedger=0;
				String receiptId="0";
				int index=0;
				Double discount=0.0;
				System.out.println("--------------------Receipt size"+receiptEntriesList.size());
				for(int i=0;i<receiptEntriesList.size();i++){
					if(!("Cash").equalsIgnoreCase(receiptEntriesList.get(i).getPayType())) {
						if(StringUtils.isNotEmpty(receiptEntriesList.get(i).getDDCHQno())) {
							paymentDetails=("Cheque").concat("--").concat(receiptEntriesList.get(i).getDDCHQno());
						}
						else {
							paymentDetails=("Cheque").concat("--").concat("00");

						}
							
					}
					//String csSchoolId1 = generatePrimaryKey("1","ORIGINAL",globalDbSchoolKey);
					List creditLedgerList1= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_subhead_mapp","subheadId",receiptEntriesList.get(i).getSubheadId().toString(),csSchoolId,schoolId);
					if(CollectionUtils.isNotEmpty(creditLedgerList1) &&  creditLedgerList1.get(0) !=null) {
						Object[] obj1 = (Object[]) creditLedgerList1.get(0);
						creditLedger =(Integer) obj1[0];
					}
					
					Integer creditLedgerForSubHeadScholarship=null;
					List creditLedgerForSubHeadScholarships = new ArrayList<>();
					creditLedgerForSubHeadScholarships= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Excess Fees",csSchoolId,schoolId);
					if(CollectionUtils.isNotEmpty(creditLedgerForSubHeadScholarships) &&  creditLedgerForSubHeadScholarships.get(0) !=null) {
						Object[] obj = (Object[]) creditLedgerForSubHeadScholarships.get(0);
						creditLedgerForSubHeadScholarship =(Integer) obj[0];
					}
					
					
					if(receiptEntriesList.get(i).getPayTypeOrPrintFlag().equals("2")) {
						List debitLedgerList= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_bank_mapp","bankId",""+receiptEntriesList.get(i).getBankId(),csSchoolId,schoolId);
						if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
							Object[] obj = (Object[]) debitLedgerList.get(0);
							cashOrBankLedger = (Integer) obj[0];
						}
					}
					
					else if(!receiptEntriesList.get(i).getPayType().equalsIgnoreCase("Cash")) {
						List debitLedgerList= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_bank_mapp","bankId",receiptEntriesList.get(i).getBankId().toString(),csSchoolId,schoolId);
						if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
							Object[] obj = (Object[]) debitLedgerList.get(0);
							cashOrBankLedger = (Integer) obj[0];
						}
					} else {
						List debitLedgerList = feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Cash Account ",csSchoolId,schoolId);
						if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
							Object[] obj = (Object[]) debitLedgerList.get(0);
							cashOrBankLedger = (Integer) obj[0];
						}
					}

					Integer debitLedgerForDiscount=null;
					List debitLedgerListForClassFees = new ArrayList<>();
					debitLedgerListForClassFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Fees Discount",csSchoolId,schoolId);
					if(CollectionUtils.isNotEmpty(debitLedgerListForClassFees) &&  debitLedgerListForClassFees.get(0) !=null) {
						Object[] obj = (Object[]) debitLedgerListForClassFees.get(0);
						debitLedgerForDiscount =(Integer) obj[0];
						
					}
			
					
					System.out.println("--------------------Receipt record"+receiptEntriesList.get(i).getReceiptNo());
					
					Integer vouchterType = feesLinkToAccountRepositoryImpl.getVouherId("Receipt",csSchoolId,schoolId);
				
				 	if(!receiptEntriesList.get(i).getReceiptId().equals(receiptId)) {
				 		index = feesLinkToAccountRepositoryImpl.getMaxReceiptId(schoolId);
				 		discount=0.0;
				 		discount = feesLinkToAccountRepositoryImpl.getReceiptWiseDiscount(receiptEntriesList.get(i).getReceiptId());

				 		StringBuffer voucherNo = new StringBuffer();
						voucherNo.append("Re").append("-").append(String.valueOf(index+2));
						StringBuffer voucherType = new StringBuffer(); 
						voucherType.append(""+vouchterType).append("_").append(voucherNo.toString());
						
						StringBuffer narration=new StringBuffer();
					    narration.append(receiptEntriesList.get(i).getYearName()).append("-").append(receiptEntriesList.get(i).getStandardName()).append("-")							
						  .append( receiptEntriesList.get(i).getRegistartionNumber()).append("-")
						  .append(receiptEntriesList.get(i).getStudentName()).append("-").append(receiptEntriesList.get(i).getHeadName()).append("-")
						  .append(receiptEntriesList.get(i).getSubHeadName());
						if(discount!=null && discount!=0){
							System.out.println("Discount Receipt  entries");
							FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,cashOrBankLedger,debitLedgerForDiscount,
									receiptEntriesList.get(i).getRenewstudentId(),"-SubHeadWise-Fees Discount Receipt");
							System.out.println("Credit"+debitLedgerForDiscount);
							System.out.println("Debit"+cashOrBankLedger);
							if(existingRecord==null) {
								
								addJournalEntry(modelmap,journalId,receiptEntriesList.get(i).getReceiptDate(),
										voucherType.toString(),cashOrBankLedger,debitLedgerForDiscount, narration+"-SubheadWise-Fees Discount Receipt",
										paymentDetails,"Re-"+String.valueOf(index+1),-discount,"0",""+receiptEntriesList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);				
							}
							else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+-discount)) || !existingRecord.getNarration().equals(narration+"-SubHeadWise-Fees Discount")) {
								feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,cashOrBankLedger,debitLedgerForDiscount, 
										narration+"-SubheadWise-Fees Discount Receipt",paymentDetails,-discount,receiptEntriesList.get(i).getRenewstudentId(),schoolId);
							}
						
							System.out.println("done");
						}
						
						
						System.out.println("Inserted discount record for "+ i + "student renew " + receiptEntriesList.get(i).getRenewstudentId());
						
				 		
				 	}
				
										
					String name = receiptEntriesList.get(i).getStudentName();
				/*	if(name.contains(".")) {
						name = name.split("\\.")[1];
					}
					if (name.charAt(0) == ' ') {
						name = name.replaceFirst(" ", "");
					}
					if(name.contains(" ")) {
				    	String nameArr[] = name.split(" ");
					    int size = nameArr.length;
					    if(size==3) {
					    	name = nameArr[0].concat(" ").concat(nameArr[1]);
					    }
					}*/
							
					String narrationFees=receiptEntriesList.get(i).getYearName()+"-"+receiptEntriesList.get(i).getStandardName()+"-"+receiptEntriesList.get(i).getRegistartionNumber()+"-"+name+"-"+receiptEntriesList.get(i).getHeadName()+"-"+receiptEntriesList.get(i).getSubHeadName()+"-"+receiptEntriesList.get(i).getReceiptNo()+"-Fees(R.E-"+receiptEntriesList.get(i).getReceiptId()+")";
					String narrationFine=receiptEntriesList.get(i).getYearName()+"-"+receiptEntriesList.get(i).getStandardName()+"-"+receiptEntriesList.get(i).getRegistartionNumber()+"-"+name+"-"+receiptEntriesList.get(i).getHeadName()+"-"+receiptEntriesList.get(i).getSubHeadName()+"-"+receiptEntriesList.get(i).getReceiptNo()+"-Fine(R.E-"+receiptEntriesList.get(i).getReceiptId()+")";
					
					if(scholarshipApprId!=0) {
						
						FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,cashOrBankLedger,creditLedger,receiptEntriesList.get(i).getRenewstudentId(),receiptEntriesList.get(i).getReceiptNo()+"-Fees");
						System.out.println("Debte"+creditLedgerForSubHeadScholarship);
						System.out.println("Credit"+cashOrBankLedger);
						if(receiptEntriesList.get(i).getScholarshipFlag().equals("1")) {
							if(existingRecord==null) {
								addJournalEntry(modelmap,journalId,receiptEntriesList.get(i).getReceiptDate(),vouchterType+"_Re-"+String.valueOf(index+1),cashOrBankLedger,creditLedgerForSubHeadScholarship,
										narrationFees, paymentDetails,"Re-"+String.valueOf(index+1),receiptEntriesList.get(i).getPaidFee(),"0",""+receiptEntriesList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);					
							}
							else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+receiptEntriesList.get(i).getPaidFee())) 
									|| !existingRecord.getNarration().equals(narrationFees) || !existingRecord.getPaymentDetails().equals(paymentDetails) ) {
								feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,cashOrBankLedger,creditLedgerForSubHeadScholarship, 
										narrationFees,paymentDetails,receiptEntriesList.get(i).getPaidFee(),receiptEntriesList.get(i).getRenewstudentId(),schoolId);
							}
							System.out.println("Debte"+creditLedgerForSubHeadScholarship);
							System.out.println("Credit"+cashOrBankLedger);
							if(receiptEntriesList.get(i).getPaidFine()>0) {				
								existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,cashOrBankLedger,creditLedger,receiptEntriesList.get(i).getRenewstudentId(),receiptEntriesList.get(i).getReceiptNo()+"-Fine");
								if(existingRecord==null) {
									addJournalEntry(modelmap,journalId,receiptEntriesList.get(i).getReceiptDate(),vouchterType+"_Re-"+String.valueOf(index+2),cashOrBankLedger,creditLedgerForSubHeadScholarship, narrationFine, paymentDetails,"Re-"+String.valueOf(index+2),receiptEntriesList.get(i).getPaidFine(),"0",""+receiptEntriesList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
								}
								else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+receiptEntriesList.get(i).getPaidFine())) || !existingRecord.getNarration().equals(narrationFine)) {
									feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,cashOrBankLedger,creditLedgerForSubHeadScholarship, 
											narrationFine,paymentDetails,receiptEntriesList.get(i).getPaidFine(),receiptEntriesList.get(i).getRenewstudentId(),schoolId);
								}
							}
						}else {
							if(existingRecord==null) {
								addJournalEntry(modelmap,journalId,receiptEntriesList.get(i).getReceiptDate(),vouchterType+"_Re-"+String.valueOf(index+1),cashOrBankLedger,creditLedger,
										narrationFees, paymentDetails,"Re-"+String.valueOf(index+1),receiptEntriesList.get(i).getPaidFee(),"0",""+receiptEntriesList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);					
							}
							else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+receiptEntriesList.get(i).getPaidFee())) 
									|| !existingRecord.getNarration().equals(narrationFees) || !existingRecord.getPaymentDetails().equals(paymentDetails) ) {
								feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,cashOrBankLedger,creditLedger, 
										narrationFees,paymentDetails,receiptEntriesList.get(i).getPaidFee(),receiptEntriesList.get(i).getRenewstudentId(),schoolId);
							}
							System.out.println("Debte"+creditLedger);
							System.out.println("Credit"+cashOrBankLedger);
							if(receiptEntriesList.get(i).getPaidFine()>0) {				
								existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,cashOrBankLedger,creditLedger,receiptEntriesList.get(i).getRenewstudentId(),receiptEntriesList.get(i).getReceiptNo()+"-Fine");
								if(existingRecord==null) {
									addJournalEntry(modelmap,journalId,receiptEntriesList.get(i).getReceiptDate(),vouchterType+"_Re-"+String.valueOf(index+2),cashOrBankLedger,creditLedger, narrationFine, paymentDetails,"Re-"+String.valueOf(index+2),receiptEntriesList.get(i).getPaidFine(),"0",""+receiptEntriesList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
								}
								else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+receiptEntriesList.get(i).getPaidFine())) || !existingRecord.getNarration().equals(narrationFine)) {
									feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,cashOrBankLedger,creditLedger, 
											narrationFine,paymentDetails,receiptEntriesList.get(i).getPaidFine(),receiptEntriesList.get(i).getRenewstudentId(),schoolId);
								}
							}
						}
						
					}else {
						FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,cashOrBankLedger,creditLedger,receiptEntriesList.get(i).getRenewstudentId(),receiptEntriesList.get(i).getReceiptNo()+"-Fees");
						System.out.println("Debte"+creditLedger);
						System.out.println("Credit"+cashOrBankLedger);
						if(existingRecord==null) {
							addJournalEntry(modelmap,journalId,receiptEntriesList.get(i).getReceiptDate(),vouchterType+"_Re-"+String.valueOf(index+1),cashOrBankLedger,creditLedger,
									narrationFees, paymentDetails,"Re-"+String.valueOf(index+1),receiptEntriesList.get(i).getPaidFee(),"0",""+receiptEntriesList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);					
						}
						else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+receiptEntriesList.get(i).getPaidFee())) 
								|| !existingRecord.getNarration().equals(narrationFees) || !existingRecord.getPaymentDetails().equals(paymentDetails) ) {
							feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,cashOrBankLedger,creditLedger, 
									narrationFees,paymentDetails,receiptEntriesList.get(i).getPaidFee(),receiptEntriesList.get(i).getRenewstudentId(),schoolId);
						}
						System.out.println("Debte"+creditLedger);
						System.out.println("Credit"+cashOrBankLedger);
						if(receiptEntriesList.get(i).getPaidFine()>0) {				
							existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,cashOrBankLedger,creditLedger,receiptEntriesList.get(i).getRenewstudentId(),receiptEntriesList.get(i).getReceiptNo()+"-Fine");
							if(existingRecord==null) {
								addJournalEntry(modelmap,journalId,receiptEntriesList.get(i).getReceiptDate(),vouchterType+"_Re-"+String.valueOf(index+2),cashOrBankLedger,creditLedger, narrationFine, paymentDetails,"Re-"+String.valueOf(index+2),receiptEntriesList.get(i).getPaidFine(),"0",""+receiptEntriesList.get(i).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
							}
							else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+receiptEntriesList.get(i).getPaidFine())) || !existingRecord.getNarration().equals(narrationFine)) {
								feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,cashOrBankLedger,creditLedger, 
										narrationFine,paymentDetails,receiptEntriesList.get(i).getPaidFine(),receiptEntriesList.get(i).getRenewstudentId(),schoolId);
							}
						}
					}
					
					
					
					receiptId=receiptEntriesList.get(i).getReceiptId();
					System.out.println("Inserted receipt record for "+ i + "student renew" + receiptEntriesList.get(i).getRenewstudentId());
				}
			}
			System.out.println("Done");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("----------------------------------------------------EXCEPTION------------------------------------------------------------------------------------");
			e.printStackTrace();
		}
	}



	private void createReceiptEntriesForAccount(String regNo, Integer yearId, Integer schoolId) {
		String paidThrough="1";
		String receiptId="0";
		String updatedBy="1";
		String oldEntries= "1";
		List<FeesToAccountLinkBean> paidFeeReportList = feesLinkToAccountRepositoryImpl.getStudentDetailsForLedgers(regNo,yearId,receiptId,schoolId);
		List<FeesToAccountLinkBean> schoolMasterBean= feesLinkToAccountRepositoryImpl.getCurrentUserForLogin(schoolId);
		String globalDbSchoolKey=(""+schoolMasterBean.get(0).getSchoolId().toString().subSequence(0, 1)).concat(schoolMasterBean.get(0).getSchoolKey());
		
		if(CollectionUtils.isNotEmpty(paidFeeReportList)) {
			addDiscountJVEntries(paidFeeReportList,schoolId,globalDbSchoolKey,paidThrough,receiptId,updatedBy,oldEntries);
		}
		
		if(CollectionUtils.isNotEmpty(paidFeeReportList)) {
			addTaxReceiptEntries(paidFeeReportList,schoolId,globalDbSchoolKey,paidThrough,receiptId,updatedBy,oldEntries); 
		}
		  
		if(CollectionUtils.isNotEmpty(paidFeeReportList)) {
		  addExcessFeeReceiptEntries(paidFeeReportList,schoolId,globalDbSchoolKey,paidThrough,receiptId,updatedBy,oldEntries);
		}
		 
		Double feeAmount =0.0;
		Double fineAmount =0.0;
		boolean processEntries = false;
		String entryDate=new SimpleDateFormat("dd-MM-yyyy").format(new Date());

		String typeSettingId=generatePrimaryKey("10","ORIGINAL",globalDbSchoolKey);
		List<FeesettingBean>  validateimage1 = feesLinkToAccountRepositoryImpl.getFeesReceiptsettingForUnit(Integer.parseInt(typeSettingId),schoolId);
		int unitSettingFlag=0;
		if(CollectionUtils.isNotEmpty(validateimage1)) {
			unitSettingFlag=Integer.parseInt(validateimage1.get(0).getSettingFlag());
		}
		String csSchoolIdForClass="";
		String csSchoolIdForStudent="";
		Integer standardId = paidFeeReportList.get(0).getStandardId();
		List<FeesToAccountLinkBean> list=feesLinkToAccountRepositoryImpl.getTypeOfUnitIdStdWise(standardId,schoolId);
		if(unitSettingFlag == 1) {
			if(list.get(0).getClassId()!=null){
				csSchoolIdForClass=generatePrimaryKey(truncateKey(""+list.get(0).getClassId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
				csSchoolIdForStudent=generatePrimaryKey(truncateKey(""+list.get(0).getStudentId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
			}else {
				 csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
			}
		}else {
			csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
			 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
		}
		try {
			if(CollectionUtils.isNotEmpty(paidFeeReportList)) {				
				if((paidThrough.equals("1") && (!paidFeeReportList.get(0).getPayType().equalsIgnoreCase("Cheque") && !paidFeeReportList.get(0).getPayType().equalsIgnoreCase("Challan")))) {
					processEntries = true;
				} else if((paidThrough.equals("2") && (!paidFeeReportList.get(0).getPayType().equalsIgnoreCase("Cheque") && !paidFeeReportList.get(0).getPayType().equalsIgnoreCase("Challan")))) {
					processEntries = true;
				} else if(paidThrough.equals("3")) {
					processEntries = true;
				}
				
				for(int i=0;i<paidFeeReportList.size();i++) {
					feeAmount += paidFeeReportList.get(i).getPaidFee();
					fineAmount += paidFeeReportList.get(i).getPaidFine();
					entryDate=paidFeeReportList.get(0).getReceiptDate(); 
				}
				
				if(processEntries) {
					ModelMap modelmap = new ModelMap();
					Long journalId=Long.parseLong("0");
				 	String paymentDetails="";
				 					 	
					// Student Fees
					int vouchterType = feesLinkToAccountRepositoryImpl.getVouherId("Receipt",csSchoolIdForStudent,schoolId);
					String feesCsSchoolId=csSchoolIdForStudent;
				 	int i = feesLinkToAccountRepositoryImpl.getMaxReceiptId(schoolId);
				 	Integer debitLedger = null;
					if(!paidFeeReportList.get(0).getPayType().equalsIgnoreCase("Cash")) {
						List debitLedgerList= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_bank_mapp","bankId",paidFeeReportList.get(0).getBankId().toString(),csSchoolIdForStudent,schoolId);
						if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
							Object[] obj = (Object[]) debitLedgerList.get(0);
							debitLedger = (Integer) obj[0];
						}
					} else {
						List debitLedgerList = feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Cash Account ",csSchoolIdForStudent,schoolId);
						if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
							Object[] obj = (Object[]) debitLedgerList.get(0);
							debitLedger = (Integer) obj[0];
						}
					}
					String name = paidFeeReportList.get(0).getStudentName();
				/*	if(name.contains(".")) {
						name = name.split("\\.")[1];
					}
					if (name.charAt(0) == ' ') {
						name = name.replaceFirst(" ", "");
					}
					if(name.contains(" ")) {
				    	String nameArr[] = name.split(" ");
					    int size = nameArr.length;
					    if(size==3) {
					    	name = nameArr[0].concat(" ").concat(nameArr[1]);
					    }
					}*/
					
					Integer creditLedger=null;					
					List creditLedgerList= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_student_mapp","studentId",paidFeeReportList.get(0).getStudentId().toString(),csSchoolIdForStudent,schoolId);
					if(CollectionUtils.isNotEmpty(creditLedgerList) &&  creditLedgerList.get(0) !=null) {
						Object[] obj = (Object[]) creditLedgerList.get(0);
						creditLedger =(Integer) obj[0];
					}
										
					String narrationFees=paidFeeReportList.get(0).getYearName()+"-"+paidFeeReportList.get(0).getStandardName()+"-"+paidFeeReportList.get(0).getRegistartionNumber()+"-"+name+"-"+paidFeeReportList.get(0).getReceiptNo()+"-Fees(R.E-"+paidFeeReportList.get(0).getReceiptId()+")";
					String narrationFine=paidFeeReportList.get(0).getYearName()+"-"+paidFeeReportList.get(0).getStandardName()+"-"+paidFeeReportList.get(0).getRegistartionNumber()+"-"+name+"-"+paidFeeReportList.get(0).getReceiptNo()+"-Fine(R.E-"+paidFeeReportList.get(0).getReceiptId()+")";
					if(!("Cash").equalsIgnoreCase(paidFeeReportList.get(0).getPayType())) {
						paymentDetails=paidFeeReportList.get(0).getPayType().concat("--").concat(paidFeeReportList.get(0).getDDCHQno());
					}
					
					FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,debitLedger,creditLedger,paidFeeReportList.get(0).getRenewstudentId(),paidFeeReportList.get(0).getReceiptNo()+"-Fees");
					if(existingRecord==null) {
						addJournalEntry(modelmap,journalId,entryDate,vouchterType+"_Re-"+String.valueOf(i+1),debitLedger,creditLedger, narrationFees, paymentDetails,"Re-"+String.valueOf(i+1),feeAmount,"0",""+paidFeeReportList.get(0).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
					}
					else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+feeAmount)) || !existingRecord.getNarration().equals(narrationFees)) {
						feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,debitLedger,creditLedger, 
								narrationFees,paymentDetails,feeAmount,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
					}
					if(fineAmount>0) {	
						existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,debitLedger,creditLedger,paidFeeReportList.get(0).getRenewstudentId(),paidFeeReportList.get(0).getReceiptNo()+"Fine");
						if(existingRecord==null) {
							addJournalEntry(modelmap,journalId,entryDate,vouchterType+"_Re-"+String.valueOf(i+2),debitLedger,creditLedger, narrationFine, paymentDetails,"Re-"+String.valueOf(i+2),fineAmount,"0",""+paidFeeReportList.get(0).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
						}
						else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+fineAmount)) 
								|| !existingRecord.getNarration().equals(narrationFine) || !existingRecord.getPaymentDetails().equals(paymentDetails)) {
							feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,debitLedger,creditLedger, 
									narrationFine,paymentDetails,fineAmount,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
						}
					}
										
					// Class ledger
					vouchterType = feesLinkToAccountRepositoryImpl.getVouherId("Receipt",csSchoolIdForClass,schoolId);
					 feesCsSchoolId=csSchoolIdForClass;
					if(!paidFeeReportList.get(0).getPayType().equalsIgnoreCase("Cash")) {
						List debitLedgerList= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_bank_mapp","bankId",paidFeeReportList.get(0).getBankId().toString(),csSchoolIdForClass,schoolId);
						if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
							Object[] obj = (Object[]) debitLedgerList.get(0);
							debitLedger = (Integer) obj[0];
						}
					} else {
						List debitLedgerList = feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Cash Account ",csSchoolIdForClass,schoolId);
						if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
							Object[] obj = (Object[]) debitLedgerList.get(0);
							debitLedger = (Integer) obj[0];
						}
					}
				 	i = feesLinkToAccountRepositoryImpl.getMaxReceiptId(schoolId);
										
					List creditLedgerList1= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_standard_mapp","standardId",paidFeeReportList.get(0).getStandardId().toString(),csSchoolIdForClass,schoolId);
					if(CollectionUtils.isNotEmpty(creditLedgerList1) &&  creditLedgerList1.get(0) !=null) {
						Object[] obj1 = (Object[]) creditLedgerList1.get(0);
						creditLedger =(Integer) obj1[0];
					}
							
					existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,debitLedger,creditLedger,paidFeeReportList.get(0).getRenewstudentId(),paidFeeReportList.get(0).getReceiptNo()+"-Fees");
					if(existingRecord==null) {
						addJournalEntry(modelmap,journalId,entryDate,vouchterType+"_Re-"+String.valueOf(i+1),debitLedger,creditLedger,
								narrationFees, paymentDetails,"Re-"+String.valueOf(i+1),feeAmount,"0",""+paidFeeReportList.get(0).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);					
					}
					else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+feeAmount)) || !existingRecord.getNarration().equals(narrationFees)) {
						feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,debitLedger,creditLedger, 
								narrationFees,paymentDetails,feeAmount,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
					}
					if(fineAmount>0) {				
						existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,vouchterType,debitLedger,creditLedger,paidFeeReportList.get(0).getRenewstudentId(),paidFeeReportList.get(0).getReceiptNo()+"-Fine");
						if(existingRecord==null) {
							addJournalEntry(modelmap,journalId,entryDate,vouchterType+"_Re-"+String.valueOf(i+2),debitLedger,creditLedger, narrationFine, paymentDetails,"Re-"+String.valueOf(i+2),fineAmount,"0",""+paidFeeReportList.get(0).getRenewstudentId(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
						}
						else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+fineAmount)) || !existingRecord.getNarration().equals(narrationFine)) {
							feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),vouchterType,debitLedger,creditLedger, 
									narrationFine,paymentDetails,fineAmount,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
						}
					}
				}			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void addExcessFeeReceiptEntries(List<FeesToAccountLinkBean> paidFeeReportList, Integer schoolId,
			String globalDbSchoolKey, String paidThrough, String receiptId1, String updatedBy, String oldEntries) {
		
		String typeSettingId=generatePrimaryKey("10","ORIGINAL",globalDbSchoolKey);
		List<FeesettingBean>  validateimage1 = feesLinkToAccountRepositoryImpl.getFeesReceiptsettingForUnit(Integer.parseInt(typeSettingId),schoolId);
		int unitSettingFlag=0;
		if(CollectionUtils.isNotEmpty(validateimage1)) {
			unitSettingFlag=Integer.parseInt(validateimage1.get(0).getSettingFlag());
		}
		String csSchoolIdForClass="";
		String csSchoolIdForStudent="";
		Integer standardId = paidFeeReportList.get(0).getStandardId();
		List<FeesToAccountLinkBean> list=feesLinkToAccountRepositoryImpl.getTypeOfUnitIdStdWise(standardId,schoolId);
		if(unitSettingFlag == 1) {
			if(list.get(0).getClassId()!=null){
				csSchoolIdForClass=generatePrimaryKey(truncateKey(""+list.get(0).getClassId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey(truncateKey(""+list.get(0).getStudentId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
			}else {
				csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
			}
		}else {
			csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
			 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
		}
		
		int voucherTypeIdForClass = feesLinkToAccountRepositoryImpl.getVouherId("Receipt",csSchoolIdForClass,schoolId);
		int voucherTypeIdForStudent =  feesLinkToAccountRepositoryImpl.getVouherId("Receipt",csSchoolIdForStudent,schoolId);

		Integer debitLedgerForClassFees=null;
		List debitLedgerListForClassFees = new ArrayList<>();
		debitLedgerListForClassFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Excess Fees",csSchoolIdForClass,schoolId);
		if(CollectionUtils.isNotEmpty(debitLedgerListForClassFees) &&  debitLedgerListForClassFees.get(0) !=null) {
			Object[] obj = (Object[]) debitLedgerListForClassFees.get(0);
			debitLedgerForClassFees =(Integer) obj[0];
		}		
		
		Integer debitLedgerForStudentFees=null;
		List debitLedgerListForStudentFees = new ArrayList<>();
		debitLedgerListForStudentFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Excess Fees",csSchoolIdForStudent,schoolId);
		if(CollectionUtils.isNotEmpty(debitLedgerListForStudentFees) &&  debitLedgerListForStudentFees.get(0) !=null) {
			Object[] obj = (Object[]) debitLedgerListForStudentFees.get(0);
			debitLedgerForStudentFees =(Integer) obj[0];
		}
				
		updatedBy="1";
		oldEntries="1";
		ModelMap modelmap = new ModelMap();
		Long journalId=Long.parseLong("0");
		String entryDate=new SimpleDateFormat("dd-MM-yyyy").format(new Date()); 
	 
		int i =0;
		String paymentDetails="";
	 
		String studentName = paidFeeReportList.get(0).getStudentName();
		String StudRegNo = paidFeeReportList.get(0).getRegistartionNumber();
		Integer studentId= paidFeeReportList.get(0).getStudentId();
		//Integer standardId = paidFeeReportList.get(0).getStandardId();
		String yearName = paidFeeReportList.get(0).getStandardName();
		String standardName = paidFeeReportList.get(0).getYearName();
		String receiptId=paidFeeReportList.get(0).getReceiptId();
		Integer creditLedgerForClassFees=null;
		List list1= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_standard_mapp","standardId",standardId.toString(),csSchoolIdForClass,schoolId);
			if(CollectionUtils.isNotEmpty(list1) && list1.get(0) !=null) {
				Object[] obj = (Object[]) list1.get(0);
				creditLedgerForClassFees = Integer.parseInt(obj[0].toString());
			}
			studentName=convertInUTFFormat(studentName);
			StudRegNo=convertInUTFFormat(StudRegNo);
			// Class Wise Journal Entry
			String feesCsSchoolId=csSchoolIdForClass;
		 	int studRenewId=feesLinkToAccountRepositoryImpl.getStudentRenewId(studentId);
			
		 	double sumOfExcessFees=feesLinkToAccountRepositoryImpl.getExcessFees(studentId,receiptId);
		 	
			String narration=yearName+"-"+standardName+"-"+StudRegNo+"-"+studentName;
			if(sumOfExcessFees!=0){
				FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForClass,debitLedgerForClassFees,
						creditLedgerForClassFees,paidFeeReportList.get(0).getRenewstudentId(),"ClassWise-Excess Fees");
				if(existingRecord==null) {
					addJournalEntry(modelmap,journalId,entryDate,voucherTypeIdForClass+"_Re-"+String.valueOf(i+1),
							debitLedgerForClassFees,creditLedgerForClassFees, narration+"-ClassWise-Excess Fees",paymentDetails,"Re-"+String.valueOf(i+1),
							sumOfExcessFees,"0",""+studRenewId,schoolId,updatedBy,feesCsSchoolId,oldEntries);
				}
				else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+sumOfExcessFees)) || !existingRecord.getNarration().equals(narration+"-ClassWise-Excess Fees")) {
					feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForClass,debitLedgerForClassFees,creditLedgerForClassFees, 
							narration+"sumOfExcessFees",paymentDetails,sumOfExcessFees,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
				}
				
			}
			
			// Student Wise receipt Entry
			 feesCsSchoolId=csSchoolIdForStudent;
			Integer creditLedgerForStudentFees=null;
			List creditLLedgerList=feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_student_mapp","studentId",studentId.toString(),csSchoolIdForStudent,schoolId);
		 	if(CollectionUtils.isNotEmpty(creditLLedgerList) && creditLLedgerList.get(0) !=null) {
				Object[] obj1 = (Object[]) creditLLedgerList.get(0);
				creditLedgerForStudentFees = (Integer) obj1[0];
			}
		 	
		 	if(sumOfExcessFees!=0){
		 		FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForStudent,debitLedgerForStudentFees,
		 				creditLedgerForStudentFees,paidFeeReportList.get(0).getRenewstudentId(),"StudentWise-Excess Fees");
				if(existingRecord==null) {
					addJournalEntry(modelmap,journalId,entryDate,voucherTypeIdForStudent+"_Re-"+String.valueOf(i+1),
			 				debitLedgerForStudentFees,creditLedgerForStudentFees, narration+"-StudentWise-Excess Fees", paymentDetails,"Re-"+String.valueOf(i+1),sumOfExcessFees,"0",""+studRenewId,schoolId,updatedBy,feesCsSchoolId,oldEntries);

				}
				else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+sumOfExcessFees)) || !existingRecord.getNarration().equals(narration+"-StudentWise-Excess Fees")) {
					feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForStudent,debitLedgerForStudentFees,creditLedgerForStudentFees, 
							narration+"-StudentWise-Excess Fees",paymentDetails,sumOfExcessFees,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
				}
		 	}
	}



	private void addTaxReceiptEntries(List<FeesToAccountLinkBean> paidFeeReportList, Integer schoolId,
			String globalDbSchoolKey, String paidThrough, String receiptId1, String updatedBy, String oldEntries) {
		
		String typeSettingId=generatePrimaryKey("10","ORIGINAL",globalDbSchoolKey);
		List<FeesettingBean>  validateimage1 = feesLinkToAccountRepositoryImpl.getFeesReceiptsettingForUnit(Integer.parseInt(typeSettingId),schoolId);
		int unitSettingFlag=0;
		if(CollectionUtils.isNotEmpty(validateimage1)) {
			unitSettingFlag=Integer.parseInt(validateimage1.get(0).getSettingFlag());
		}
		String csSchoolIdForClass="";
		String csSchoolIdForStudent="";
		Integer standardId = paidFeeReportList.get(0).getStandardId();
		List<FeesToAccountLinkBean> list=feesLinkToAccountRepositoryImpl.getTypeOfUnitIdStdWise(standardId,schoolId);
		if(unitSettingFlag == 1) {
			if(list.get(0).getClassId()!=null){
				csSchoolIdForClass=generatePrimaryKey(truncateKey(""+list.get(0).getClassId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey(truncateKey(""+list.get(0).getStudentId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
			}else {
				csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
			}
			
		}else {
			csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
			 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
		}
		
		
		int voucherTypeIdForClass = feesLinkToAccountRepositoryImpl.getVouherId("Receipt",csSchoolIdForClass,schoolId);
		int voucherTypeIdForStudent =  feesLinkToAccountRepositoryImpl.getVouherId("Receipt",csSchoolIdForStudent,schoolId);

		Integer debitLedgerForClassFees=null;
		List debitLedgerListForClassFees = new ArrayList<>();
		debitLedgerListForClassFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Tax Collected From Fees",csSchoolIdForClass,schoolId);
		if(CollectionUtils.isNotEmpty(debitLedgerListForClassFees) &&  debitLedgerListForClassFees.get(0) !=null) {
			Object[] obj = (Object[]) debitLedgerListForClassFees.get(0);
			debitLedgerForClassFees =(Integer) obj[0];
		}
				
		Integer debitLedgerForStudentFees=null;
		List debitLedgerListForStudentFees = new ArrayList<>();
		debitLedgerListForStudentFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Tax Collected From Fees",csSchoolIdForStudent,schoolId);
		if(CollectionUtils.isNotEmpty(debitLedgerListForStudentFees) &&  debitLedgerListForStudentFees.get(0) !=null) {
			Object[] obj = (Object[]) debitLedgerListForStudentFees.get(0);
			debitLedgerForStudentFees =(Integer) obj[0];
		}
				
		updatedBy="1";
		oldEntries="1";
		ModelMap modelmap = new ModelMap();
		Long journalId=Long.parseLong("0");
		String entryDate=new SimpleDateFormat("dd-MM-yyyy").format(new Date()); 
	 
		int i =0;
		String paymentDetails="";
	 
		String studentName = paidFeeReportList.get(0).getStudentName();
		String StudRegNo = paidFeeReportList.get(0).getRegistartionNumber();
		Integer studentId= paidFeeReportList.get(0).getStudentId();
		//Integer standardId = paidFeeReportList.get(0).getStandardId();
		String yearName = paidFeeReportList.get(0).getStandardName();
		String standardName = paidFeeReportList.get(0).getYearName();
		String receiptId=paidFeeReportList.get(0).getReceiptId();
		Integer creditLedgerForClassFees=null;
		List list1= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_standard_mapp","standardId",standardId.toString(),csSchoolIdForClass,schoolId);
			if(CollectionUtils.isNotEmpty(list1) && list1.get(0) !=null) {
				Object[] obj = (Object[]) list1.get(0);
				creditLedgerForClassFees = Integer.parseInt(obj[0].toString());
			}
			studentName=convertInUTFFormat(studentName);
			StudRegNo=convertInUTFFormat(StudRegNo);
			// Class Wise Journal Entry
			String feesCsSchoolId=csSchoolIdForClass;
					
		 	int studRenewId=feesLinkToAccountRepositoryImpl.getStudentRenewId(studentId);			
		 	double sumOfTax=feesLinkToAccountRepositoryImpl.getTax(studentId,receiptId);
		 	
			String narration=yearName+"-"+standardName+"-"+StudRegNo+"-"+studentName;
			if(sumOfTax!=0){
				FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForClass,debitLedgerForClassFees,
						creditLedgerForClassFees,paidFeeReportList.get(0).getRenewstudentId(),"ClassWise-Tax Collected From Fees");
				if(existingRecord==null) {
					addJournalEntry(modelmap,journalId,entryDate,voucherTypeIdForClass+"_Re-"+String.valueOf(i+1),debitLedgerForClassFees,
							creditLedgerForClassFees, narration+"-ClassWise-Tax Collected From Fees",paymentDetails,"Re-"+String.valueOf(i+1),sumOfTax,"0",""+studRenewId,schoolId,updatedBy,feesCsSchoolId,oldEntries);
				}
				else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+sumOfTax)) || !existingRecord.getNarration().equals(narration+"-ClassWise-Tax Collected From Fees")) {
					feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForClass,debitLedgerForClassFees,creditLedgerForClassFees, 
							narration+"-ClassWise-Tax Collected From Fees",paymentDetails,sumOfTax,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
				}
				
			}
			
			// Student Wise receipt Entry
			 feesCsSchoolId=csSchoolIdForStudent;
			Integer creditLedgerForStudentFees=null;
			List creditLLedgerList=feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_student_mapp","studentId",studentId.toString(),csSchoolIdForStudent,schoolId);
		 	if(CollectionUtils.isNotEmpty(creditLLedgerList) && creditLLedgerList.get(0) !=null) {
				Object[] obj1 = (Object[]) creditLLedgerList.get(0);
				creditLedgerForStudentFees = (Integer) obj1[0];
			}
		 	if(sumOfTax!=0){
		 		FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForStudent,debitLedgerForStudentFees,
		 				creditLedgerForStudentFees,paidFeeReportList.get(0).getRenewstudentId(),"StudentWise-Tax Collected From Fees");
				if(existingRecord==null) {
					addJournalEntry(modelmap,journalId,entryDate,voucherTypeIdForStudent+"_Re-"+String.valueOf(i+1),debitLedgerForStudentFees
							,creditLedgerForStudentFees, narration+"-StudentWise-Tax Collected From Fees",paymentDetails,"Re-"+String.valueOf(i+1),sumOfTax,"0",""+studRenewId,schoolId,updatedBy,feesCsSchoolId,oldEntries);
				}
				else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+sumOfTax)) || !existingRecord.getNarration().equals(narration+"-StudentWise-Tax Collected From Fees")) {
					feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForStudent,debitLedgerForStudentFees,creditLedgerForStudentFees, 
							narration+"-StudentWise-Tax Collected From Fees",paymentDetails,sumOfTax,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
				}
		 		
		 	}
	
	}



	private void addDiscountJVEntries(List<FeesToAccountLinkBean> paidFeeReportList, Integer schoolId, String globalDbSchoolKey, String paidThrough, String receiptId, String updatedBy, String oldEntries) {		
		String typeSettingId=generatePrimaryKey("10","ORIGINAL",globalDbSchoolKey);
		List<FeesettingBean>  validateimage1 = feesLinkToAccountRepositoryImpl.getFeesReceiptsettingForUnit(Integer.parseInt(typeSettingId),schoolId);
		int unitSettingFlag=0;
		if(CollectionUtils.isNotEmpty(validateimage1)) {
			unitSettingFlag=Integer.parseInt(validateimage1.get(0).getSettingFlag());
		}
		String csSchoolIdForClass="";
		String csSchoolIdForStudent="";
		Integer standardId = paidFeeReportList.get(0).getStandardId();
		List<FeesToAccountLinkBean> list=feesLinkToAccountRepositoryImpl.getTypeOfUnitIdStdWise(standardId,schoolId);
		if(unitSettingFlag == 1) {
			if(list.get(0).getClassId()!=null){
				csSchoolIdForClass=generatePrimaryKey(truncateKey(""+list.get(0).getClassId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey(truncateKey(""+list.get(0).getStudentId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
			}else {
				csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
			}
			
		}else {
			csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
			 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
		}
		
		
		int voucherTypeIdForClass = feesLinkToAccountRepositoryImpl.getVouherId("Journal",csSchoolIdForClass,schoolId);
		int voucherTypeIdForStudent =  feesLinkToAccountRepositoryImpl.getVouherId("Journal",csSchoolIdForStudent,schoolId);

		Integer debitLedgerForClassFees=null;
		List debitLedgerListForClassFees = new ArrayList<>();
		debitLedgerListForClassFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Fees Discount",csSchoolIdForClass,schoolId);
		if(CollectionUtils.isNotEmpty(debitLedgerListForClassFees) &&  debitLedgerListForClassFees.get(0) !=null) {
			Object[] obj = (Object[]) debitLedgerListForClassFees.get(0);
			debitLedgerForClassFees =(Integer) obj[0];
		}		
		
		Integer debitLedgerForStudentFees=null;
		List debitLedgerListForStudentFees = new ArrayList<>();
		debitLedgerListForStudentFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Fees Discount",csSchoolIdForStudent,schoolId);
		if(CollectionUtils.isNotEmpty(debitLedgerListForStudentFees) &&  debitLedgerListForStudentFees.get(0) !=null) {
			Object[] obj = (Object[]) debitLedgerListForStudentFees.get(0);
			debitLedgerForStudentFees =(Integer) obj[0];
		}
		
		updatedBy="1";
		oldEntries="1";
		ModelMap modelmap = new ModelMap();
		Long journalId=Long.parseLong("0");
		String entryDate=new SimpleDateFormat("dd-MM-yyyy").format(new Date()); 
	 
		int i =0;
		String paymentDetails="";
	 
		String studentName = paidFeeReportList.get(0).getStudentName();
		String StudRegNo = paidFeeReportList.get(0).getRegistartionNumber();
		Integer studentId= paidFeeReportList.get(0).getStudentId();
		//Integer standardId = paidFeeReportList.get(0).getStandardId();
		String yearName = paidFeeReportList.get(0).getStandardName();
		String standardName = paidFeeReportList.get(0).getYearName();
		
		Integer creditLedgerForClassFees=null;
		List list1= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_standard_mapp","standardId",standardId.toString(),csSchoolIdForClass,schoolId);
			if(CollectionUtils.isNotEmpty(list1) && list1.get(0) !=null) {
				Object[] obj = (Object[]) list1.get(0);
				creditLedgerForClassFees = Integer.parseInt(obj[0].toString());
			}
			studentName=convertInUTFFormat(studentName);
			StudRegNo=convertInUTFFormat(StudRegNo);
			// Class Wise Journal Entry
			String feesCsSchoolId=csSchoolIdForClass;
		 	int studRenewId=feesLinkToAccountRepositoryImpl.getStudentRenewId(studentId);			
		 	double sumOfDiscount=feesLinkToAccountRepositoryImpl.getDisount(studentId);
		 	
			String narration=yearName+"-"+standardName+"-"+StudRegNo+"-"+studentName;
			if(sumOfDiscount!=0){
				FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForClass,debitLedgerForClassFees,creditLedgerForClassFees,paidFeeReportList.get(0).getRenewstudentId(),"-ClassWise-Fees Discount");
				if(existingRecord==null) {
					addJournalEntry(modelmap,journalId,entryDate,voucherTypeIdForClass+"_Jo-"+String.valueOf(i+1),debitLedgerForClassFees,creditLedgerForClassFees, narration+"-ClassWise-Fees Discount",
							paymentDetails,"Jo-"+String.valueOf(i+1),sumOfDiscount,"0",""+studRenewId,schoolId,updatedBy,feesCsSchoolId,oldEntries);				
				}
				else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+sumOfDiscount)) || !existingRecord.getNarration().equals(narration+"-ClassWise-Fees Discount")) {
					feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForClass,debitLedgerForClassFees,creditLedgerForClassFees, 
							narration+"-ClassWise-Fees Discount",paymentDetails,sumOfDiscount,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
				}
				
			}
			
			// Student Wise Journal Entry
			 feesCsSchoolId=csSchoolIdForStudent;
			Integer creditLedgerForStudentFees=null;
			List creditLLedgerList=feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_student_mapp","studentId",studentId.toString(),csSchoolIdForStudent,schoolId);
		 	if(CollectionUtils.isNotEmpty(creditLLedgerList) && creditLLedgerList.get(0) !=null) {
				Object[] obj1 = (Object[]) creditLLedgerList.get(0);
				creditLedgerForStudentFees = (Integer) obj1[0];
			}
		 	
		 	if(sumOfDiscount!=0){
		 		FeesToAccountLinkBean existingRecord = feesLinkToAccountRepositoryImpl.checkRecord(schoolId,voucherTypeIdForStudent,debitLedgerForStudentFees,
		 				creditLedgerForStudentFees,paidFeeReportList.get(0).getRenewstudentId(),"StudentWise-Fees Discount");
				if(existingRecord==null) {
					addJournalEntry(modelmap,journalId,entryDate,voucherTypeIdForStudent+"_Jo-"+String.valueOf(i+1),debitLedgerForStudentFees,
							creditLedgerForStudentFees, narration+"-StudentWise-Fees Discount", paymentDetails,"Jo-"+String.valueOf(i+1),sumOfDiscount,"0",""+studRenewId,schoolId,updatedBy,feesCsSchoolId,oldEntries);			
				}
				else if((Double.parseDouble(existingRecord.getAmount()) != Double.parseDouble(""+sumOfDiscount)) || !existingRecord.getNarration().equals(narration+"-StudentWise-Fees Discount")) {
					feesLinkToAccountRepositoryImpl.updateEntries(existingRecord.getJournalId(),voucherTypeIdForStudent,debitLedgerForStudentFees,creditLedgerForStudentFees, 
							narration+"-StudentWise-Fees Discount",paymentDetails,sumOfDiscount,paidFeeReportList.get(0).getRenewstudentId(),schoolId);
				}
		 	}		
	}



	



	// for generate primary key
	public static String generatePrimaryKey(String... str) {
		String newGenetatedKey=str[0];
		try {
			
			if(str[1].equals("ORIGINAL")) {
				if(str[2]!=null && str[2].length()==10) {
					return str[2];
				}
				if(str[0]!=null && str[0].length()<10) {
					if(str[0].equals("00001")) {
						newGenetatedKey=str[2]+("0000000000".substring(0, ("0000000000".length()-(str[2]).length())))+str[0];
					} else {
						newGenetatedKey=str[2]+("0000000000".substring(0, ("0000000000".length()-(str[2]+str[0]).length())))+str[0];
					}
				}
			}
			if(str[1].equals("WithSchoolKey")) {
				return str[0]+"00001";
			}
			if(str[1].equals("EXECUTE")) {
				return str[0];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
	
	public static String truncateKey(String... id) {
		Integer globalDbSchoolKeyLength=(id[1]).length()+1;
		String subStringId=id[0].substring(globalDbSchoolKeyLength,id[0].length());
		int truncateKey=Integer.parseInt(subStringId);
		return String.valueOf(truncateKey);
	}
	
	public static String convertInUTFFormat(String str) {
		if (StringUtils.isNotEmpty(str)&& !str.equals("null")) {
			try {
				if(str.contains("¤") || str.contains("à") || str.contains("£") || str.contains("�") || str.contains("¥") || str.contains("«") || str.contains("‹") || str.contains("•")){
					str = new String(str.getBytes("iso-8859-1"), "UTF-8");
				}
				return str;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			return "";
		}
		return str;
	}



	@Override
	public List<AssignFeeBean> passFeeSettingEntryInAccount(AssignFeeBean assignFeeBean) {
		try {
			
			
			Integer debitLedgerForStudent = 0;
			Integer debitLedgerForClass = 0;
			Integer debitLedgerForSubHead=0;
			Integer groupSubGroupId=null;
			Integer ledgerHeadId;
			Integer schoolId=assignFeeBean.getSchoolId();
			Integer userId=assignFeeBean.getUserId();
			
			
			String csSchoolIdForClass="";
			String csSchoolIdForStudent="";
			String csSchoolIdForSubhead="";
			List<FeesToAccountLinkBean> schoolMasterBean= feesLinkToAccountRepositoryImpl.getCurrentUserForLogin(schoolId);
			String globalDbSchoolKey=(""+assignFeeBean.getSchoolId().toString().subSequence(0, 1)).concat(schoolMasterBean.get(0).getSchoolKey());
			int unitSettingFlag=0;
			String typeSettingId=generatePrimaryKey("10","ORIGINAL",globalDbSchoolKey);
			List<FeesettingBean>  validateimage1 = feesLinkToAccountRepositoryImpl.getFeesReceiptsettingForUnit(Integer.parseInt(typeSettingId),schoolId);
			if(CollectionUtils.isNotEmpty(validateimage1)) {
				unitSettingFlag=Integer.parseInt(validateimage1.get(0).getSettingFlag());
			}
			List<FeesToAccountLinkBean> listTypeOfUnit=feesLinkToAccountRepositoryImpl.getTypeOfUnitIdStdWise(assignFeeBean.getStandardId(),assignFeeBean.getSchoolId());
			if(unitSettingFlag!=0) {
				if(listTypeOfUnit.get(0).getClassId()!=null) {
					 csSchoolIdForClass=generatePrimaryKey(truncateKey(""+listTypeOfUnit.get(0).getClassId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
					 csSchoolIdForStudent=generatePrimaryKey(truncateKey(""+listTypeOfUnit.get(0).getStudentId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
					 csSchoolIdForSubhead=generatePrimaryKey(truncateKey(""+listTypeOfUnit.get(0).getSubheadId(),globalDbSchoolKey),"ORIGINAL",globalDbSchoolKey);
				}else {
					csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
					 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
					 csSchoolIdForSubhead=generatePrimaryKey("1","ORIGINAL",globalDbSchoolKey); 
				}
			}else {
				 csSchoolIdForClass=generatePrimaryKey("3","ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForStudent=generatePrimaryKey("2","ORIGINAL",globalDbSchoolKey);
				 csSchoolIdForSubhead=generatePrimaryKey("1","ORIGINAL",globalDbSchoolKey); 
			}
			
			 
			
			int voucherTypeIdForClass = feesLinkToAccountRepositoryImpl.getVouherId("Journal",csSchoolIdForClass,schoolId);
			int voucherTypeIdForStudent =  feesLinkToAccountRepositoryImpl.getVouherId("Journal",csSchoolIdForStudent,schoolId);
			int voucherTypeIdForSubHead =  feesLinkToAccountRepositoryImpl.getVouherId("Journal",csSchoolIdForStudent,schoolId);

			Integer creditLedgerForClassFees=null;
			List creditLedgerListForClassFees = new ArrayList<>();
			creditLedgerListForClassFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("School Fees",csSchoolIdForClass,schoolId);
			if(CollectionUtils.isNotEmpty(creditLedgerListForClassFees) &&  creditLedgerListForClassFees.get(0) !=null) {
				Object[] obj = (Object[]) creditLedgerListForClassFees.get(0);
				creditLedgerForClassFees =(Integer) obj[0];
			}
			
			Integer creditLedgerForClassFine=null;
			List creditLedgerListForClassFine = new ArrayList<>();
			creditLedgerListForClassFine= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Late Fees",csSchoolIdForClass,schoolId);
			if(CollectionUtils.isNotEmpty(creditLedgerListForClassFine) &&  creditLedgerListForClassFine.get(0) !=null) {
				Object[] obj = (Object[]) creditLedgerListForClassFine.get(0);
				creditLedgerForClassFine =(Integer) obj[0];
			}
			
			Integer creditLedgerForStudentFees=null;
			List creditLedgerListForStudentFees = new ArrayList<>();
			creditLedgerListForStudentFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("School Fees",csSchoolIdForStudent,schoolId);
			if(CollectionUtils.isNotEmpty(creditLedgerListForStudentFees) &&  creditLedgerListForStudentFees.get(0) !=null) {
				Object[] obj = (Object[]) creditLedgerListForStudentFees.get(0);
				creditLedgerForStudentFees =(Integer) obj[0];
			}
			
			Integer creditLedgerForStudentFine=null;
			List creditLedgerListForStudentFine = new ArrayList<>();
			creditLedgerListForStudentFine= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Late Fees",csSchoolIdForStudent,schoolId);
			if(CollectionUtils.isNotEmpty(creditLedgerListForStudentFine) &&  creditLedgerListForStudentFine.get(0) !=null) {
				Object[] obj = (Object[]) creditLedgerListForStudentFine.get(0);
				creditLedgerForStudentFine =(Integer) obj[0];
			}
			
			Integer creditLedgerForSubHeadFees=null;
			List creditLedgerListForSubHeadFees = new ArrayList<>();
			creditLedgerListForSubHeadFees= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("School Fees",csSchoolIdForSubhead,schoolId);
			if(CollectionUtils.isNotEmpty(creditLedgerListForSubHeadFees) &&  creditLedgerListForSubHeadFees.get(0) !=null) {
				Object[] obj = (Object[]) creditLedgerListForSubHeadFees.get(0);
				creditLedgerForSubHeadFees =(Integer) obj[0];
			}
			
			Integer creditLedgerForSubHeadFine=null;
			List creditLedgerListForSubHeadFine = new ArrayList<>();
			creditLedgerListForSubHeadFine= feesLinkToAccountRepositoryImpl.getLedgerIdFromLedgerName("Late Fees",csSchoolIdForSubhead,schoolId);
			if(CollectionUtils.isNotEmpty(creditLedgerListForSubHeadFine) &&  creditLedgerListForSubHeadFine.get(0) !=null) {
				Object[] obj = (Object[]) creditLedgerListForSubHeadFine.get(0);
				creditLedgerForSubHeadFine =(Integer) obj[0];
			}
			
			String updatedBy="1";
			ModelMap modelmap = new ModelMap();
			Long journalId=Long.parseLong("0");
			String entryDate=new SimpleDateFormat("dd-MM-yyyy").format(new Date()); 
		 
			int i =0;
			String paymentDetails="";
			StringBuffer voucherNo = new StringBuffer();
			StringBuffer narration = new StringBuffer();
			StringBuffer voucherType = new StringBuffer(); 
		 
			// Create Class ledger
			List list1= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_standard_mapp","standardId",assignFeeBean.getStandardId().toString(),csSchoolIdForClass,schoolId);
			
			AppUserRoleModel appUserRoleModel=new AppUserRoleModel();
			SchoolMasterModel schoolMasterModel=new SchoolMasterModel();
			schoolMasterModel.setSchoolid(assignFeeBean.getSchoolId());
			appUserRoleModel.setAppUserRoleId(assignFeeBean.getUserId());
			if(CollectionUtils.isEmpty(list1)) {
				Integer ledgerId=0;
				AccCreateLedger accCreateLedger1= new AccCreateLedger();
				AccCreateSchoolCollege accCreateSchoolCollege1 = new AccCreateSchoolCollege();
				accCreateSchoolCollege1.setCschoolId(Integer.parseInt(csSchoolIdForClass));
				accCreateLedger1.setAccCreateSchoolCollege(accCreateSchoolCollege1);
			   	AccCreateLedgerHead AccCreateLedgerHead1 = new AccCreateLedgerHead();
			   	ledgerHeadId = feesLinkToAccountRepositoryImpl.getLedgerHeadId("Class Wise Fees Receivable", csSchoolIdForClass,schoolId);
			   	AccCreateLedgerHead1.setLedgerHeadID(ledgerHeadId);
			   	accCreateLedger1.setAccCreateLedgerHead(AccCreateLedgerHead1);
			    accCreateLedger1.setLedgerName(assignFeeBean.getStandardName());
			    accCreateLedger1.setAccCreateSchoolCollege(accCreateSchoolCollege1);
			    accCreateLedger1.setAmountType("0");
			    accCreateLedger1.setSeventyThirty("0");
			    accCreateLedger1.setIsDel("0");
			    accCreateLedger1.setUserId(appUserRoleModel);
			    accCreateLedger1.setSchoolMasterModel(schoolMasterModel);
			    /*accCreateLedger1.setIpAddress(sessionUserBean.getIpAddress());
			    accCreateLedger1.setMacAddress(sessionUserBean.getMacAddress());
			    accCreateLedger1.setDeviceType(sessionUserBean.getDeviceType());*/
			    accCreateLedger1.setUpdatedBy("1");
			   	AccAddGroupAndSubgroup addGroupAndSubgroup1 = new AccAddGroupAndSubgroup();
			   	groupSubGroupId = feesLinkToAccountRepositoryImpl.getGroupSubGroup("Current Assets", csSchoolIdForClass,schoolId);
			   	addGroupAndSubgroup1.setGroupSubgroupId(groupSubGroupId);
			    accCreateLedger1.setAddGroupAndSubgroup(addGroupAndSubgroup1);
			    accCreateLedgerRepository.save(accCreateLedger1);
			    
				AccCreateLedgerStandardMapp accCreateLedgerStandardMapp = new AccCreateLedgerStandardMapp();
				accCreateLedgerStandardMapp.setAccCreateLedger(accCreateLedger1);
				StandardMasterModel standardMasterModel=new StandardMasterModel();
				standardMasterModel.setStandardId(assignFeeBean.getStandardId());
				accCreateLedgerStandardMapp.setStandardMasterModel(standardMasterModel);
				accCreateLedgerStandardMapp.setIsDel("0");
				accCreateLedgerStandardMapp.setAppUserRoleModel(appUserRoleModel);
				accCreateLedgerStandardMapp.setSchoolMasterModel(schoolMasterModel);
				accCreateLedgerStandardMapp.setAppUserRoleModel(appUserRoleModel);
				accCreateLedgerStandardMappRepository.save(accCreateLedgerStandardMapp);

				AccDepreciation acc_depreciation=new AccDepreciation();
				acc_depreciation.setCreateLedger(accCreateLedger1);
				acc_depreciation.setPercent(0.0f);
				acc_depreciation.setIsDel("0");
				acc_depreciation.setUserId(appUserRoleModel);
				acc_depreciation.setSchoolMasterModel(schoolMasterModel);
				accDepreciationRepository.save(acc_depreciation);
			 	debitLedgerForClass=accCreateLedger1.getLedgerId();
			 	
			}
			else {
				Object[] obj = (Object[]) list1.get(0);
				debitLedgerForClass = Integer.parseInt(obj[0].toString());
			}
			
			String oldEntries="1";
			for(int k=0;k<assignFeeBean.getStudentId().size();k++){
				String studName=convertInUTFFormat(assignFeeBean.getStudName().get(k));
				String regNo=convertInUTFFormat(assignFeeBean.getStudRegNo().get(k));
				//studName=convertInUTFFormat(studName);
				//assignFeeBean.getStudRegNo().get(k)=convertInUTFFormat(assignFeeBean.getStudRegNo().get(k));
				// Class Wise Journal Entry
				String feesCsSchoolId=csSchoolIdForClass;
			 	i = feesLinkToAccountRepositoryImpl.getMaxJournalId(schoolId);
			 	voucherType = new StringBuffer(); 
			 	voucherNo = new StringBuffer();
				voucherNo.append("Jo").append("-").append(String.valueOf(i+1));
				voucherType.append(""+voucherTypeIdForClass).append("_").append(voucherNo.toString());
			 	
				
				narration = new StringBuffer();
				narration.append(assignFeeBean.getYearName()).append("-").append(assignFeeBean.getStandardName()).append("-").append(regNo).append("-")
			     		 .append(studName).append("-").append("ClassWise-Fees");
				addJournalEntry(modelmap,journalId,entryDate,voucherType.toString(),debitLedgerForClass,creditLedgerForClassFees,
						  narration.toString(),paymentDetails,voucherNo.toString(),Double.parseDouble(assignFeeBean.getTotalAssignFee()),"0",assignFeeBean.getStudRenewId().get(k).toString(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
			
				
				if(Integer.parseInt(assignFeeBean.getTotalAssignFine())>0) {
					voucherNo = new StringBuffer();
					voucherNo.append("Jo").append("-").append(String.valueOf(i+2));
					voucherType = new StringBuffer(); 
					voucherType.append(""+voucherTypeIdForClass).append("_").append(voucherNo.toString());
					
					narration = new StringBuffer();
					narration.append(assignFeeBean.getYearName()).append("-").append(assignFeeBean.getStandardName()).append("-").append(regNo).append("-")
			        		 .append(studName).append("-").append("ClassWise-Fine");
					addJournalEntry(modelmap,journalId,entryDate,voucherType.toString(),debitLedgerForClass,creditLedgerForClassFine,
							  narration.toString(),paymentDetails,voucherNo.toString(),Double.parseDouble(assignFeeBean.getTotalAssignFine()),"0",assignFeeBean.getStudRenewId().get(k).toString(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
				}
			
				// Student Wise Ledger Entry
				 feesCsSchoolId=csSchoolIdForStudent;
				List list2= feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_student_mapp","studentId",assignFeeBean.getStudentId().get(k).toString(),csSchoolIdForStudent,schoolId);
				if(CollectionUtils.isEmpty(list2)) {
					AccCreateLedger accCreateLedger= new AccCreateLedger();
					int getMaxLegerId=feesLinkToAccountRepositoryImpl.getMaxLegerId(schoolId);
					accCreateLedger.setLedgerId(getMaxLegerId);
					AccCreateSchoolCollege accCreateSchoolCollege = new AccCreateSchoolCollege();
					accCreateSchoolCollege.setCschoolId(Integer.parseInt(csSchoolIdForStudent));
					accCreateLedger.setAccCreateSchoolCollege(accCreateSchoolCollege);
				   	AccCreateLedgerHead AccCreateLedgerHead = new AccCreateLedgerHead();
				 	ledgerHeadId = feesLinkToAccountRepositoryImpl.getLedgerHeadId("Fees Receivable", csSchoolIdForStudent,schoolId);
				   	AccCreateLedgerHead.setLedgerHeadID(ledgerHeadId);
				   	accCreateLedger.setAccCreateLedgerHead(AccCreateLedgerHead);
				   	accCreateLedger.setIsDel("0");
				   	accCreateLedger.setUserId(appUserRoleModel);
				   	accCreateLedger.setSchoolMasterModel(schoolMasterModel);
				/*   	accCreateLedger.setIpAddress(sessionUserBean.getIpAddress());
				   	accCreateLedger.setMacAddress(sessionUserBean.getMacAddress());
				   	accCreateLedger.setDeviceType(sessionUserBean.getDeviceType());*/
				    accCreateLedger.setUpdatedBy("1");
				    String studentLedgerName = studName;
					if(studName.contains(".")) {
						studName= studName.split("\\.")[1];
					}
					
				    if (studName.charAt(0) == ' ') {
				    	studName = studName.replaceFirst(" ", "");
					}
				    
				    if(studName.contains(" ")) {
				    	String nameArr[] = studName.split(" ");
					    int size = nameArr.length;
					    if(size>2) {
					    	studName = nameArr[0].concat(" ").concat(nameArr[1]);
					    }
				    }
				
				   	accCreateLedger.setLedgerName(regNo.concat("-").concat(studentLedgerName));
				   	accCreateLedger.setAccCreateSchoolCollege(accCreateSchoolCollege);
				   	accCreateLedger.setAmountType("0");
				   	accCreateLedger.setSeventyThirty("0");
				   	AccAddGroupAndSubgroup addGroupAndSubgroup = new AccAddGroupAndSubgroup();
					groupSubGroupId = feesLinkToAccountRepositoryImpl.getGroupSubGroup("Current Assets", csSchoolIdForStudent,schoolId);
				   	addGroupAndSubgroup.setGroupSubgroupId(groupSubGroupId);
					accCreateLedger.setAddGroupAndSubgroup(addGroupAndSubgroup);
					accCreateLedgerRepository.save(accCreateLedger);

				 	AccCreateLedgerStudentMapp accCreateLedgerStudentMapp = new AccCreateLedgerStudentMapp();
				 	int getMaxLegerStudMappId=feesLinkToAccountRepositoryImpl.getMaxLegerStudMappId(schoolId);
				 	accCreateLedgerStudentMapp.setLedgerStudentMappId(getMaxLegerStudMappId);
				 	accCreateLedgerStudentMapp.setAccCreateLedger(accCreateLedger);
				 	StudentMasterModel studentMaster = new StudentMasterModel();
					studentMaster.setStudentId(assignFeeBean.getStudentId().get(k));
					accCreateLedgerStudentMapp.setStudentMasterModel(studentMaster );
					accCreateLedgerStudentMapp.setIsDel("0");
					accCreateLedgerStudentMapp.setAppUserRoleModel(appUserRoleModel);
					accCreateLedgerStudentMapp.setSchoolMasterModel(schoolMasterModel);
					accCreateLedgerStudentMappRepository.save(accCreateLedgerStudentMapp);
					
					AccDepreciation acc_depreciation=new AccDepreciation();
					int getMaxAccDeprecationId=feesLinkToAccountRepositoryImpl.getMaxAccDeprecationId(schoolId);
					acc_depreciation.setDepreId(getMaxAccDeprecationId);
					acc_depreciation.setCreateLedger(accCreateLedger);
					acc_depreciation.setPercent(0.0f);
					acc_depreciation.setIsDel("0");
					acc_depreciation.setUserId(appUserRoleModel);
					acc_depreciation.setSchoolMasterModel(schoolMasterModel);
					accDepreciationRepository.save(acc_depreciation);
				}
			
				// Student Wise Journal Entry
			 	i = feesLinkToAccountRepositoryImpl.getMaxJournalId(schoolId);
				voucherNo = new StringBuffer();
				voucherNo.append("Jo").append("-").append(String.valueOf(i+1));
				voucherType = new StringBuffer(); 
				voucherType.append(""+voucherTypeIdForStudent).append("_").append(voucherNo.toString());
				List debitLedgerList=feesLinkToAccountRepositoryImpl.getLedgerId("acc_create_ledger_to_student_mapp","studentId",assignFeeBean.getStudentId().get(k).toString(),csSchoolIdForStudent,schoolId);
			 	if(CollectionUtils.isNotEmpty(debitLedgerList) && debitLedgerList.get(0) !=null) {
					Object[] obj = (Object[]) debitLedgerList.get(0);
					debitLedgerForStudent = (Integer) obj[0];
				}
			 	narration = new StringBuffer();
			 	narration.append(assignFeeBean.getYearName()).append("-").append(assignFeeBean.getStandardName()).append("-").append(regNo).append("-")
					     .append(studName).append("-").append("StudentWise-Fees");
			 	addJournalEntry(modelmap,journalId,entryDate,voucherType.toString(),debitLedgerForStudent,creditLedgerForStudentFees,
						  narration.toString(),paymentDetails,voucherNo.toString(),Double.parseDouble(assignFeeBean.getTotalAssignFee()),"0",assignFeeBean.getStudRenewId().get(k).toString(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
			
				if(Integer.parseInt(assignFeeBean.getTotalAssignFine())>0) {
					voucherNo = new StringBuffer();
					voucherNo.append("Jo").append("-").append(String.valueOf(i+2));
					voucherType = new StringBuffer(); 
					voucherType.append(""+voucherTypeIdForStudent).append("_").append(voucherNo.toString());
					narration = new StringBuffer();
					narration.append(assignFeeBean.getYearName()).append("-").append(assignFeeBean.getStandardName()).append("-").append(regNo).append("-")
					         .append(studName).append("-").append("StudentWise-Fine");
					addJournalEntry(modelmap,journalId,entryDate,voucherType.toString(),debitLedgerForStudent,creditLedgerForStudentFine,
							  narration.toString(),paymentDetails,voucherNo.toString(),Double.parseDouble(assignFeeBean.getTotalAssignFine()),"0",assignFeeBean.getStudRenewId().get(k).toString(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
				}

				// SubHead Wise Journal Entry - Commented temporarily - code in use dont delete
				 feesCsSchoolId=csSchoolIdForSubhead;
					
				 if(CollectionUtils.isNotEmpty(assignFeeBean.getHeadId())  &&assignFeeBean.getHeadId().size()>0) {
					 for(int j=0;j<assignFeeBean.getHeadId().size();j++) {
						
						for(int l=0;l<assignFeeBean.getHeadId().get(j).getSubHeadId().size();l++) {
							if(assignFeeBean.getHeadId().get(j).getAssignFee().get(l)!=null && assignFeeBean.getHeadId().get(j).getAssignFee().get(l)>0) {
								i = feesLinkToAccountRepositoryImpl.getMaxJournalId(schoolId);
								voucherNo = new StringBuffer();
								voucherNo.append("Jo").append("-").append(String.valueOf(i+1));
								voucherType = new StringBuffer(); 
								voucherType.append(""+voucherTypeIdForSubHead).append("_").append(voucherNo.toString());
								List list = feesLinkToAccountRepositoryImpl.getHeadAndSubHeadDetails(""+assignFeeBean.getHeadId().get(j).getSubHeadId().get(l),schoolId);
								String headName = "";
								String subHeadName ="";
								if(CollectionUtils.isNotEmpty(list) && list.get(0) !=null) {
									Object[] obj = (Object[]) list.get(0);
									headName = (String) obj[2];
									subHeadName = (String) obj[0];
								}
								
								debitLedgerForSubHead=feesLinkToAccountRepositoryImpl.getLedgerIdBySubHeadId(""+assignFeeBean.getHeadId().get(j).getSubHeadId().get(l),schoolId);
								narration = new StringBuffer();
								narration.append(assignFeeBean.getYearName()).append("-").append(assignFeeBean.getStandardName()).append("-").append(regNo).append("-")
										 .append(studName).append("-").append(headName).append("-")
										 .append(subHeadName).append("-").append("SubHeadWise-Fee");
								addJournalEntry(modelmap,journalId,entryDate,voucherType.toString(),debitLedgerForSubHead,creditLedgerForSubHeadFees,
										  narration.toString(),paymentDetails,voucherNo.toString(),assignFeeBean.getHeadId().get(j).getAssignFee().get(l),"0",assignFeeBean.getStudRenewId().get(k).toString(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
							
							}
							if(assignFeeBean.getHeadId().get(j).getFine().get(l)!=null && assignFeeBean.getHeadId().get(j).getFine().get(l)>0) {
								voucherNo = new StringBuffer();
								voucherNo.append("Jo").append("-").append(String.valueOf(i+2));
								voucherType = new StringBuffer(); 
								voucherType.append(""+voucherTypeIdForSubHead).append("_").append(voucherNo.toString());
								narration = new StringBuffer();
								String headName = "";
								String subHeadName ="";
								List list = feesLinkToAccountRepositoryImpl.getHeadAndSubHeadDetails(""+assignFeeBean.getHeadId().get(j).getSubHeadId().get(l),schoolId);
								if(CollectionUtils.isNotEmpty(list) && list.get(0) !=null) {
									Object[] obj = (Object[]) list.get(0);
									headName = (String) obj[2];
									subHeadName = (String) obj[0];
								}
							
								narration.append(assignFeeBean.getYearName()).append("-").append(assignFeeBean.getStandardName()).append("-").append(regNo).append("-")
						  		 						  .append(studName).append("-").append(headName).append("-")
						  		 						  .append(subHeadName).append("-").append("SubHeadWise-Fine");
								addJournalEntry(modelmap,journalId,entryDate,voucherType.toString(),debitLedgerForSubHead,creditLedgerForSubHeadFine,
										  narration.toString(),paymentDetails,voucherNo.toString(),assignFeeBean.getHeadId().get(j).getFine().get(l),"0",assignFeeBean.getStudRenewId().get(k).toString(),schoolId,updatedBy,feesCsSchoolId,oldEntries);
							}
						}
				 	}
				}
			
			}
			return new ArrayList<>();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void addJournalEntry(ModelMap modelmap, Long journalId, String entryDate, String vType,
			Integer debitLedger, Integer creditLedger, String narration, String pDetails, String vNo,
			Double amount, String checkBoxKeep, String studentId,Integer schoolId, String updatedBy, String feesCsSchoolId, String oldEntries) {
		try {
			journalId=feesLinkToAccountRepositoryImpl.getMaxjourNalId(schoolId);
			entryDate=convertInUTFFormat(entryDate);
			vType=convertInUTFFormat(vType);
			pDetails=convertInUTFFormat(pDetails);
			vNo=convertInUTFFormat(vNo);
			checkBoxKeep=convertInUTFFormat(checkBoxKeep);
			studentId=convertInUTFFormat(studentId);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
			Date date = new Date();
			String currentDate= new SimpleDateFormat("dd-mm-yyyy").format(date);

			/*
			 * AppUserRole appUserRole = new AppUserRole(); int userId =
			 * Integer.parseInt(""+session.getAttribute("sessionUserId"));
			 * appUserRole.setAppuserroleid(userId);
			 */
			SchoolMasterModel schoolMaster = new SchoolMasterModel();
			schoolMaster.setSchoolid(schoolId);
			String refundId="";
			if(narration.contains("Fees Refund")) {
				String narration1[]=narration.split("@");
				narration=narration1[0];
				refundId=narration1[1];
			}
			
			
			//String ipAddress = ""+session.getAttribute("sessionIPAddress");
			//String macAddress = ""+session.getAttribute("sessionMacAddress");
			//String deviceType = ""+session.getAttribute("sessionDeviceType");
			
			String cschoolId="";
			if(("1").equals(updatedBy)) {
				cschoolId =""+feesCsSchoolId;
			}
			else if(("3").equals(updatedBy)) {
				cschoolId =""+feesCsSchoolId;
			}
			else {
				narration=convertInUTFFormat(narration);
				//cschoolId=""+session.getAttribute("cschoolId");
			}
			//String isScholoarship[]=narration.split("-");

			SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
			Calendar now2 = Calendar.getInstance();

			String cdate=""+dateFormat2.format(now2.getTime());

			SimpleDateFormat dateFormat3 = new SimpleDateFormat("hh:mm");
			Calendar now3 = Calendar.getInstance();

			String ctime=""+dateFormat3.format(now3.getTime());
			Integer vt=Integer.parseInt(""+vType.substring(0,vType.indexOf("_")));
			AccJournalDayBookEntry accJournalDayBookEntry=new AccJournalDayBookEntry();
				if(journalId!=0){
					accJournalDayBookEntry.setJournalId(journalId);
				}

				AccCreateSchoolCollege accCreateSchoolCollege=new AccCreateSchoolCollege();
				accCreateSchoolCollege.setCschoolId(Integer.parseInt(""+cschoolId));

				AccAddVoucherType accAddVoucherType=new AccAddVoucherType();
				accAddVoucherType.setVtypeId(vt);

				AccCreateLedger createLedgerByDebitLedger=new AccCreateLedger();
				createLedgerByDebitLedger.setLedgerId(debitLedger);

				AccCreateLedger createLedgerByCreditLedger=new AccCreateLedger();
				createLedgerByCreditLedger.setLedgerId(creditLedger);

				accJournalDayBookEntry.setAccCreateSchoolCollege(accCreateSchoolCollege);
				accJournalDayBookEntry.setCdate(cdate);
				accJournalDayBookEntry.setCtime(ctime);
				accJournalDayBookEntry.setEntryDate(entryDate);
				accJournalDayBookEntry.setAccAddVoucherType(accAddVoucherType);
				accJournalDayBookEntry.setInvNo(vNo);
				accJournalDayBookEntry.setNarration(narration);
				accJournalDayBookEntry.setPaymentDetails(pDetails);
				accJournalDayBookEntry.setCreateLedgerByDebitLedger(createLedgerByDebitLedger);
				accJournalDayBookEntry.setCreateLedgerByCreditLedger(createLedgerByCreditLedger);
				
				if(narration.contains("Fees Refund")) {
					accJournalDayBookEntry.setCrAmount(amount);
				}else {
					accJournalDayBookEntry.setDrAmount(amount);
				}
				
				//accJournalDayBookEntry.setUserId(appUserRole);
				accJournalDayBookEntry.setSchoolMasterModel(schoolMaster);
				//accJournalDayBookEntry.setIpAddress(ipAddress);
				//accJournalDayBookEntry.setMacAddress(macAddress);
				//accJournalDayBookEntry.setDeviceType(deviceType);
				if(narration.contains("Scholarship")) {
					accJournalDayBookEntry.setScholarshipFlag("1");
				}
				
//				if(sdf.parse(entryDate).before(sdf.parse(currentDate))) {
//					accJournalDayBookEntry.setBdFlag("1");
//				}
				
				if(!studentId.equals("0")) {
					accJournalDayBookEntry.setStudentId(Integer.parseInt(studentId));
				}

//			List lastClosingAmountDr=journalDayBookEntryService.getlastClosingAmountByLagerIdDr(debitLedger, cschoolId,amount);
//			journalDayBookEntry.setLastClosingAmount(Double.parseDouble(""+lastClosingAmountDr.get(0)));
//			journalDayBookEntry.setAmountType(""+lastClosingAmountDr.get(1));
				accJournalDayBookEntry.setEntryOf("0");
				accJournalDayBookEntry.setIsDel("0");
				Boolean isSaveSuccessFul1= false;
				if(updatedBy=="1" && oldEntries=="1") { //
					accJournalDayBookEntry.setUpdatedBy(1);
					feesLinkToAccountRepository.save(accJournalDayBookEntry);  // used
				}
				else if(updatedBy=="1") { //
					accJournalDayBookEntry.setUpdatedBy(1);
					//feesLinkToAccountRepository.save(accJournalDayBookEntry);  // used
				}
				else if(updatedBy=="3") {
					accJournalDayBookEntry.setUpdatedBy(3);
					// accJournalDayBookEntryService.addEditDelete(accJournalDayBookEntry);  // used
				}
				else {
					feesLinkToAccountRepository.save(accJournalDayBookEntry);  // used
				}
				
				if (narration.contains("Fees Refund")) {
					/*
					 * String refundIds[]=refundId.split(",");
					 * 
					 * FeeRefundMappToAccJournalModel feeRefundMappToAccJournalModel=new
					 * FeeRefundMappToAccJournalModel();
					 * 
					 * for(int y=0;y<refundIds.length;y++) {
					 * feeRefundMappToAccJournalModel.setAccJournalDayBookEntry(
					 * accJournalDayBookEntry);
					 * feeRefundMappToAccJournalModel.setUserId(appUserRole);
					 * feeRefundMappToAccJournalModel.setSchoolMaster(schoolMaster);
					 * feeRefundMappToAccJournalModel.setIpAddress(ipAddress);
					 * feeRefundMappToAccJournalModel.setMacAddress(macAddress);
					 * feeRefundMappToAccJournalModel.setDeviceType(deviceType); FeeRefundtable
					 * feeRefundtable=new FeeRefundtable();
					 * feeRefundtable.setRefundId(Integer.parseInt(refundIds[y]));
					 * feeRefundMappToAccJournalModel.setFeeRefundtable(feeRefundtable);
					 * 
					 * accJournalDayBookEntryService.saveRefundMappData(
					 * feeRefundMappToAccJournalModel); }
					 */}
				
				AccJournalDayBookEntry journalDayBookEntry2=new AccJournalDayBookEntry();

				if(journalId!=0){
					journalId=journalId+1;
					journalDayBookEntry2.setJournalId(journalId);
				}

				journalDayBookEntry2.setAccCreateSchoolCollege(accCreateSchoolCollege);
				journalDayBookEntry2.setCdate(cdate);
				journalDayBookEntry2.setCtime(ctime);
				journalDayBookEntry2.setEntryDate(entryDate);
				journalDayBookEntry2.setAccAddVoucherType(accAddVoucherType);
				journalDayBookEntry2.setInvNo(vNo);
				journalDayBookEntry2.setNarration(narration);
				journalDayBookEntry2.setPaymentDetails(pDetails);
				
				
				journalDayBookEntry2.setCreateLedgerByDebitLedger(createLedgerByCreditLedger);
				journalDayBookEntry2.setCreateLedgerByCreditLedger(createLedgerByDebitLedger);
				if(narration.contains("Fees Refund")) {
					journalDayBookEntry2.setDrAmount(amount);
				}else {
					journalDayBookEntry2.setCrAmount(amount);
				}
				
//				if(sdf.parse(entryDate).before(sdf.parse(currentDate))) {
//					journalDayBookEntry2.setBdFlag("1");
//				}
				if(narration.contains("Scholarship")) {
					journalDayBookEntry2.setScholarshipFlag("1");
				}
				if(!studentId.equals("0")) {
					journalDayBookEntry2.setStudentId(Integer.parseInt(studentId));
				}
//			List lastClosingAmountCr=journalDayBookEntryService.getlastClosingAmountByLagerIdCr(creditLedger, cschoolId,amount);
//			journalDayBookEntry2.setLastClosingAmount(Double.parseDouble(""+lastClosingAmountCr.get(0)));
//			journalDayBookEntry2.setAmountType(""+lastClosingAmountCr.get(1));
				journalDayBookEntry2.setEntryOf("1");
				
				//journalDayBookEntry2.setUserId(appUserRole);
				journalDayBookEntry2.setSchoolMasterModel(schoolMaster);
				//journalDayBookEntry2.setIpAddress(ipAddress);
				//journalDayBookEntry2.setMacAddress(macAddress);
				//journalDayBookEntry2.setDeviceType(deviceType);

				Boolean isSaveSuccessFul2 = false;
				if(updatedBy=="1" && updatedBy=="1") { //
					journalDayBookEntry2.setUpdatedBy(1);
					 feesLinkToAccountRepository.save(journalDayBookEntry2);  // used
				}
				else if(updatedBy=="1") { //
					journalDayBookEntry2.setUpdatedBy(1);
					//isSaveSuccessFul2 = accJournalDayBookEntryService.addEditDelete(journalDayBookEntry2);  // used
				}	
				else if(updatedBy=="3") {
					journalDayBookEntry2.setUpdatedBy(3);
					//isSaveSuccessFul2 = accJournalDayBookEntryService.addEditDelete(journalDayBookEntry2);  // used
				}
				else {
					feesLinkToAccountRepository.save(journalDayBookEntry2);  // used
				}
			
				if (narration.contains("Fees Refund")) {
					/*
					 * String refundIds[]=refundId.split(",");
					 * 
					 * FeeRefundMappToAccJournalModel feeRefundMappToAccJournalModel=new
					 * FeeRefundMappToAccJournalModel();
					 * 
					 * for(int y=0;y<refundIds.length;y++) {
					 * feeRefundMappToAccJournalModel.setAccJournalDayBookEntry(journalDayBookEntry2
					 * ); feeRefundMappToAccJournalModel.setUserId(appUserRole);
					 * feeRefundMappToAccJournalModel.setSchoolMaster(schoolMaster);
					 * feeRefundMappToAccJournalModel.setIpAddress(ipAddress);
					 * feeRefundMappToAccJournalModel.setMacAddress(macAddress);
					 * feeRefundMappToAccJournalModel.setDeviceType(deviceType); FeeRefundtable
					 * feeRefundtable=new FeeRefundtable();
					 * feeRefundtable.setRefundId(Integer.parseInt(refundIds[y]));
					 * feeRefundMappToAccJournalModel.setFeeRefundtable(feeRefundtable);
					 * 
					 * accJournalDayBookEntryService.saveRefundMappData(
					 * feeRefundMappToAccJournalModel); }
					 */}
				
				if(isSaveSuccessFul1 && isSaveSuccessFul2) {
					//session.setAttribute("AccountLinkSuccessFul", "Success");
				}

				String vty=""+vType.substring(vType.indexOf("_")+1,vType.indexOf("-"));
				Integer voucherSeries=Integer.parseInt(""+vType.substring(vType.indexOf("-")+1,vType.length()));
				if(checkBoxKeep.equals("0")){
					voucherSeries++;
				}
				feesLinkToAccountRepositoryImpl.editVoucherSeries(vt,vty+"-"+voucherSeries,schoolId);  // used

				//session.setAttribute("journalId", journalDayBookEntry.getJournalId());
				//session.setAttribute("debitLedger", debitLedger);

				//===================== Start Depreciation ==================================================
				if(feesLinkToAccountRepositoryImpl.isDepreciationLager(""+cschoolId,""+debitLedger,schoolId)!=0){
					SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy");
					Calendar now1 = Calendar.getInstance();
					AccJournalDayBookEntry journalDayBookEntryDep=new AccJournalDayBookEntry();
					if(feesLinkToAccountRepositoryImpl.isExistLedger(""+cschoolId,""+debitLedger,schoolId)==0){

						if(journalId!=0){
							journalId=journalId+1;
							journalDayBookEntryDep.setJournalId(journalId);
						}
						AccCreateSchoolCollege createSchoolCollegeDep=new AccCreateSchoolCollege();
						createSchoolCollegeDep.setCschoolId(Integer.parseInt(""+cschoolId));

						AccAddVoucherType addVoucherTypeDep=new AccAddVoucherType();
						List<AccAddVoucherTypeBean> list1 = feesLinkToAccountRepositoryImpl.getVoucherType(""+cschoolId,"Journal",schoolId);
						for(int i=0;i<list1.size();i++)	{
							AccAddVoucherTypeBean b=list1.get(i);
							vt=b.getVtypeId();
							//vType=b.getVoucherName();
							vNo=""+b.getVoucherSeries();
						}
						addVoucherTypeDep.setVtypeId(vt);

						AccCreateLedger createLedgerByDebitLedgerDep=new AccCreateLedger();
						createLedgerByDebitLedgerDep.setLedgerId(3); //Depreciation ID

						AccCreateLedger createLedgerByCreditLedgerDep=new AccCreateLedger();
						createLedgerByCreditLedgerDep.setLedgerId(debitLedger);

						journalDayBookEntryDep.setAccCreateSchoolCollege(createSchoolCollegeDep);
						journalDayBookEntryDep.setCdate(cdate);
						journalDayBookEntryDep.setCtime(ctime);
						int ddd=Integer.parseInt(""+dateFormat1.format(now1.getTime()));
						ddd++;
						journalDayBookEntryDep.setEntryDate("31-03-"+ddd);
						journalDayBookEntryDep.setAccAddVoucherType(addVoucherTypeDep);
						journalDayBookEntryDep.setInvNo(vNo);
						journalDayBookEntryDep.setNarration("Depreciation Charge for the year");
						journalDayBookEntryDep.setPaymentDetails("Depreciation");
						journalDayBookEntryDep.setCreateLedgerByDebitLedger(createLedgerByDebitLedgerDep);
						journalDayBookEntryDep.setCreateLedgerByCreditLedger(createLedgerByCreditLedgerDep);
						//amount=journalDayBookEntryService.getDepreciationAnnaxure(""+cschoolId,""+debitLedger);
						journalDayBookEntryDep.setDrAmount(amount);

			//			List lastClosingAmountDr=journalDayBookEntryDepService.getlastClosingAmountByLagerIdDr(debitLedger, cschoolId,amount);
//					journalDayBookEntryDep.setLastClosingAmount(0.0);
//					journalDayBookEntryDep.setAmountType("0");
						journalDayBookEntryDep.setEntryOf("0");
						
						//journalDayBookEntryDep.setUserId(appUserRole);
						journalDayBookEntryDep.setSchoolMasterModel(schoolMaster);
						//journalDayBookEntryDep.setIpAddress(ipAddress);
						//journalDayBookEntryDep.setMacAddress(macAddress);
						//journalDayBookEntryDep.setDeviceType(deviceType);

						if(updatedBy=="1") { //
							feesLinkToAccountRepository.save(journalDayBookEntryDep);  // used
						}	
						else {
							feesLinkToAccountRepository.save(journalDayBookEntryDep);  // used
						}

						AccJournalDayBookEntry journalDayBookEntryDep2=new AccJournalDayBookEntry();

						if(journalId!=0){
							journalId=journalId+1;
							journalDayBookEntryDep2.setJournalId(journalId);
						}

						journalDayBookEntryDep2.setAccCreateSchoolCollege(createSchoolCollegeDep);
						journalDayBookEntryDep2.setCdate(cdate);
						journalDayBookEntryDep2.setCtime(ctime);
						journalDayBookEntryDep2.setEntryDate("31-03-"+ddd);
						journalDayBookEntryDep2.setAccAddVoucherType(addVoucherTypeDep);
						journalDayBookEntryDep2.setInvNo(vNo);
						journalDayBookEntryDep2.setNarration("Depreciation Charge for the year");
						journalDayBookEntryDep2.setPaymentDetails("Depreciation");
						journalDayBookEntryDep2.setCreateLedgerByDebitLedger(createLedgerByCreditLedgerDep);
						journalDayBookEntryDep2.setCreateLedgerByCreditLedger(createLedgerByDebitLedgerDep);
						journalDayBookEntryDep2.setCrAmount(amount);
			//			List lastClosingAmountCr=journalDayBookEntryDepService.getlastClosingAmountByLagerIdCr(creditLedger, cschoolId,amount);
			//			journalDayBookEntryDep2.setLastClosingAmount(Double.parseDouble(""+lastClosingAmountCr.get(0)));
			//			journalDayBookEntryDep2.setAmountType(""+lastClosingAmountCr.get(1));
						journalDayBookEntryDep2.setEntryOf("1");
						
						//journalDayBookEntryDep.setUserId(appUserRole);
						journalDayBookEntryDep2.setSchoolMasterModel(schoolMaster);
						//journalDayBookEntryDep2.setIpAddress(ipAddress);
						//journalDayBookEntryDep2.setMacAddress(macAddress);
						//journalDayBookEntryDep2.setDeviceType(deviceType);

						if(updatedBy=="1") { //
							feesLinkToAccountRepository.save(journalDayBookEntryDep);  // used
						}	
						else {
							feesLinkToAccountRepository.save(journalDayBookEntryDep);  // used
						}

						voucherSeries=Integer.parseInt(""+vNo.substring(vNo.indexOf("-")+1,vNo.length()));
						voucherSeries++;

						feesLinkToAccountRepositoryImpl.editVoucherSeries(vt,"Jo-"+voucherSeries,schoolId);  // used

						//session.setAttribute("journalId", journalDayBookEntryDep.getJournalId());
					}
				}
				//==================== End Depreciation ==================================================

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
