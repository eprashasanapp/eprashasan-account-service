package com.ingenio.account.service;

import java.util.List;

import com.ingenio.account.bean.AssignFeeBean;

public interface FeesLinkToAccountService {

	List<AssignFeeBean> passEntryInAccount(String regNo, Integer yearId, Integer schoolId);

	List<AssignFeeBean> passFeeSettingEntryInAccount(AssignFeeBean assignFeeBean);

}
