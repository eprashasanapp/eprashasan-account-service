package com.ingenio.account.service;

import java.io.File;

import javax.servlet.http.HttpServletResponse;

public interface AccountsService {

	String generateTallyReport(Integer schoolId, Integer csSchoolId, String year, String toDate, Integer type);

	String generateTallyMasters(Integer schoolId, Integer csSchoolId);

	String uploadXMLToAWS(String fileFolderPath, String fileName, HttpServletResponse response, Integer schoolId,
			Integer csSchoolId, File file, Integer type);

}
