package com.ingenio.account.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ingenio.account.bean.ResponseBodyBean;
import com.ingenio.account.service.FeesLinkToAccountService;
import com.ingenio.account.bean.AssignFeeBean;


@RestController
public class FeesToLinkAccountController {
	@Autowired
	FeesLinkToAccountService feesLinkToAccountService;
	
//	@PostMapping(value="/passEntryInAccount" )
//	@ResponseBody
//	public boolean passEntryInAccount(@RequestParam("regNo") String regNo,@RequestParam("yearId") Integer yearId,@RequestParam("schoolId") Integer schoolId){
//		try {
//			feesLinkToAccountService.passEntryInAccount(regNo,yearId,schoolId);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
	
	@PostMapping(value="/passEntryInAccount", consumes = "application/json")
	public ResponseBodyBean<AssignFeeBean> passEntryInAccount(@RequestBody AssignFeeBean assignFeeBean,HttpSession session){
		try {
			List<AssignFeeBean> feeBean=feesLinkToAccountService.passEntryInAccount(assignFeeBean.getStudRegNo().get(0),assignFeeBean.getYearId(),assignFeeBean.getSchoolId());
			if(feeBean!=null) {
				session.setAttribute("feeBeanList", feeBean);
				return new ResponseBodyBean<AssignFeeBean>("200",HttpStatus.OK,feeBean,"Ok");
			}
			else {
				return new ResponseBodyBean<AssignFeeBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>(),"Payment Cant't be done. Please contact Administration");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AssignFeeBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	
	@PostMapping(value="/passFeeSettingEntryInAccount" , consumes = "application/json" )
	public ResponseBodyBean<AssignFeeBean> passFeeSettingEntryInAccount(@RequestBody AssignFeeBean assignFeeBean,HttpSession session){
		try {
			List<AssignFeeBean> feeBean=feesLinkToAccountService.passFeeSettingEntryInAccount(assignFeeBean);
			if(feeBean!=null) {
				session.setAttribute("feeBeanList", feeBean);
				return new ResponseBodyBean<AssignFeeBean>("200",HttpStatus.OK,feeBean,"Ok");
			}
			else {
				return new ResponseBodyBean<AssignFeeBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>(),"Payment Cant't be done. Please contact Administration");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AssignFeeBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
}
