package com.ingenio.account.controller;

import java.io.File;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.account.service.AccountsService;

@RestController
public class AccountsController {
	
	@Autowired
	AccountsService accountService;
	
	@GetMapping(value="/generateTallyReport")
	public String generateTallyReport(@RequestParam("schoolId") Integer schoolId,@RequestParam("csSchoolId") Integer csSchoolId,
			@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,
			@RequestParam("type") Integer type,@RequestParam("ipAddress") String ipAddress,HttpServletResponse response){
		String filePathName =  accountService.generateTallyReport(schoolId,csSchoolId,fromDate,toDate,type);
		if(!"".equals(filePathName)) {
			String fileNameAndPath[] = filePathName.split("#"); 
			String fileFolderPath = fileNameAndPath[0];
			String fileName =fileNameAndPath[1];
			 File file = new File(fileFolderPath+"\\"+fileName+".xml");
			return accountService.uploadXMLToAWS(fileFolderPath,fileName,response,schoolId,csSchoolId,file,type);
		}
		return filePathName;
//		return accountService.generateTallyReport(schoolId,csSchoolId,fromDate,toDate,type);
	}

	@GetMapping(value="/generateTallyMasters")
	public String generateTallyMasters(@RequestParam("schoolId") Integer schoolId,@RequestParam("csSchoolId") Integer csSchoolId,HttpServletResponse response){
		
		String filePathName =  accountService.generateTallyMasters(schoolId,csSchoolId);
		if(!"".equals(filePathName)) {
			String fileNameAndPath[] = filePathName.split("#"); 
			String fileFolderPath = fileNameAndPath[0];
			String fileName =fileNameAndPath[1];
			 File file = new File(fileFolderPath+"\\"+fileName+".xml");
			 Integer type=0;
			return accountService.uploadXMLToAWS(fileFolderPath,fileName,response,schoolId,csSchoolId,file,type);
		}
		return filePathName;
	}

	
	
}
