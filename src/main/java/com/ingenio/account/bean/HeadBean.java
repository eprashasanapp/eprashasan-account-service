package com.ingenio.account.bean;

import java.util.List;

public class HeadBean {

	private Integer headId;
	private List<Integer>subHeadId;
	private List<Double> assignFee;
	private List<Double> fine;
	private List<Integer> fineIncrement;
	private List<String> dueDate;
	public Integer getHeadId() {
		return headId;
	}
	public void setHeadId(Integer headId) {
		this.headId = headId;
	}
	public List<Integer> getSubHeadId() {
		return subHeadId;
	}
	public void setSubHeadId(List<Integer> subHeadId) {
		this.subHeadId = subHeadId;
	}
	public List<Double> getAssignFee() {
		return assignFee;
	}
	public void setAssignFee(List<Double> assignFee) {
		this.assignFee = assignFee;
	}
	public List<Double> getFine() {
		return fine;
	}
	public void setFine(List<Double> fine) {
		this.fine = fine;
	}
	public List<Integer> getFineIncrement() {
		return fineIncrement;
	}
	public void setFineIncrement(List<Integer> fineIncrement) {
		this.fineIncrement = fineIncrement;
	}
	public List<String> getDueDate() {
		return dueDate;
	}
	public void setDueDate(List<String> dueDate) {
		this.dueDate = dueDate;
	}
	
}
