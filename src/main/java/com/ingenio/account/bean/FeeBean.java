package com.ingenio.account.bean;

import java.math.BigInteger;
import java.util.List;

public class FeeBean {
	
	private Integer headId;
	private String paymentGatewayAccountId;
	private String headName;
	private Integer subheadId;
	private String subheadName;
	private Double assignFee;
	private Double assignedFine;
	private Double paidFee;
	private Double dueFee;
	private String paidFine;
	private String clearanceFee;
	private String clearanceFine;
	private String remainingFee;
	private String remainingFine;
	private Integer receiptId;
	private String receiptNo;
	private String paidFeeDate;
	private String receivedBy;
	private String paidBy;
	private String discount;
	private String studentName;
	private String studentRegNo;
	private Integer renewstudentId;
	private Integer standardId;
	private String standardName;
	private String rollNo;
	private String divisionId;
	private BigInteger feeSetMultiID;
	private Integer studFeeID;
	private Double paymentGatewayAmount;
	private String orderId;
	private Integer transactionId;
	private String message;
	private String merchantName;
	private String merchantDescription;
	private Integer payType;
	private Integer userId;
	private Double transanctionAmount;
	private String paymentStatus;
	private String processId;
	private String time;
	private Integer studentId;
	private String YearName;
	private Integer yearId;
	private Integer renewId;
	private List<FeeBean> feeBeanList;
	private String totalPaidFee;
	private String totalRemainingFee; 
	private String transactionId1;
	private Integer schoolId;
	private String scholarshipFlag;
	private String societyFlag;
	private String key;
	private String salt;
	private String mobileNo;
	private String email;
	private String easeBuzzId;
	private String voucherName;
	public FeeBean() {
		super();
	}

	public FeeBean(Integer headId, String headName, Integer subheadId, String subheadName, Double assignFee) {
		super();
		this.headId = headId;
		this.headName = headName;
		this.subheadId = subheadId;
		this.subheadName = subheadName;
		this.assignFee = assignFee;
	}
	
	public FeeBean(Integer headId, String headName, Integer subheadId, String subheadName, Double paidFee,
			Integer receiptId, String receiptNo,String paidFeedate) {
		super();
		this.headId = headId;
		this.headName = headName;
		this.subheadId = subheadId;
		this.subheadName = subheadName;
		this.paidFee = paidFee;
		this.receiptId = receiptId;
		this.receiptNo = receiptNo;
		this.paidFeeDate= paidFeedate;
	}
	
	

	public FeeBean(BigInteger feeSetMultiID, Integer studFeeID, Integer headId, Integer subheadId, 
			Double paidFee,Double discount, Double dueFee, String paidFeeDate,Integer payType,Integer userId) {
		super();
		this.feeSetMultiID = feeSetMultiID;
		this.studFeeID = studFeeID;
		this.headId = headId;
		this.subheadId = subheadId;
		this.paidFee = paidFee;
		this.dueFee = dueFee;
		this.discount=""+discount;
		this.paidFeeDate = paidFeeDate;
		this.payType = payType;
		this.userId=userId;
		
	}
	
	public FeeBean(Double transanctionAmount,String paidFeeDate, String paymentStatus,Integer processId,String orderId) {
		super();
		this.paidFeeDate = paidFeeDate;
		this.transanctionAmount = transanctionAmount;
		this.paymentStatus=paymentStatus;
		this.processId=""+processId;
		this.orderId=orderId;
	}
	
	public FeeBean(Double transanctionAmount,String paidFeeDate, String paymentStatus,Integer processId,String orderId,String time) {
		super();
		this.paidFeeDate = paidFeeDate;
		this.transanctionAmount = transanctionAmount;
		this.paymentStatus=paymentStatus;
		this.processId=""+processId;
		this.orderId=orderId;
		this.time=time;
	}
	
	public FeeBean(String transactionId1 ,Double transanctionAmount,String paidFeeDate, String paymentStatus,
			Integer processId,String orderId,String time,String receiptNo ,Integer receiptId ) {
		super();
		this.transactionId1=transactionId1;
		this.paidFeeDate = paidFeeDate;
		this.transanctionAmount = transanctionAmount;
		this.paymentStatus=paymentStatus;
		this.processId=""+processId;
		this.orderId=orderId;
		this.time=time;
		this.receiptId = receiptId;
		this.receiptNo = receiptNo;
	}


	public String getTransactionId1() {
		return transactionId1;
	}

	public void setTransactionId1(String transactionId1) {
		this.transactionId1 = transactionId1;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	public Integer getSubheadId() {
		return subheadId;
	}

	public void setSubheadId(Integer subheadId) {
		this.subheadId = subheadId;
	}

	public String getSubheadName() {
		return subheadName;
	}

	public void setSubheadName(String subheadName) {
		this.subheadName = subheadName;
	}

	public Double getAssignFee() {
		return assignFee;
	}

	public void setAssignFee(Double assignFee) {
		this.assignFee = assignFee;
	}

	public Double getAssignedFine() {
		return assignedFine;
	}

	public void setAssignedFine(Double assignedFine) {
		this.assignedFine = assignedFine;
	}

	public Double getPaidFee() {
		return paidFee;
	}

	public void setPaidFee(Double paidFee) {
		this.paidFee = paidFee;
	}

	public String getPaidFine() {
		return paidFine;
	}

	public void setPaidFine(String paidFine) {
		this.paidFine = paidFine;
	}

	public String getClearanceFee() {
		return clearanceFee;
	}

	public void setClearanceFee(String clearanceFee) {
		this.clearanceFee = clearanceFee;
	}

	public String getClearanceFine() {
		return clearanceFine;
	}

	public void setClearanceFine(String clearanceFine) {
		this.clearanceFine = clearanceFine;
	}

	public String getRemainingFee() {
		return remainingFee;
	}

	public void setRemainingFee(String remainingFee) {
		this.remainingFee = remainingFee;
	}

	public String getRemainingFine() {
		return remainingFine;
	}

	public void setRemainingFine(String remainingFine) {
		this.remainingFine = remainingFine;
	}

	public Integer getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getPaidFeeDate() {
		return paidFeeDate;
	}

	public void setPaidFeeDate(String paidFeeDate) {
		this.paidFeeDate = paidFeeDate;
	}

	public Double getDueFee() {
		return dueFee;
	}

	public void setDueFee(Double dueFee) {
		this.dueFee = dueFee;
	}

	public String getReceivedBy() {
		return receivedBy;
	}

	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	public String getPaidBy() {
		return paidBy;
	}

	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentRegNo() {
		return studentRegNo;
	}

	public void setStudentRegNo(String studentRegNo) {
		this.studentRegNo = studentRegNo;
	}

	public Integer getRenewstudentId() {
		return renewstudentId;
	}

	public void setRenewstudentId(Integer renewstudentId) {
		this.renewstudentId = renewstudentId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	public BigInteger getFeeSetMultiID() {
		return feeSetMultiID;
	}

	public void setFeeSetMultiID(BigInteger feeSetMultiID) {
		this.feeSetMultiID = feeSetMultiID;
	}

	public Integer getStudFeeID() {
		return studFeeID;
	}

	public void setStudFeeID(Integer studFeeID) {
		this.studFeeID = studFeeID;
	}

	public String getPaymentGatewayAccountId() {
		return paymentGatewayAccountId;
	}

	public void setPaymentGatewayAccountId(String paymentGatewayAccountId) {
		this.paymentGatewayAccountId = paymentGatewayAccountId;
	}

	public Double getPaymentGatewayAmount() {
		return paymentGatewayAmount;
	}

	public void setPaymentGatewayAmount(Double paymentGatewayAmount) {
		this.paymentGatewayAmount = paymentGatewayAmount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantDescription() {
		return merchantDescription;
	}

	public void setMerchantDescription(String merchantDescription) {
		this.merchantDescription = merchantDescription;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getTransanctionAmount() {
		return transanctionAmount;
	}

	public void setTransanctionAmount(Double transanctionAmount) {
		this.transanctionAmount = transanctionAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String  processId) {
		this.processId = processId;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getYearName() {
		return YearName;
	}

	public void setYearName(String yearName) {
		YearName = yearName;
	}

	public Integer getRenewId() {
		return renewId;
	}

	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	
	  public Integer getYearId() {
			return yearId;
		}

		public void setYearId(Integer yearId) {
			this.yearId = yearId;
		}

		public List<FeeBean> getFeeBeanList() {
			return feeBeanList;
		}

		public void setFeeBeanList(List<FeeBean> feeBeanList) {
			this.feeBeanList = feeBeanList;
		}

		public String getTotalPaidFee() {
			return totalPaidFee;
		}

		public void setTotalPaidFee(String totalPaidFee) {
			this.totalPaidFee = totalPaidFee;
		}

		public String getTotalRemainingFee() {
			return totalRemainingFee;
		}

		public void setTotalRemainingFee(String totalRemainingFee) {
			this.totalRemainingFee = totalRemainingFee;
		}

		public Integer getSchoolId() {
			return schoolId;
		}

		public void setSchoolId(Integer schoolId) {
			this.schoolId = schoolId;
		}

		public String getScholarshipFlag() {
			return scholarshipFlag;
		}

		public void setScholarshipFlag(String scholarshipFlag) {
			this.scholarshipFlag = scholarshipFlag;
		}

		public String getSocietyFlag() {
			return societyFlag;
		}

		public void setSocietyFlag(String societyFlag) {
			this.societyFlag = societyFlag;
		}

		public Integer getStudentId() {
			return studentId;
		}

		public void setStudentId(Integer studentId) {
			this.studentId = studentId;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getSalt() {
			return salt;
		}

		public void setSalt(String salt) {
			this.salt = salt;
		}

		public String getMobileNo() {
			return mobileNo;
		}

		public void setMobileNo(String mobileNo) {
			this.mobileNo = mobileNo;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getEaseBuzzId() {
			return easeBuzzId;
		}

		public void setEaseBuzzId(String easeBuzzId) {
			this.easeBuzzId = easeBuzzId;
		}

		public String getVoucherName() {
			return voucherName;
		}

		public void setVoucherName(String voucherName) {
			this.voucherName = voucherName;
		}
		
		
	
}
