package com.ingenio.account.bean;

public class FCMTokenBean {
	
	private String token;
	private String deviceType;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	public FCMTokenBean() {
		super();
	}
	public FCMTokenBean(String token, String deviceType) {
		super();
		this.token = token;
		this.deviceType = deviceType;
	}
	
	

}
