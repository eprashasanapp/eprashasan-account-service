package com.ingenio.account.bean;

public class AccAddVoucherTypeBean {
	private Integer vtypeId;
    private String voucherName;
    private String voucherSeries;
    private String debit;
    private String credit;
    private String isDel;
    
	public Integer getVtypeId() {
		return vtypeId;
	}
	public void setVtypeId(Integer vtypeId) {
		this.vtypeId = vtypeId;
	}
	public String getVoucherName() {
		return voucherName;
	}
	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}
	public String getVoucherSeries() {
		return voucherSeries;
	}
	public void setVoucherSeries(String voucherSeries) {
		this.voucherSeries = voucherSeries;
	}
	public String getDebit() {
		return debit;
	}
	public void setDebit(String debit) {
		this.debit = debit;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}    
}
