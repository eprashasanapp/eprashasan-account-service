package com.ingenio.account.bean;

public class XmlFilePathBean {

	private Integer filePathId;
	private Integer schoolid;
	private Integer csSchoolid;
	private String filePath;
	private String xmlType;
	public Integer getFilePathId() {
		return filePathId;
	}
	public void setFilePathId(Integer filePathId) {
		this.filePathId = filePathId;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public Integer getCsSchoolid() {
		return csSchoolid;
	}
	public void setCsSchoolid(Integer csSchoolid) {
		this.csSchoolid = csSchoolid;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getXmlType() {
		return xmlType;
	}
	public void setXmlType(String xmlType) {
		this.xmlType = xmlType;
	}
	public XmlFilePathBean(Integer filePathId, Integer schoolid, Integer csSchoolid, String filePath, String xmlType) {
		super();
		this.filePathId = filePathId;
		this.schoolid = schoolid;
		this.csSchoolid = csSchoolid;
		this.filePath = filePath;
		this.xmlType = xmlType;
	}
	
	
}
