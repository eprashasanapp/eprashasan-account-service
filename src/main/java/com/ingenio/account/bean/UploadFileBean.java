package com.ingenio.account.bean;

import java.io.File;

public class UploadFileBean{

	private String FileName;
	private String filePath; 
	private File file;
	private byte[] fileArr;
 	
	public String getFileName() {
		return FileName;
	}
	public void setFileName(String fileName) {
		FileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public byte[] getFileArr() {
		return fileArr;
	}
	public void setFileArr(byte[] fileArr) {
		this.fileArr = fileArr;
	}
	
}
