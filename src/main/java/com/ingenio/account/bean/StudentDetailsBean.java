package com.ingenio.account.bean;

public class StudentDetailsBean {
	
	private Integer renewStudentId;
	private Integer standardId;
	private String  standardName;
	private Integer divisionId;
	private String  divisionName;
	private Integer yearId;
	private String yearName;

	
	public Integer getRenewStudentId() {
		return renewStudentId;
	}
	public void setRenewStudentId(Integer renewStudentId) {
		this.renewStudentId = renewStudentId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public String getYearName() {
		return yearName;
	}
	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public StudentDetailsBean(Integer renewStudentId, Integer standardId, String standardName, Integer divisionId,
			String divisionName, Integer yearId, String yearName) {
		super();
		this.renewStudentId = renewStudentId;
		this.standardId = standardId;
		this.standardName = standardName;
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.yearId = yearId;
		this.yearName = yearName;
	
	}
	public StudentDetailsBean() {
		super();
	}
	
	
}