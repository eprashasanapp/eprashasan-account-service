package com.ingenio.account.bean;

import java.math.BigInteger;
import java.util.List;

public class FeesettingBean {
	
	
	private	Integer yearId;
	private Integer standardId;
    private String narration;
	private String head;
	private String subHead;
	private String feesAmt;
	private String totalfees;
	private String fine;
	private String dueDate;
	private String fineIcrement;
	private Integer srNo;
	private Integer feeSettingId;
	private BigInteger feeSetMultiID;
	private Integer narrationId;
	private String StudName;
	private Integer StudRenewId;
	private Integer headId;
	private Integer subHeadId;
	private String totalFine;
	private String totalPaidFee;
	private String totalRefundFee;
	private String totalClearanceFee;
	private String totalDueFee;
	private String totalAssignFee;
	private Integer studentId;
	private String studentName;
	private String yearName;
	private String StandardName;
	private String studRegNo;
	private String categoryName;
	private String divName;
	private String religionName;
	private String casteName;
	private String gender;
	private String currentDate;
	private Integer schoolTypeId;
	private Integer schoolID;
	private Integer feeSetMulti2ID;
	private String mobNo;
	private String emailId;
	private String stdName;
    private String birthdate;
    private String totalRemainFee;
    private String currentyear;
    private String ageInNumber;
    private String medium;
    private String deliveryReport;
    private String textbox1;
    private String textbox2;
    private String textbox3;
    private String textbox4;
    private String textbox5;
    private String comboValue1;
    private String comboValue2;
    private String comboValue3;
    private String comboValue4;
    private String comboValue5;
    private String feeType;
    private Integer feetypeId;
    private Integer isConsolidation;
    private Integer renewId;
    private String settingFlag;
    private Integer studId;
    private String studentFee;
    private String scholarhipFee;
    private String totalFee;
    private Integer divId;
    private List<FeesettingBean> feesettingList;
    public Integer getStudFeeID() {
		return studFeeID;
	}
	public void setStudFeeID(Integer studFeeID) {
		this.studFeeID = studFeeID;
	}
	private Integer studFeeID;
    
    
	
	public Integer getIsConsolidation() {
		return isConsolidation;
	}
	public void setIsConsolidation(Integer isConsolidation) {
		this.isConsolidation = isConsolidation;
	}
	public Integer getFeetypeId() {
		return feetypeId;
	}
	public void setFeetypeId(Integer feetypeId) {
		this.feetypeId = feetypeId;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public String getTextbox1() {
		return textbox1;
	}
	public void setTextbox1(String textbox1) {
		this.textbox1 = textbox1;
	}
	public String getTextbox2() {
		return textbox2;
	}
	public void setTextbox2(String textbox2) {
		this.textbox2 = textbox2;
	}
	public String getTextbox3() {
		return textbox3;
	}
	public void setTextbox3(String textbox3) {
		this.textbox3 = textbox3;
	}
	public String getTextbox4() {
		return textbox4;
	}
	public void setTextbox4(String textbox4) {
		this.textbox4 = textbox4;
	}
	public String getTextbox5() {
		return textbox5;
	}
	public void setTextbox5(String textbox5) {
		this.textbox5 = textbox5;
	}
	public String getComboValue1() {
		return comboValue1;
	}
	public void setComboValue1(String comboValue1) {
		this.comboValue1 = comboValue1;
	}
	public String getComboValue2() {
		return comboValue2;
	}
	public void setComboValue2(String comboValue2) {
		this.comboValue2 = comboValue2;
	}
	public String getComboValue3() {
		return comboValue3;
	}
	public void setComboValue3(String comboValue3) {
		this.comboValue3 = comboValue3;
	}
	public String getComboValue4() {
		return comboValue4;
	}
	public void setComboValue4(String comboValue4) {
		this.comboValue4 = comboValue4;
	}
	public String getComboValue5() {
		return comboValue5;
	}
	public void setComboValue5(String comboValue5) {
		this.comboValue5 = comboValue5;
	}
	public Integer getFeeSetMulti2ID() {
		return feeSetMulti2ID;
	}
	public void setFeeSetMulti2ID(Integer feeSetMulti2ID) {
		this.feeSetMulti2ID = feeSetMulti2ID;
	}
	public Integer getIsConsolation() {
		return isConsolation;
	}
	public void setIsConsolation(Integer isConsolation) {
		this.isConsolation = isConsolation;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	public Integer getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}
	private Integer isConsolation;
	private Integer isDel;
	private Integer isEdit;
	private String feeSetMultiId1;
	
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getDivName() {
		return divName;
	}
	public void setDivName(String divName) {
		this.divName = divName;
	}
	public String getReligionName() {
		return religionName;
	}
	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}
	public String getCasteName() {
		return casteName;
	}
	public void setCasteName(String casteName) {
		this.casteName = casteName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	public String getYearName() {
		return yearName;
	}
	public void setYearName(String yearName) {
		this.yearName = yearName;
	}
	public String getStandardName() {
		return StandardName;
	}
	public void setStandardName(String standardName) {
		StandardName = standardName;
	}
	public String getStudRegNo() {
		return studRegNo;
	}
	public void setStudRegNo(String studRegNo) {
		this.studRegNo = studRegNo;
	}
	public String getTotalFine() {
		return totalFine;
	}
	public void setTotalFine(String totalFine) {
		this.totalFine = totalFine;
	}
	public String getTotalPaidFee() {
		return totalPaidFee;
	}
	public void setTotalPaidFee(String totalPaidFee) {
		this.totalPaidFee = totalPaidFee;
	}
	public String getTotalRefundFee() {
		return totalRefundFee;
	}
	public void setTotalRefundFee(String totalRefundFee) {
		this.totalRefundFee = totalRefundFee;
	}
	public String getTotalClearanceFee() {
		return totalClearanceFee;
	}
	public void setTotalClearanceFee(String totalClearanceFee) {
		this.totalClearanceFee = totalClearanceFee;
	}
	public String getTotalDueFee() {
		return totalDueFee;
	}
	public void setTotalDueFee(String totalDueFee) {
		this.totalDueFee = totalDueFee;
	}
	public Integer getHeadId() {
		return headId;
	}
	public void setHeadId(Integer headId) {
		this.headId = headId;
	}
	public Integer getSubHeadId() {
		return subHeadId;
	}
	public void setSubHeadId(Integer subHeadId) {
		this.subHeadId = subHeadId;
	}

	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getSubHead() {
		return subHead;
	}
	public void setSubHead(String subHead) {
		this.subHead = subHead;
	}
	public String getTotalfees() {
		return totalfees;
	}
	public void setTotalfees(String totalfees) {
		this.totalfees = totalfees;
	}
	public String getFine() {
		return fine;
	}
	public void setFine(String fine) {
		this.fine = fine;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getFineIcrement() {
		return fineIcrement;
	}
	public void setFineIcrement(String fineIcrement) {
		this.fineIcrement = fineIcrement;
	}
	public String getStudName() {
		return StudName;
	}
	public void setStudName(String studName) {
		StudName = studName;
	}
	public String getFeesAmt() {
		return feesAmt;
	}
	public void setFeesAmt(String feesAmt) {
		this.feesAmt = feesAmt;
	}
	public Integer getStudRenewId() {
		return StudRenewId;
	}
	public void setStudRenewId(Integer studRenewId) {
		StudRenewId = studRenewId;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public Integer getSrNo() {
		return srNo;
	}
	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}
	public Integer getFeeSettingId() {
		return feeSettingId;
	}
	public void setFeeSettingId(Integer feeSettingId) {
		this.feeSettingId = feeSettingId;
	}
	public BigInteger getFeeSetMultiID() {
		return feeSetMultiID;
	}
	public void setFeeSetMultiID(BigInteger feeSetMultiID) {
		this.feeSetMultiID = feeSetMultiID;
	}
	public Integer getNarrationId() {
		return narrationId;
	}
	public void setNarrationId(Integer narrationId) {
		this.narrationId = narrationId;
	}
	public String getTotalAssignFee() {
		return totalAssignFee;
	}
	public void setTotalAssignFee(String totalAssignFee) {
		this.totalAssignFee = totalAssignFee;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public Integer getSchoolID() {
		return schoolID;
	}
	public void setSchoolID(Integer schoolID) {
		this.schoolID = schoolID;
	}
	public Integer getSchoolTypeId() {
		return schoolTypeId;
	}
	public void setSchoolTypeId(Integer schoolTypeId) {
		this.schoolTypeId = schoolTypeId;
	}
	public String getMobNo() {
		return mobNo;
	}
	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public String getTotalRemainFee() {
		return totalRemainFee;
	}
	public void setTotalRemainFee(String totalRemainFee) {
		this.totalRemainFee = totalRemainFee;
	}
	public String getCurrentyear() {
		return currentyear;
	}
	public void setCurrentyear(String currentyear) {
		this.currentyear = currentyear;
	}
	public String getAgeInNumber() {
		return ageInNumber;
	}
	public void setAgeInNumber(String ageInNumber) {
		this.ageInNumber = ageInNumber;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	public String getDeliveryReport() {
		return deliveryReport;
	}
	public void setDeliveryReport(String deliveryReport) {
		this.deliveryReport = deliveryReport;
	}
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public String getSettingFlag() {
		return settingFlag;
	}
	public void setSettingFlag(String settingFlag) {
		this.settingFlag = settingFlag;
	}
	public String getFeeSetMultiId1() {
		return feeSetMultiId1;
	}
	public void setFeeSetMultiId1(String feeSetMultiId1) {
		this.feeSetMultiId1 = feeSetMultiId1;
	}
	public Integer getStudId() {
		return studId;
	}
	public void setStudId(Integer studId) {
		this.studId = studId;
	}
	public String getScholarhipFee() {
		return scholarhipFee;
	}
	public void setScholarhipFee(String scholarhipFee) {
		this.scholarhipFee = scholarhipFee;
	}
	public String getStudentFee() {
		return studentFee;
	}
	public void setStudentFee(String studentFee) {
		this.studentFee = studentFee;
	}
	public String getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	public List<FeesettingBean> getFeesettingList() {
		return feesettingList;
	}
	public void setFeesettingList(List<FeesettingBean> feesettingList) {
		this.feesettingList = feesettingList;
	}
	public Integer getDivId() {
		return divId;
	}
	public void setDivId(Integer divId) {
		this.divId = divId;
	}
	




}
