package com.ingenio.account.bean;

public class XMLResponseBean {
	
	private Long journalId;
	private String entryOf;
	private String date;
	private String narration;
	private String vouvherTypeName;
	private String voucherNumber;
	private String creditLedgerName;
	private String debitLedgerName;
	private Double drAmount;
	private Double crAmount;
	private Integer groupId;
	private String groupName;
	private Integer ledgerId;
	private String ledgerName;
	private Double openingAmount;
	private String ledgerHead;
	private String paymentDetails;
	private Integer bankId;
	
	public XMLResponseBean(Long journalId,String entryOf, String date, String narration, String vouvherTypeName,
			String voucherNumber, String creditLedgerName, String debitLedgerName,Double drAmount,Double crAmount,String paymentDetails,Integer bankId) {
		super();
		this.journalId = journalId;
		this.entryOf=entryOf;
		this.date = date;
		this.narration = narration;
		this.vouvherTypeName = vouvherTypeName;
		this.voucherNumber = voucherNumber;
		this.creditLedgerName = creditLedgerName;
		this.debitLedgerName = debitLedgerName;
		this.drAmount=drAmount;
		this.crAmount=crAmount;
		this.paymentDetails=paymentDetails;
		this.setBankId(bankId);
	}
	
	public XMLResponseBean(Integer groupId, String groupName) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
	}


	public XMLResponseBean(Integer ledgerId, String ledgerName, Double openingAmount, String ledgerHead,String groupName) {
		super();
		this.ledgerId = ledgerId;
		this.ledgerName = ledgerName;
		this.openingAmount = openingAmount;
		this.ledgerHead = ledgerHead;
		this.groupName=groupName;
	}



	public Long getJournalId() {
		return journalId;
	}
	public void setJournalId(Long journalId) {
		this.journalId = journalId;
	}
	public String getEntryOf() {
		return entryOf;
	}
	public void setEntryOf(String entryOf) {
		this.entryOf = entryOf;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getVouvherTypeName() {
		return vouvherTypeName;
	}
	public void setVouvherTypeName(String vouvherTypeName) {
		this.vouvherTypeName = vouvherTypeName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCreditLedgerName() {
		return creditLedgerName;
	}
	public void setCreditLedgerName(String creditLedgerName) {
		this.creditLedgerName = creditLedgerName;
	}
	public String getDebitLedgerName() {
		return debitLedgerName;
	}
	public void setDebitLedgerName(String debitLedgerName) {
		this.debitLedgerName = debitLedgerName;
	}


	public Double getDrAmount() {
		return drAmount;
	}


	public void setDrAmount(Double drAmount) {
		this.drAmount = drAmount;
	}


	public Double getCrAmount() {
		return crAmount;
	}


	public void setCrAmount(Double crAmount) {
		this.crAmount = crAmount;
	}


	public Integer getGroupId() {
		return groupId;
	}


	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Integer ledgerId) {
		this.ledgerId = ledgerId;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	public Double getOpeningAmount() {
		return openingAmount;
	}

	public void setOpeningAmount(Double openingAmount) {
		this.openingAmount = openingAmount;
	}

	public String getLedgerHead() {
		return ledgerHead;
	}

	public void setLedgerHead(String ledgerHead) {
		this.ledgerHead = ledgerHead;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
	
}
