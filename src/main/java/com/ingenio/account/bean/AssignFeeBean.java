package com.ingenio.account.bean;

import java.util.List;

public class AssignFeeBean {

	private Integer yearId;
	private Integer standardId;
	private Integer feeTypeId;
	private List<HeadBean> headId;
//	private List<Integer> subHeadId;
//	private List<Double> assignFee;
	private String totalAssignFee;
	private Integer schoolId;
	private Integer userId;
	private List<Integer> studentId;
//	private List<Double> fine;
//	private List<Integer> fineIncrement;
//	private List<String> dueDate;
	private String narrationName;
	private Integer narrationId;
	private List<Integer> studRenewId;
	private List<String> studName;
	private List<String> studRegNo;
	private String totalAssignFine;
	private String yearName;
	private String standardName;
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public Integer getFeeTypeId() {
		return feeTypeId;
	}
	public void setFeeTypeId(Integer feeTypeId) {
		this.feeTypeId = feeTypeId;
	}

//	public List<Integer> getSubHeadId() {
//		return subHeadId;
//	}
//	public void setSubHeadId(List<Integer> subHeadId) {
//		this.subHeadId = subHeadId;
//	}
//	public List<Double> getAssignFee() {
//		return assignFee;
//	}
//	public void setAssignFee(List<Double> assignFee) {
//		this.assignFee = assignFee;
//	}
	public String getTotalAssignFee() {
		return totalAssignFee;
	}
	public void setTotalAssignFee(String totalAssignFee) {
		this.totalAssignFee = totalAssignFee;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public List<Integer> getStudentId() {
		return studentId;
	}
	public void setStudentId(List<Integer> studentId) {
		this.studentId = studentId;
	}
//	public List<Double> getFine() {
//		return fine;
//	}
//	public void setFine(List<Double> fine) {
//		this.fine = fine;
//	}
//	public List<Integer> getFineIncrement() {
//		return fineIncrement;
//	}
//	public void setFineIncrement(List<Integer> fineIncrement) {
//		this.fineIncrement = fineIncrement;
//	}
//	public List<String> getDueDate() {
//		return dueDate;
//	}
//	public void setDueDate(List<String> dueDate) {
//		this.dueDate = dueDate;
//	}
	public List<Integer> getStudRenewId() {
		return studRenewId;
	}
	public void setStudRenewId(List<Integer> studRenewId) {
		this.studRenewId = studRenewId;
	}
	public List<String> getStudName() {
		return studName;
	}
	public void setStudName(List<String> studName) {
		this.studName = studName;
	}
	public List<String> getStudRegNo() {
		return studRegNo;
	}
	public void setStudRegNo(List<String> studRegNo) {
		this.studRegNo = studRegNo;
	}
	public String getTotalAssignFine() {
		return totalAssignFine;
	}
	public void setTotalAssignFine(String totalAssignFine) {
		this.totalAssignFine = totalAssignFine;
	}
	public String getYearName() {
		return yearName;
	}
	public void setYearName(String yearName) {
		this.yearName = yearName;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public List<HeadBean> getHeadId() {
		return headId;
	}
	public void setHeadId(List<HeadBean> headId) {
		this.headId = headId;
	}
	public String getNarrationName() {
		return narrationName;
	}
	public void setNarrationName(String narrationName) {
		this.narrationName = narrationName;
	}
	public Integer getNarrationId() {
		return narrationId;
	}
	public void setNarrationId(Integer narrationId) {
		this.narrationId = narrationId;
	}

}
