package com.ingenio.account.bean;

import java.math.BigInteger;
import java.util.List;

public class FeesToAccountLinkBean {

	
	private Integer schoolId;
	public Integer headId;
	public String headName;
	private String subheadIdArray;
	public String subHeadName;
	private List<String> arrayOfSubheadName;
	private List<String> arrayOfSubheadId;
	public Integer subheadId;
	public String[] headIdStr;
	public String[] subHeadIdStr;
	private Integer debitLedgerId;
	private String debitLedgerName;
	private String[] debitLedgerIdStr;
	private Integer creditLedgerId;
	private String creditLedgerName;
	private String[] creditLedgerIdStr;
	private Integer dueFineLedgerId;
	private String dueFineLedgerName;
	private String[] dueFineLedgerIdStr;
	private String[] feeToAccLinkIdStr;
	private Integer feeToAccLinkId;
	private String studentName;
	private String registartionNumber;
	private String yearName;
	private String standardName;
	private String receiptNo;
	private String receiptDate;
	private String payType;
	private String DDCHQno;
	private Double paidFee;
	private Double paidFine;
	private Integer maxRecieptId;
	private Integer paidId;
	private Integer bankId;
	private String bankName;
	private String branchName;
	private String bankAccNo;
	private Integer standardId;
	private Integer studentId;
	private String receiptId;
	private Integer renewstudentId;
	private Integer yearId;
	private String assignFee;
	private String assignFine;
	private int refundId;
	private String refundFee;
	private Double discount;
	private BigInteger journalId;
	private String amount;
	private String narration;
	private String paymentDetails;
	private String payTypeOrPrintFlag;
	private Integer vtypeId;
	private Integer cschoolID;
	private Integer ledgerId;
	private String scholarshipFlag;
	private String headID;
	private String subheadID;
	private String schoolKey;
	private Integer unitMappAccId;
	private Integer typeOfUnitId;
	private Integer classId;
	private Integer vTypeID;
	private String maxRecieptId1;
	private long maxRecieptIds;
	private Integer ledgerHeadId;
	private Integer groupSubgroupID;
	public Integer getVtypeId() {
		return vtypeId;
	}
	public void setVtypeId(Integer vtypeId) {
		this.vtypeId = vtypeId;
	}
	public Integer getHeadId() {
		return headId;
	}
	public void setHeadId(Integer headId) {
		this.headId = headId;
	}
	public String getHeadName() {
		return headName;
	}
	public void setHeadName(String headName) {
		this.headName = headName;
	}
	public String getSubheadIdArray() {
		return subheadIdArray;
	}
	public void setSubheadIdArray(String subheadIdArray) {
		this.subheadIdArray = subheadIdArray;
	}
	public String getSubHeadName() {
		return subHeadName;
	}
	public void setSubHeadName(String subHeadName) {
		this.subHeadName = subHeadName;
	}
	public List<String> getArrayOfSubheadName() {
		return arrayOfSubheadName;
	}
	public void setArrayOfSubheadName(List<String> arrayOfSubheadName) {
		this.arrayOfSubheadName = arrayOfSubheadName;
	}
	public List<String> getArrayOfSubheadId() {
		return arrayOfSubheadId;
	}
	public void setArrayOfSubheadId(List<String> arrayOfSubheadId) {
		this.arrayOfSubheadId = arrayOfSubheadId;
	}
	public Integer getSubheadId() {
		return subheadId;
	}
	public void setSubheadId(Integer subheadId) {
		this.subheadId = subheadId;
	}
	public String[] getHeadIdStr() {
		return headIdStr;
	}
	public void setHeadIdStr(String[] headIdStr) {
		this.headIdStr = headIdStr;
	}
	public String[] getSubHeadIdStr() {
		return subHeadIdStr;
	}
	public void setSubHeadIdStr(String[] subHeadIdStr) {
		this.subHeadIdStr = subHeadIdStr;
	}
	public Integer getDebitLedgerId() {
		return debitLedgerId;
	}
	public void setDebitLedgerId(Integer debitLedgerId) {
		this.debitLedgerId = debitLedgerId;
	}
	public String getDebitLedgerName() {
		return debitLedgerName;
	}
	public void setDebitLedgerName(String debitLedgerName) {
		this.debitLedgerName = debitLedgerName;
	}
	public String[] getDebitLedgerIdStr() {
		return debitLedgerIdStr;
	}
	public void setDebitLedgerIdStr(String[] debitLedgerIdStr) {
		this.debitLedgerIdStr = debitLedgerIdStr;
	}
	public Integer getCreditLedgerId() {
		return creditLedgerId;
	}
	public void setCreditLedgerId(Integer creditLedgerId) {
		this.creditLedgerId = creditLedgerId;
	}
	public String getCreditLedgerName() {
		return creditLedgerName;
	}
	public void setCreditLedgerName(String creditLedgerName) {
		this.creditLedgerName = creditLedgerName;
	}
	public String[] getCreditLedgerIdStr() {
		return creditLedgerIdStr;
	}
	public void setCreditLedgerIdStr(String[] creditLedgerIdStr) {
		this.creditLedgerIdStr = creditLedgerIdStr;
	}
	public Integer getDueFineLedgerId() {
		return dueFineLedgerId;
	}
	public void setDueFineLedgerId(Integer dueFineLedgerId) {
		this.dueFineLedgerId = dueFineLedgerId;
	}
	public String getDueFineLedgerName() {
		return dueFineLedgerName;
	}
	public void setDueFineLedgerName(String dueFineLedgerName) {
		this.dueFineLedgerName = dueFineLedgerName;
	}
	public String[] getDueFineLedgerIdStr() {
		return dueFineLedgerIdStr;
	}
	public void setDueFineLedgerIdStr(String[] dueFineLedgerIdStr) {
		this.dueFineLedgerIdStr = dueFineLedgerIdStr;
	}
	public String[] getFeeToAccLinkIdStr() {
		return feeToAccLinkIdStr;
	}
	public void setFeeToAccLinkIdStr(String[] feeToAccLinkIdStr) {
		this.feeToAccLinkIdStr = feeToAccLinkIdStr;
	}
	public Integer getFeeToAccLinkId() {
		return feeToAccLinkId;
	}
	public void setFeeToAccLinkId(Integer feeToAccLinkId) {
		this.feeToAccLinkId = feeToAccLinkId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getRegistartionNumber() {
		return registartionNumber;
	}
	public void setRegistartionNumber(String registartionNumber) {
		this.registartionNumber = registartionNumber;
	}
	public String getYearName() {
		return yearName;
	}
	public void setYearName(String yearName) {
		this.yearName = yearName;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getDDCHQno() {
		return DDCHQno;
	}
	public void setDDCHQno(String dDCHQno) {
		DDCHQno = dDCHQno;
	}
	public Double getPaidFee() {
		return paidFee;
	}
	public void setPaidFee(Double paidFee) {
		this.paidFee = paidFee;
	}
	public Double getPaidFine() {
		return paidFine;
	}
	public void setPaidFine(Double paidFine) {
		this.paidFine = paidFine;
	}
	public Integer getMaxRecieptId() {
		return maxRecieptId;
	}
	public void setMaxRecieptId(Integer maxRecieptId) {
		this.maxRecieptId = maxRecieptId;
	}
	public Integer getPaidId() {
		return paidId;
	}
	public void setPaidId(Integer paidId) {
		this.paidId = paidId;
	}
	public Integer getBankId() {
		return bankId;
	}
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBankAccNo() {
		return bankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}
	public Integer getRenewstudentId() {
		return renewstudentId;
	}
	public void setRenewstudentId(Integer renewstudentId) {
		this.renewstudentId = renewstudentId;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public String getAssignFee() {
		return assignFee;
	}
	public void setAssignFee(String assignFee) {
		this.assignFee = assignFee;
	}
	public String getAssignFine() {
		return assignFine;
	}
	public void setAssignFine(String assignFine) {
		this.assignFine = assignFine;
	}
	public int getRefundId() {
		return refundId;
	}
	public void setRefundId(int refundId) {
		this.refundId = refundId;
	}
	public String getRefundFee() {
		return refundFee;
	}
	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}
	public BigInteger getJournalId() {
		return journalId;
	}
	public void setJournalId(BigInteger journalId) {
		this.journalId = journalId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public String getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public String getPayTypeOrPrintFlag() {
		return payTypeOrPrintFlag;
	}
	public void setPayTypeOrPrintFlag(String payTypeOrPrintFlag) {
		this.payTypeOrPrintFlag = payTypeOrPrintFlag;
	}
	public Integer getCschoolID() {
		return cschoolID;
	}
	public void setCschoolID(Integer cschoolID) {
		this.cschoolID = cschoolID;
	}
	public Integer getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(Integer ledgerId) {
		this.ledgerId = ledgerId;
	}
	public String getScholarshipFlag() {
		return scholarshipFlag;
	}
	public void setScholarshipFlag(String scholarshipFlag) {
		this.scholarshipFlag = scholarshipFlag;
	}
	public String getHeadID() {
		return headID;
	}
	public void setHeadID(String headID) {
		this.headID = headID;
	}
	public String getSubheadID() {
		return subheadID;
	}
	public void setSubheadID(String subheadID) {
		this.subheadID = subheadID;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolKey() {
		return schoolKey;
	}
	public void setSchoolKey(String schoolKey) {
		this.schoolKey = schoolKey;
	}
	public Integer getClassId() {
		return classId;
	}
	public void setClassId(Integer classId) {
		this.classId = classId;
	}
	public Integer getTypeOfUnitId() {
		return typeOfUnitId;
	}
	public void setTypeOfUnitId(Integer typeOfUnitId) {
		this.typeOfUnitId = typeOfUnitId;
	}
	public Integer getUnitMappAccId() {
		return unitMappAccId;
	}
	public void setUnitMappAccId(Integer unitMappAccId) {
		this.unitMappAccId = unitMappAccId;
	}
	public Integer getvTypeID() {
		return vTypeID;
	}
	public void setvTypeID(Integer vTypeID) {
		this.vTypeID = vTypeID;
	}
	public long getMaxRecieptIds() {
		return maxRecieptIds;
	}
	public void setMaxRecieptIds(long maxRecieptIds) {
		this.maxRecieptIds = maxRecieptIds;
	}
	public String getMaxRecieptId1() {
		return maxRecieptId1;
	}
	public void setMaxRecieptId1(String maxRecieptId1) {
		this.maxRecieptId1 = maxRecieptId1;
	}
	public Integer getLedgerHeadId() {
		return ledgerHeadId;
	}
	public void setLedgerHeadId(Integer ledgerHeadId) {
		this.ledgerHeadId = ledgerHeadId;
	}
	public Integer getGroupSubgroupID() {
		return groupSubgroupID;
	}
	public void setGroupSubgroupID(Integer groupSubgroupID) {
		this.groupSubgroupID = groupSubgroupID;
	}

}
